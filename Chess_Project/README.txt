This project is a chess game.
The game built for two human players that playing on the same computer.
In this project, I created the engine of the game (the backend) in C++, and the graphics of the game (frontend) was already implementated in c#.
This project focuses on the object-oriented programming concepts. Classes, Encapsulation, Inheritance, Plymorphism, Abstraction and more.