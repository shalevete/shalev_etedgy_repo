#pragma once
#include <iostream>
#include <string>
#include "Defines.h"
#include "ChessPiece.h"

using std::string;

class Bishop : public ChessPiece
{
public:
	Bishop(bool isWhite);
	~Bishop();

	//move
	virtual string move(string to, string board);

	//answer to fronted	
	virtual answerToFrontend canMove(string from, string to, string board);
	bool checkIfLegalMove(string from, string to, string board);
	string getLeftDiagonal(string pos, int posTo, string board);
	string getRightDiagonal(string pos, int posTo, string board);
	bool checktDiagonal(int from, int to, string board, string diagonal);
};
