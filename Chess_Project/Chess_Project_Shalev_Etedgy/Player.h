#pragma once
#include <iostream>
#include <string>
#include "Defines.h"

using std::string;

class Player
{
public:
	Player(const bool isWhite);
	~Player();

	//turn
	void setTurn(bool isMyTurn);	
	bool getTurn();

	//win
	void setHasWon(bool hasWon);
	bool getHasWon();

	//color
	bool getIsWhite();

private:
	bool _isMyTurn;
	bool _hasWon;
	bool _isWhite;
};