#pragma once
#include <iostream>
#include <string>
#include "Defines.h"
#include "ChessPiece.h"

using std::string;

class King : public ChessPiece
{
public:
	King(bool isWhite);
	~King();

	//move
	virtual string move(string to, string board);

	//answer to fronted	
	virtual answerToFrontend canMove(string from, string to, string board);
	bool checkIfLegalMove(string from, string to, string board);

	//chess
	virtual answerToFrontend checkChess(string board, int ownPiece);
	answerToFrontend checkIfChessByRook(char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN], int ownPieceI, int ownPieceJ, bool isWhiteKing);
	answerToFrontend checkIfChessByKing(char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN], int ownPieceI, int ownPieceJ, bool isWhiteKing);
	answerToFrontend checkIfChessByBishop(char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN], int ownPieceI, int ownPieceJ, bool isWhiteKing);
	answerToFrontend checkIfChessByKnight(char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN], int ownPieceI, int ownPieceJ, bool isWhiteKing);

};
