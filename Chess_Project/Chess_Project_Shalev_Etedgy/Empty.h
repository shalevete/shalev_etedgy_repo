#pragma once
#include <iostream>
#include <string>
#include "Defines.h"
#include "ChessPiece.h"

using std::string;

class Empty : public ChessPiece
{
public:
	Empty(bool isWhite);
	~Empty();

	//move
	virtual answerToFrontend canMove(string from, string to, string board);
	virtual string move(string to, string board);
};
