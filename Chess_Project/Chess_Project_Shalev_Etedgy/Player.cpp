#include "Player.h"

Player::Player(const bool isWhite)
{
	this->_isWhite = isWhite;
	this->_hasWon = false;
	this->_isMyTurn = false;
}

Player::~Player()
{

}


//turn
void Player::setTurn(bool isMyTurn)
{
	this->_isMyTurn = isMyTurn;
}

bool Player::getTurn()
{
	return this->_isMyTurn;
}


//win
void Player::setHasWon(bool hasWon)
{
	this->_hasWon = hasWon;
}

bool Player::getHasWon()
{
	return this->_hasWon;
}


//color
bool Player::getIsWhite()
{
	return this->_isWhite;
}