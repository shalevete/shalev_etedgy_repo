#pragma once
#include <iostream>
#include <string>
#include "Defines.h"
#include "ChessPiece.h"

using std::string;

class Pawn : public ChessPiece
{
public:
	Pawn(bool isWhite);
	~Pawn();

	virtual string move(string to, string board);

	//answer to fronted	
	virtual answerToFrontend canMove(string from, string to, string board);
	bool checkIfLegalMove(string from, string to, string board);

private:
	bool _has_moved;
};
