#include "ChessPiece.h"

ChessPiece::ChessPiece(const bool isWhite, const char role)
{
	this->_role = role;
	changeRoleColor(isWhite);
	this->_pos = "";
}

ChessPiece::~ChessPiece()
{

}

//eat
void ChessPiece::eatMe()
{
	this->_role = EMPTY;
}


//position
void ChessPiece::setPosition(string pos)
{
	this->_pos = pos;
}

string ChessPiece::getPosition()
{
	return this->_pos;
}


//role
void ChessPiece::setRole(char role)
{
	this->_role = role;
}

char ChessPiece::getRole()
{
	return this->_role;
}

void ChessPiece::changeRoleColor(bool isWhite)
{
	if (isWhite)
	{
		this->_role = this->_role - 32;	//97('a') - 65('A') = 32
	}
}

answerToFrontend ChessPiece::checkChess(string board, int ownPiece)
{
	return res_goodMove;
}