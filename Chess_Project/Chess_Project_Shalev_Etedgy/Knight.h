#pragma once
#include <iostream>
#include <string>
#include "Defines.h"
#include "ChessPiece.h"

using std::string;

class Knight : public ChessPiece
{
public:
	Knight(bool isWhite);
	~Knight();

	//move
	virtual string move(string to, string board);

	//answer to fronted	
	virtual answerToFrontend canMove(string from, string to, string board);
	bool checkIfLegalMove(string from, string to, string board);

};
