#include "Rook.h"

Rook::Rook(bool isWhite) : ChessPiece(isWhite, BLACK_ROOK)
{
}

Rook::~Rook()
{

}

answerToFrontend Rook::canMove(string from, string to, string board)
{
	answerToFrontend answer = res_goodMove;


	if (from != this->_pos)
	{
		answer = res_ErrNoCurrPiece;
	}
	else if (from == to)
	{
		answer = res_ErrSamePlaces;
	}
	else if (!checkIfLegalMove(from, to, board))
	{
		answer = res_ErrIllegalMove;
	}

	return answer;
}

bool Rook::checkIfLegalMove(string from, string to, string board)
{
	int letterFrom = from[0] - ASCII_SMALL_A;
	int letterTo = to[0] - ASCII_SMALL_A;
	int intFrom = int(from[1]) - ASCII_0;
	int intTo = int(to[1]) - ASCII_0;
	bool isLegalMove = false;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	string yAxis = getYAxis(from, board);
	string xAxis = getXAxis(fromIndex, board);

	if (letterFrom == letterTo)	//move up or down
	{
		intFrom--;
		intTo--;
		isLegalMove = checkAxis(intFrom, intTo, board, yAxis);
	}
	else if (intFrom == intTo)	//move right or left
	{
		isLegalMove = checkAxis(letterFrom, letterTo, board, xAxis);
	}

	return isLegalMove;
}

string Rook::move(string to, string board)
{
	string from = this->_pos;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	board[toIndex] = board[fromIndex];
	board[fromIndex] = EMPTY;

	this->_pos = to;

	return board;
}

string Rook::getYAxis(string pos, string board)
{
	string strToRet = "";
	int i = 0;
	int letterOfPos = pos[0] - ASCII_SMALL_A;
	int posIndex = (int(pos[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(pos[1]) - ASCII_0)));

	for (i = 0; i < BOXES_IN_BOARD; i++)
	{
		if (posIndex == i)
		{
			strToRet += OWN_PIECE;
		}
		else if (i % LEN_OF_ROW_OR_COLUMN == letterOfPos)
		{
			strToRet += board[i];
		}

	}
	std::reverse(strToRet.begin(), strToRet.end());
	return strToRet;
}

string Rook::getXAxis(int pos, string board)
{
	string strToRet = "";
	int i = 0;
	int startOfXAxis = pos - (pos % LEN_OF_ROW_OR_COLUMN);

	for (i = startOfXAxis; i < startOfXAxis + LEN_OF_ROW_OR_COLUMN; i++)
	{
		if (pos == i)
		{
			strToRet += OWN_PIECE;
		}
		else
		{
			strToRet += board[i];
		}
	}
	return strToRet;
}

bool Rook::checkAxis(int from, int to, string board, string axis)
{
	int i = 0;
	bool isLegal = true;

	if (from < to)
	{
		for (i = from; (i < to) && (isLegal == true); i++)
		{
			if (axis[i] != EMPTY)
			{
				if (axis[i] == OWN_PIECE)
				{
					isLegal = true;
				}
				else
				{
					isLegal = false;
				}
			}
		}
	}
	else
	{
		for (i = from; (i > to) && (isLegal == true); i--)
		{
			if ((axis[i] != OWN_PIECE) && (axis[i] != EMPTY))
			{
				isLegal = false;
			}
		}
	}

	return isLegal;
}
