#include "King.h"

King::King(bool isWhite) : ChessPiece(isWhite, BLACK_KING)
{
}

King::~King()
{

}


answerToFrontend King::canMove(string from, string to, string board)
{
	answerToFrontend answer = res_goodMove;


	if (from != this->_pos)
	{
		answer = res_ErrNoCurrPiece;
	}
	else if (from == to)
	{
		answer = res_ErrSamePlaces;
	}
	else if (!checkIfLegalMove(from, to, board))
	{
		answer = res_ErrIllegalMove;
	}
	return answer;
}

bool King::checkIfLegalMove(string from, string to, string board)
{
	int letterFrom = from[0] - ASCII_SMALL_A;
	int letterTo = to[0] - ASCII_SMALL_A;
	int numFrom = int(from[1]) - ASCII_0;
	int numTo = int(to[1]) - ASCII_0;

	bool isLegalMove = false;

	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	if ((abs(letterFrom - letterTo) == 1) && (abs(numFrom - numTo) <= 1))
	{
		isLegalMove = true;
	}
	else if ((abs(numFrom - numTo) == 1) && (abs(letterFrom - letterTo) <= 1))
	{
		isLegalMove = true;
	}


	return isLegalMove;
}


string King::move(string to, string board)
{
	string from = this->_pos;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	board[toIndex] = board[fromIndex];
	board[fromIndex] = EMPTY;

	this->_pos = to;

	return board;
}


answerToFrontend King::checkChess(string board, int ownPiece)
{
	int ownPieceI = 0;
	int ownPieceJ = 0;
	answerToFrontend isChess = res_goodMove;
	char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN] = { 0 };
	int i = 0;
	int j = 0;
	int boardI = 0;
	bool isWhiteKing = (board[ownPiece] == WHITE_KING);
	board[ownPiece] = OWN_PIECE;

	//orginize the board in a 2d array
	for (i = 0; i < LEN_OF_ROW_OR_COLUMN; i++)
	{
		for (j = 0; j < LEN_OF_ROW_OR_COLUMN; j++)
		{
			board2D[i][j] = board[boardI];
			boardI++;
			if (board2D[i][j] == OWN_PIECE)
			{
				ownPieceI = i;
				ownPieceJ = j;
			}
		}
	}

	isChess = res_goodMove;
	if (KEEP_CHECK_CHESS)
	{
		//check if there are rooks that can eat the king (or queens)
		isChess = checkIfChessByRook(board2D, ownPieceI, ownPieceJ, isWhiteKing);
		if (KEEP_CHECK_CHESS)
		{
			isChess = checkIfChessByKing(board2D, ownPieceI, ownPieceJ, isWhiteKing);
			if (KEEP_CHECK_CHESS)
			{
				//check if there are bishops that can eat the king (or queens)
				isChess = checkIfChessByBishop(board2D, ownPieceI, ownPieceJ, isWhiteKing);
				if (KEEP_CHECK_CHESS)
				{
					isChess = checkIfChessByKnight(board2D, ownPieceI, ownPieceJ, isWhiteKing);
				}
			}
		}
	}
	return isChess;
}

answerToFrontend King::checkIfChessByRook(char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN], int ownPieceI, int ownPieceJ, bool isWhiteKing)
{
	answerToFrontend isChess = res_goodMove;
	char rook = 0;
	char queen = 0;
	int j = 0;
	int i = 0;
	if (isWhiteKing)
	{
		rook = BLACK_ROOK;
		queen = BLACK_QUEEN;
	}
	else
	{
		rook = WHITE_ROOK;
		queen = WHITE_QUEEN;
	}

	for (j = ownPieceJ + 1; ((j < LEN_OF_ROW_OR_COLUMN) && (KEEP_CHECK_CHESS)); j++)
	{
		if ((board2D[ownPieceI][j] == rook) || (board2D[ownPieceI][j] == (queen)))
		{
			isChess = res_goodMoveAndChess;
		}
		else if (board2D[ownPieceI][j] != EMPTY)
		{
			j = LEN_OF_ROW_OR_COLUMN;
		}
	}

	if (KEEP_CHECK_CHESS)
	{
		for (j = ownPieceJ - 1; ((j >= 0) && (KEEP_CHECK_CHESS)); j--)
		{
			if ((board2D[ownPieceI][j] == rook) || (board2D[ownPieceI][j] == (queen)))
			{
				isChess = res_goodMoveAndChess;
			}
			else if (board2D[ownPieceI][j] != EMPTY)
			{
				j = NOTHING;
			}
		}
	}

	if (KEEP_CHECK_CHESS)
	{
		for (i = ownPieceI + 1; ((i < LEN_OF_ROW_OR_COLUMN) && (KEEP_CHECK_CHESS)); i++)
		{
			if ((board2D[i][ownPieceJ] == rook) ||( board2D[i][ownPieceJ] == (queen)))
			{
				isChess = res_goodMoveAndChess;
			}
			else if (board2D[i][ownPieceJ] != EMPTY)
			{
				i = LEN_OF_ROW_OR_COLUMN;
			}
		}
	}

	if (KEEP_CHECK_CHESS)
	{
		for (i = ownPieceI - 1; ((i >= 0) && (KEEP_CHECK_CHESS)); i--)
		{
			if ((board2D[i][ownPieceJ] == rook) || (board2D[i][ownPieceJ] == (queen)))
			{
				isChess = res_goodMoveAndChess;
			}
			else if (board2D[i][ownPieceJ] != EMPTY)
			{
				i = NOTHING;
			}
		}
	}
	return isChess;
}

answerToFrontend King::checkIfChessByKing(char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN], int ownPieceI, int ownPieceJ, bool isWhiteKing)
{
	answerToFrontend isChess = res_goodMove;
	char king = 0;
	int j = 0;
	int i = 0;
	if (isWhiteKing)
	{
		king = BLACK_KING;
	}
	else
	{
		king = WHITE_KING;
	}

	//check the 3 boxes that above the piece
	if (ownPieceI > 0)
	{
		if (board2D[ownPieceI - 1][ownPieceJ] == (king))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((ownPieceJ > 0) && (board2D[ownPieceI - 1][ownPieceJ-1] == (king)))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((isWhiteKing) && ( ((ownPieceJ > 0) && (board2D[ownPieceI - 1][ownPieceJ - 1] == BLACK_PAWN)) || ((ownPieceJ < LEN_OF_ROW_OR_COLUMN -1) && (board2D[ownPieceI - 1][ownPieceJ + 1] == BLACK_PAWN))))
		{
			isChess = res_goodMoveAndChess; 
		}
		else if ((ownPieceJ < LEN_OF_ROW_OR_COLUMN - 1) && (board2D[ownPieceI - 1][ownPieceJ + 1] == (king)))
		{
			isChess = res_goodMoveAndChess;
		}
	}
	//check the 3 boxes that under the piece
	if ((ownPieceI < LEN_OF_ROW_OR_COLUMN -  1) && (KEEP_CHECK_CHESS))	//(from0-7 and not from 1-8)
	{
		if (board2D[ownPieceI + 1][ownPieceJ] == (king))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((ownPieceJ < LEN_OF_ROW_OR_COLUMN - 1) && ((board2D[ownPieceI + 1][ownPieceJ + 1] == (king))))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((isWhiteKing) && (((ownPieceJ < LEN_OF_ROW_OR_COLUMN - 1) && (board2D[ownPieceI + 1][ownPieceJ + 1] == BLACK_PAWN)) || ((ownPieceJ > 0) && (board2D[ownPieceI + 1][ownPieceJ - 1] == BLACK_PAWN))))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((ownPieceJ > 0) && ((board2D[ownPieceI + 1][ownPieceJ - 1] == (king))))
		{
			isChess = res_goodMoveAndChess;
		}
	}
	//check left and right
	if ((ownPieceJ < LEN_OF_ROW_OR_COLUMN - 1) && (KEEP_CHECK_CHESS))
	{
		if (board2D[ownPieceI ][ownPieceJ+1] == (king))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((ownPieceJ > 0) && ((board2D[ownPieceI][ownPieceJ - 1] == (king))))
		{
			isChess = res_goodMoveAndChess;
		}
	}
	return isChess;
}

answerToFrontend King::checkIfChessByBishop(char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN], int ownPieceI, int ownPieceJ, bool isWhiteKing)
{
	answerToFrontend isChess = res_goodMove;
	bool isBishopFound = false;
	bool isOwnPieceFound = false;
	char bishop = 0;
	char queen = 0;
	int j = 0;
	int i = 0;
	if (isWhiteKing)
	{
		bishop = BLACK_BISHOP;
		queen = BLACK_QUEEN;
	}
	else
	{
		bishop = WHITE_BISHOP;
		queen = WHITE_QUEEN;
	}

	for (i = 0; i < LEN_OF_ROW_OR_COLUMN; i++)
	{
		for (j = 0; j <= ownPieceJ; j++)
		{
			if ((ownPieceJ - j == ownPieceI - i) || (-1 * (ownPieceJ - j) == ownPieceI - i))
			{
				if (board2D[i][j] == (bishop) || (board2D[i][j] == (queen)))
				{
					isBishopFound = true;
				}
				else if (board2D[i][j] == OWN_PIECE)
				{
					isOwnPieceFound = true;
				}
				else if ((board2D[i][j] != EMPTY) && (board2D[i][j] != OWN_PIECE) || (board2D[i][j] == (queen)))
				{
					if (isBishopFound && isOwnPieceFound)
					{
						isChess = res_goodMoveAndChess;
					}
					else if (isBishopFound || isOwnPieceFound)
					{
						j = LEN_OF_ROW_OR_COLUMN;
						i = LEN_OF_ROW_OR_COLUMN;
					}
				}
			}
		}

	}

	if (isBishopFound && isOwnPieceFound)
	{
		isChess = res_goodMoveAndChess;
	}


	if (KEEP_CHECK_CHESS)
	{
		isBishopFound = false;
		isOwnPieceFound = false;
		for (i = 0; i < LEN_OF_ROW_OR_COLUMN; i++)
		{
			for (j = ownPieceJ; j < LEN_OF_ROW_OR_COLUMN; j++)
			{
				if ((j - ownPieceJ == ownPieceI - i) || (-1 * (j - ownPieceJ) == ownPieceI - i))
				{
					if (board2D[i][j] == (bishop))
					{
						isBishopFound = true;
					}
					else if (board2D[i][j] == OWN_PIECE)
					{
						isOwnPieceFound = true;
					}
					else if ((board2D[i][j] != EMPTY) && (board2D[i][j] != OWN_PIECE))
					{
						if (isBishopFound && isOwnPieceFound)
						{
							isChess = res_goodMoveAndChess;
						}
						else if (isBishopFound || isOwnPieceFound)
						{
							j = LEN_OF_ROW_OR_COLUMN;
							i = LEN_OF_ROW_OR_COLUMN;
						}
					}
				}
			}
		}
	}

	if (isBishopFound && isOwnPieceFound)
	{
		isChess = res_goodMoveAndChess;
	}

	return isChess;
}

answerToFrontend King::checkIfChessByKnight(char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN], int ownPieceI, int ownPieceJ, bool isWhiteKing)
{
	answerToFrontend isChess = res_goodMove;
	char knight = 0;
	if (isWhiteKing)
	{
		knight = BLACK_KNIGHT;
	}
	else
	{
		knight = WHITE_KNIGHT;
	}

	if ((ownPieceI < LEN_OF_ROW_OR_COLUMN - 2) && ((ownPieceI > 0) || (ownPieceJ > 0)))
	{
		if (board2D[ownPieceI + 2][ownPieceJ + 1] == (knight))
		{
			isChess = res_goodMoveAndChess;
		}
		else if (board2D[ownPieceI + 2][ownPieceJ - 1] == (knight))
		{
			isChess = res_goodMoveAndChess;
		}
	}
	if ((ownPieceJ < LEN_OF_ROW_OR_COLUMN - 2) && ((ownPieceI > 0) || (ownPieceJ > 0)))
	{
		if (board2D[ownPieceI + 1][ownPieceJ + 2] == (knight))
		{
			isChess = res_goodMoveAndChess;
		}
		else if (board2D[ownPieceI - 1][ownPieceJ + 2] == (knight))
		{
			isChess = res_goodMoveAndChess;
		}

	}
	if (((ownPieceI < LEN_OF_ROW_OR_COLUMN - 1) || (ownPieceJ < LEN_OF_ROW_OR_COLUMN - 1)) && ((ownPieceI > 1) || (ownPieceJ > 1)))
	{

		if ((ownPieceJ < LEN_OF_ROW_OR_COLUMN - 1) && (ownPieceI > 1) && (board2D[ownPieceI - 2][ownPieceJ + 1] == (knight)))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((ownPieceI > 1) && (board2D[ownPieceI - 2][ownPieceJ - 1] == (knight)))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((ownPieceJ > 1) && board2D[ownPieceI - 1][ownPieceJ - 2] == (knight))
		{
			isChess = res_goodMoveAndChess;
		}
		else if ((ownPieceJ < LEN_OF_ROW_OR_COLUMN - 1) && (ownPieceJ > 1) && board2D[ownPieceI + 1][ownPieceJ - 2] == (knight))
		{
			isChess = res_goodMoveAndChess;
		}
	}
	return isChess;
}