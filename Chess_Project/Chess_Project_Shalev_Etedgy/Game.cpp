#include "Game.h"

Game::Game() : _white(true), _black(false)
{
	this->_gameBoard.startBoardMsg();
	this->_isPipeConnected = false;
	this->_isWhiteTurn = true;
	this->_white.setTurn(true);
	this->_msgFromGraphics = "";
	this->_msgToSend = "";
}

Game::~Game()
{
	endGame();
}


//startGame & endGame
void Game::startGame()
{
	connectToPipe();
	this->_isPipeConnected = isPipeConnected();
	if (!this->_isPipeConnected)
	{
		throw(PE_cantConnect);
	}
	this->_msgToSend = this->_gameBoard.getBoardMsg(this->_isWhiteTurn);
	sendMsg(this->_msgToSend);
	this->_gameBoard.printBoard();
}

void Game::playGame()
{
	answerToFrontend isTurnValid = res_goodMove;

	// get message from graphics
	this->_msgFromGraphics = getMsg();

	while ((this->_msgFromGraphics != "quit") && (!this->_white.getHasWon()) && (!this->_black.getHasWon()))
	{
		isTurnValid = this->_gameBoard.checkTurnValid(this->_msgFromGraphics, this->_isWhiteTurn);
		

		if (isTurnValid == res_goodMoveAndWChess && this->_white.getTurn())
		{			
				isTurnValid = res_ErrOwnChess;	
		}
		else if (isTurnValid == res_goodMoveAndBChess && this->_black.getTurn())
		{
				isTurnValid = res_ErrOwnChess;
		}
		else if ((isTurnValid == res_goodMoveAndBChess) || (isTurnValid == res_goodMoveAndWChess))
		{
			isTurnValid = res_goodMoveAndChess;
		}

		this->_msgToSend = whatHappened(isTurnValid);

		sendMsg(this->_msgToSend);
		this->_gameBoard.printBoard();
		// get message from graphics
		nextTurn();
		if ((!this->_white.getHasWon()) && (!this->_black.getHasWon()))
		{
		this->_msgFromGraphics = getMsg();
		}
	}
}

void Game::endGame()
{
	this->_p.close();
}

string Game::whatHappened(answerToFrontend TurnValid)
{
	string stringToReturn = "";

	stringToReturn = std::to_string(int(TurnValid));

	switch (TurnValid)
	{
	case res_goodMove:
		this->_gameBoard.callMove(this->_msgFromGraphics);
		break;
	case res_goodMoveAndChess:
		this->_gameBoard.callMove(this->_msgFromGraphics);
		break;
	case res_goodMoveWin:
		this->_gameBoard.callMove(this->_msgFromGraphics);
		if (this->_isWhiteTurn)
		{
			this->_white.setHasWon(true);
		}
		else
		{
			this->_black.setHasWon(true);
		}
		break;
	default:
		nextTurn();
		break;
	}
	return stringToReturn;
}


//Pipe
void Game::connectToPipe()
{
	this->_isPipeConnected = this->_p.connect();
}

bool Game::isPipeConnected()
{
	bool flagTryAgain = true;
	string ans;
	while (!this->_isPipeConnected && flagTryAgain)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			this->_isPipeConnected = this->_p.connect();
		}
		else
		{
			this->_p.close();
			this->_isPipeConnected = false;
			flagTryAgain = false;
		}
	}
	return this->_isPipeConnected;
}

void Game::sendMsg(string boardMsg)
{
	char msgToGraphics[66];
	strcpy_s(msgToGraphics, boardMsg.c_str());
	this->_p.sendMessageToGraphics(msgToGraphics);
}

string Game::getMsg()
{
	string msgFromGraphics = this->_p.getMessageFromGraphics();
	return msgFromGraphics;
}


//turn
void Game::setTurn(bool _isWhite)
{
	this->_isWhiteTurn = _isWhite;
	if (_isWhite)
	{
		this->_white.setTurn(true);
		this->_black.setTurn(false);
	}
	else
	{
		this->_white.setTurn(false);
		this->_black.setTurn(true);
	}
}

bool Game::getTurn()
{
	return this->_isWhiteTurn;
}

void Game::nextTurn()
{
	setTurn(!this->_isWhiteTurn);
}
