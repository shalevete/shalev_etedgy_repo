#include "Knight.h"

Knight::Knight(bool isWhite) : ChessPiece(isWhite, BLACK_KNIGHT)
{
}

Knight::~Knight()
{

}


answerToFrontend Knight::canMove(string from, string to, string board)
{
	answerToFrontend answer = res_goodMove;


	if (from != this->_pos)
	{
		answer = res_ErrNoCurrPiece;
	}
	else if (from == to)
	{
		answer = res_ErrSamePlaces;
	}
	else if (!checkIfLegalMove(from, to, board))
	{
		answer = res_ErrIllegalMove;
	}
	return answer;
}

bool Knight::checkIfLegalMove(string from, string to, string board)
{
	int letterFrom = from[0] - ASCII_SMALL_A;
	int letterTo = to[0] - ASCII_SMALL_A;
	int numFrom = int(from[1]) - ASCII_0;
	int numTo = int(to[1]) - ASCII_0;
	bool isLegalMove = false;

	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	if ((numFrom + 2 == numTo) && ((letterFrom + 1 == letterTo) || ((letterFrom - 1 == letterTo))))
	{
		isLegalMove = true;
	}
	else if ((letterFrom + 2 == letterTo) && ((numFrom + 1 == numTo) || ((numFrom - 1 == numTo))))
	{
		isLegalMove = true;
	}
	else if ((numFrom - 2 == numTo) && ((letterFrom + 1 == letterTo) || ((letterFrom - 1 == letterTo))))
	{
		isLegalMove = true;
	}
	else if ((letterFrom - 2 == letterTo) && ((numFrom + 1 == numTo) || ((numFrom - 1 == numTo))))
	{
		isLegalMove = true;
	}

	return isLegalMove;
}


string Knight::move(string to, string board)
{
	string from = this->_pos;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	board[toIndex] = board[fromIndex];
	board[fromIndex] = EMPTY;

	this->_pos = to;

	return board;
}
