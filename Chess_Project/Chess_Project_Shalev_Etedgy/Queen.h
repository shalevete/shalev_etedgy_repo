#pragma once
#include <iostream>
#include <string>
#include "Defines.h"
#include "ChessPiece.h"

using std::string;

class Queen : public ChessPiece
{
public:
	Queen(bool isWhite);
	~Queen();

	//move
	virtual string move(string to, string board);

	//answer to fronted	
	virtual answerToFrontend canMove(string from, string to, string board);
	bool checkIfLegalMoveLikeRook(string from, string to, string board);
	bool checkIfLegalMoveLikeBishop(string from, string to, string board);
	string getYAxis(string pos, string board);
	string getXAxis(int pos, string board);
	bool checkAxis(int from, int to, string board, string axis);
	bool checktDiagonal(int from, int to, string board, string diagonal);
	string getRightDiagonal(string pos, int posTo, string board);
	string getLeftDiagonal(string pos, int posTo, string board);

};
