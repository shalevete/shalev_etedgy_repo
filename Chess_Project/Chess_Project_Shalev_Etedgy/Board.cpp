#include "Board.h"

Board::Board()
{
	startBoardMsg();
	addPieces(START_BOARD_MSG);
	this->_isValidTurn = false;
	this->_indexToEat = NOTHING;
	this->_needToEat = false;
}

void Board::addPieces(string boardLayout)
{
	int i = 0;
	string pos = START_OF_BOARD;

	for (i = 0; i < BOXES_IN_BOARD; i++)
	{
		if (i % LEN_OF_ROW_OR_COLUMN == 0)
		{
			pos[0] = ASCII_SMALL_A;
			pos[1] = (LEN_OF_ROW_OR_COLUMN - (i / LEN_OF_ROW_OR_COLUMN)) + ASCII_0;
		}
		else
		{
			pos[0]++;
		}

		switch (boardLayout[i])
		{
		case BLACK_ROOK:
			addRook(pos, false);
			break;
		case WHITE_ROOK:
			addRook(pos, true);
			break;
		case BLACK_KING:
			addKing(pos, false);
			break;
		case WHITE_KING:
			addKing(pos, true);
			break;
		case BLACK_BISHOP:
			addBishop(pos, false);
			break;
		case WHITE_BISHOP:
			addBishop(pos, true);
			break;
		case BLACK_KNIGHT:
			addKnight(pos, false);
			break;
		case WHITE_KNIGHT:
			addKnight(pos, true);
			break;
		case BLACK_QUEEN:
			addQueen(pos, false);
			break;
		case WHITE_QUEEN:
			addQueen(pos, true);
			break;
		case BLACK_PAWN:
			addPawn(pos, false);
			break;
		case WHITE_PAWN:
			addPawn(pos, true);
			break;
		case EMPTY:
			addEmpty(pos);
			break;
		default:
			addEmpty(pos);
			break;
		}
	}
}

Board::~Board()
{
	removeAllPieces();
}

void Board::removeAllPieces()
{
	int i = 0;
	for (i = 0; i < this->_allPieces.size(); i++)
	{
		removePiece(i);
	}
}

void Board::removePiece(int index)
{
	delete this->_allPieces[index];
	this->_allPieces.erase(this->_allPieces.begin() + index);
}


//orginize the board message
void Board::organizeBoard(bool isWhiteTurn)
{
	char board[BOXES_IN_BOARD + 1] = "";	//65 instead of 64 because of the '0' or '1' that say the turn in the end
	int position = 0;
	string posStr = "";
	int i = 0;
	for (i = 0; i < BOXES_IN_BOARD; i++)
	{
		posStr = this->_allPieces[i]->getPosition();
		position = (int(posStr[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(posStr[1]) - ASCII_0)));	//row number * 8 + column = position in the msg
		board[position] = (this->_allPieces[i])->getRole();
	}

	if (isWhiteTurn)
	{
		board[BOXES_IN_BOARD] = ASCII_0;
	}
	else
	{
		board[BOXES_IN_BOARD] = ASCII_1;
	}

	this->_boardMsg = string(board);
	string temp = this->_boardMsg;
	this->_boardMsg = temp.substr(0, BOXES_IN_BOARD + 1);
}

void Board::startBoardMsg()
{
	this->_boardMsg = START_BOARD_MSG;
}

string Board::getBoardMsg(bool isWhiteTurn)
{
	organizeBoard(isWhiteTurn);
	return this->_boardMsg;
}


//Move
answerToFrontend Board::checkTurnValid(string moveStr, bool isWhiteTurn)
{
	answerToFrontend answer  = res_goodMove;
	string from = moveStr.substr(0, 2);
	string to = moveStr.substr(2, 2);
	char sourcePiece = this->_allPieces[findIndex(from)]->getRole();
	char dstPiece = this->_allPieces[findIndex(to)]->getRole();
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	answer = (this->_allPieces[findIndex(from)])->canMove(from, to, this->_boardMsg);	//check if can move according to the chess rules
	
	if (answer == res_goodMove)
	{
		if ((sourcePiece == EMPTY) || (!isPieceColorGood(sourcePiece, isWhiteTurn)))	//check if the src box is empty or the current player piece
		{
			answer = res_ErrNoCurrPiece;
		}
		else
		{
			if (from == to)		//making sure that the src box and the dst box are not the same
			{
				answer = res_ErrSamePlaces;
			}
			else if((dstPiece != EMPTY) && (!isPieceColorGood(dstPiece, !isWhiteTurn)))		//check if the dst box is empty or the current player piece
			{
				answer = res_ErrCurrPieceInDst;
			}
			else if (!goodIndex(from) || !goodIndex(to))	//check if indexes are good
			{
				answer = res_ErrIllegalIndex;
			}
		}
	}
	
	if (answer == res_goodMove)
	{
		if ((dstPiece == WHITE_KING) || (dstPiece == BLACK_KING))
		{
			answer = res_goodMoveWin;
		}
	}
	if (answer == res_goodMove)
	{
		this->_boardMsg[fromIndex] = EMPTY;
		this->_boardMsg[toIndex] = sourcePiece;


		answer = checkChess(true);
		if (answer == res_goodMoveAndChess)
		{
			answer = res_goodMoveAndWChess;
		}
		

		if (answer == res_goodMove)
		{
			answer = checkChess(false);
			if (answer == res_goodMoveAndChess)
			{
				answer = res_goodMoveAndBChess;
			}
		}

		this->_boardMsg[fromIndex] = sourcePiece;
		this->_boardMsg[toIndex] = dstPiece;
	}
	return answer;
}

answerToFrontend Board::checkChess(bool checkWhite)
{
	answerToFrontend answer = res_goodMove;
	int kingPos = 0;
	int kingIndex = 0;

	if (checkWhite)
	{
		kingPos = findIndexByRoleInMsg(WHITE_KING);
		kingIndex = findIndexByRole(WHITE_KING);
		answer = this->_allPieces[kingIndex]->checkChess(this->_boardMsg, kingPos);
	}
	else
	{
		kingPos = findIndexByRoleInMsg(BLACK_KING);
		kingIndex = findIndexByRole(BLACK_KING);
		answer = this->_allPieces[kingIndex]->checkChess(this->_boardMsg, kingPos);
	}

	return answer;
}

bool Board::isPieceColorGood(char source, bool isWhiteTurn)
{
	bool ansToRet = false;

	if ((isWhiteTurn == true) && ((source >= ASCII_BIG_A) && (source <= ASCII_BIG_Z)))
	{
		ansToRet = true;
	}
	else if ((isWhiteTurn == false) && ((source >= ASCII_SMALL_A) && (source <= ASCII_SMALL_Z)))
	{
		ansToRet = true;
	}
	return ansToRet;
}


bool Board::goodIndex(string index)
{
	bool isGoodIndex = true;

	if (((int(index[1]) - ASCII_0 < 0) || (int(index[1]) - ASCII_0 > LEN_OF_ROW_OR_COLUMN))  && ((index[0] < ASCII_SMALL_A) || (index[0] > ASCII_SMALL_H) ))
	{
		isGoodIndex = false;
	}

	return isGoodIndex;
}


int Board::findIndex(string boxIndex)
{
	int posOfPiece = 0;
	string posStr = "";
	int i = 0;
	bool pieceFound = false;
	int indexInVec = 0;

	for (i = 0; ((i < BOXES_IN_BOARD) && (!pieceFound)); i++)
	{
		posStr = this->_allPieces[i]->getPosition();
		if (boxIndex == posStr)
		{
			indexInVec = i;
			pieceFound = true;
		}
	}

	return indexInVec;
}

int Board::findIndexByRoleInMsg(char role)
{
	int posOfPiece = 0;
	char roleToCheck = 0;
	int i = 0;
	bool pieceFound = false;
	int indexInVec = 0;

	for (i = 0; ((i < BOXES_IN_BOARD) && (!pieceFound)); i++)
	{
		roleToCheck = this->_boardMsg[i];
		if (role == roleToCheck)
		{
			indexInVec = i;
			pieceFound = true;
		}
	}

	return indexInVec;
}

int Board::findIndexByRole(char role)
{
	int posOfPiece = 0;
	char roleToCheck = 0;
	int i = 0;
	bool pieceFound = false;
	int indexInVec = 0;

	for (i = 0; ((i < BOXES_IN_BOARD) && (!pieceFound)); i++)
	{
		roleToCheck = this->_allPieces[i]->getRole();
		if (role == roleToCheck)
		{
			indexInVec = i;
			pieceFound = true;
		}
	}

	return indexInVec;
}

void Board::callMove(string moveStr)
{
	string moveTo = moveStr.substr(2, 2);
	string moveFrom = moveStr.substr(0, 2);

	//find the index of the boxes in the vector of the pieces
	int toIndex = findIndex(moveTo);

	//eat the piece that in the dst box
	(this->_allPieces)[toIndex]->eatMe();
	this->removePiece(toIndex);

	//move the piece (update the boardMsg and the pieces vector)
	this->_boardMsg = (this->_allPieces)[findIndex(moveFrom)]->move(moveTo, this->_boardMsg);

	//add an empty box to the vector of the pieces
	addEmpty(moveFrom);
}


void Board::addRook(string pos, bool isWhite)
{
	ChessPiece* rookptr = new Rook(isWhite);
	rookptr->setPosition(pos);
	this->_allPieces.push_back(rookptr);
}

void Board::addEmpty(string pos)
{
	ChessPiece* emptyptr = new Empty(false);
	emptyptr->setPosition(pos);
	this->_allPieces.push_back(emptyptr);
}

void Board::addKing(string pos, bool isWhite)
{
	ChessPiece* kingptr = new King(isWhite);
	kingptr->setPosition(pos);
	this->_allPieces.push_back(kingptr);
}

void Board::addBishop(string pos, bool isWhite)
{
	ChessPiece* bishopptr = new Bishop(isWhite);
	bishopptr->setPosition(pos);
	this->_allPieces.push_back(bishopptr);
}

void Board::addKnight(string pos, bool isWhite)
{
	ChessPiece* knightptr = new Knight(isWhite);
	knightptr->setPosition(pos);
	this->_allPieces.push_back(knightptr);
}

void Board::addQueen(string pos, bool isWhite)
{
	ChessPiece* queentptr = new Queen(isWhite);
	queentptr->setPosition(pos);
	this->_allPieces.push_back(queentptr);
}

void Board::addPawn(string pos, bool isWhite)
{
	ChessPiece* pawnptr = new Pawn(isWhite);
	pawnptr->setPosition(pos);
	this->_allPieces.push_back(pawnptr);
}

void Board::printBoard()
{
	string pos = START_OF_BOARD;

	int i = 0;
	for (i = 0; i < BOXES_IN_BOARD; i++)
	{
		if (i % LEN_OF_ROW_OR_COLUMN == 0)
		{
			pos[0] = ASCII_SMALL_A;
			pos[1] = (LEN_OF_ROW_OR_COLUMN - (i / LEN_OF_ROW_OR_COLUMN)) + ASCII_0;
		}
		else
		{
			pos[0] = pos[0] + 1;
		}

		if (i % LEN_OF_ROW_OR_COLUMN == 0 && i!=0)
		{
			std::cout << "\n";
		}
		std::cout << this->_allPieces[findIndex(pos)]->getRole() << " ";

	}

	std::cout << "boardMsg: " << this->_boardMsg << "\n";
}
