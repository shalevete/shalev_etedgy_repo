#include "Queen.h"

Queen::Queen(bool isWhite) : ChessPiece(isWhite, BLACK_QUEEN)
{
}

Queen::~Queen()
{

}

answerToFrontend Queen::canMove(string from, string to, string board)
{
	answerToFrontend answer = res_goodMove;


	if (from != this->_pos)
	{
		answer = res_ErrNoCurrPiece;
	}
	else if (from == to)
	{
		answer = res_ErrSamePlaces;
	}
	else if ((!checkIfLegalMoveLikeRook(from, to, board)) && (!checkIfLegalMoveLikeBishop(from, to, board)))
	{
		answer = res_ErrIllegalMove;
	}

	return answer;
}

string Queen::move(string to, string board)
{
	string from = this->_pos;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	board[toIndex] = board[fromIndex];
	board[fromIndex] = EMPTY;

	this->_pos = to;

	return board;
}

bool Queen::checkIfLegalMoveLikeRook(string from, string to, string board)
{
	int letterFrom = from[0] - ASCII_SMALL_A;
	int letterTo = to[0] - ASCII_SMALL_A;
	int intFrom = int(from[1]) - ASCII_0;
	int intTo = int(to[1]) - ASCII_0;
	bool isLegalMove = false;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	string yAxis = getYAxis(from, board);
	string xAxis = getXAxis(fromIndex, board);

	if (letterFrom == letterTo)	//move up or down
	{
		intFrom--;
		intTo--;
		isLegalMove = checkAxis(intFrom, intTo, board, yAxis);
	}
	else if (intFrom == intTo)	//move right or left
	{
		isLegalMove = checkAxis(letterFrom, letterTo, board, xAxis);
	}

	return isLegalMove;
}

string Queen::getYAxis(string pos, string board)
{
	string strToRet = "";
	int i = 0;
	int letterOfPos = pos[0] - ASCII_SMALL_A;
	int posIndex = (int(pos[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(pos[1]) - ASCII_0)));

	for (i = 0; i < BOXES_IN_BOARD; i++)
	{
		if (posIndex == i)
		{
			strToRet += OWN_PIECE;
		}
		else if (i % LEN_OF_ROW_OR_COLUMN == letterOfPos)
		{
			strToRet += board[i];
		}

	}
	std::reverse(strToRet.begin(), strToRet.end());
	return strToRet;
}

string Queen::getXAxis(int pos, string board)
{
	string strToRet = "";
	int i = 0;
	int startOfXAxis = pos - (pos % LEN_OF_ROW_OR_COLUMN);

	for (i = startOfXAxis; i < startOfXAxis + LEN_OF_ROW_OR_COLUMN; i++)
	{
		if (pos == i)
		{
			strToRet += OWN_PIECE;
		}
		else
		{
			strToRet += board[i];
		}
	}
	return strToRet;
}

bool Queen::checkAxis(int from, int to, string board, string axis)
{
	int i = 0;
	bool isLegal = true;

	if (from < to)
	{
		for (i = from; (i < to) && (isLegal == true); i++)
		{
			if (axis[i] != EMPTY)
			{
				if (axis[i] == OWN_PIECE)
				{
					isLegal = true;
				}
				else
				{
					isLegal = false;
				}
			}
		}
	}
	else
	{
		for (i = from; (i > to) && (isLegal == true); i--)
		{
			if ((axis[i] != OWN_PIECE) && (axis[i] != EMPTY))
			{
				isLegal = false;
			}
		}
	}

	return isLegal;
}

bool Queen::checkIfLegalMoveLikeBishop(string from, string to, string board)
{
	int letterFrom = from[0] - ASCII_SMALL_A;
	int letterTo = to[0] - ASCII_SMALL_A;
	int intFrom = int(from[1]) - ASCII_0;
	int intTo = int(to[1]) - ASCII_0;
	bool isLegalMove = false;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));
	int i = 0;
	int indexInLeftFrom = NOTHING;
	int indexInRightFrom = NOTHING;
	int indexInLeftTo = NOTHING;
	int indexInRightTo = NOTHING;

	string leftDiagonal = getLeftDiagonal(from, toIndex, board);
	for (i = 0; i < leftDiagonal.length(); i++)
	{
		if (leftDiagonal[i] == OWN_PIECE)
		{
			indexInLeftFrom = i;
		}
		else if (leftDiagonal[i] == PIECE_TO_REACH)
		{
			indexInLeftTo = i;
		}
	}
	string rightDiagonal = getRightDiagonal(from, toIndex, board);

	for (i = 0; i < rightDiagonal.length(); i++)
	{
		if (rightDiagonal[i] == OWN_PIECE)
		{
			indexInRightFrom = i;
		}
		else if (rightDiagonal[i] == PIECE_TO_REACH)
		{
			indexInRightTo = i;
		}
	}

	if (indexInLeftTo != NOTHING)
	{
		isLegalMove = checktDiagonal(indexInLeftFrom, indexInLeftTo, board, leftDiagonal);
	}
	else if (indexInRightTo != NOTHING)
	{
		isLegalMove = checktDiagonal(indexInRightFrom, indexInRightTo, board, rightDiagonal);
	}


	return isLegalMove;
}

string Queen::getLeftDiagonal(string pos, int posTo, string board)
{
	string strToRet = "";
	int i = 0;
	int j = 0;
	int letterOfPos = pos[0] - ASCII_SMALL_A;
	int posIndex = (int(pos[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(pos[1]) - ASCII_0)));
	int boardI = 0;
	char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN] = { 0 };
	int ownJ = pos[0] - ASCII_SMALL_A;
	int ownI = (LEN_OF_ROW_OR_COLUMN - (int(pos[1]) - ASCII_0));
	boardI = i * LEN_OF_ROW_OR_COLUMN + j;

	//orginize the board in a 2d array
	for (i = 0; i < LEN_OF_ROW_OR_COLUMN; i++)
	{
		for (j = 0; j <= ownJ; j++)
		{
			boardI = i * LEN_OF_ROW_OR_COLUMN + j;
			board2D[i][j] = board[boardI];
			if ((ownJ - j == ownI - i) || (-1 * (ownJ - j) == ownI - i))
			{
				if (board2D[i][j] == this->_role)
				{
					strToRet += OWN_PIECE;
				}
				else if (boardI == posTo)
				{
					strToRet += PIECE_TO_REACH;
				}
				else
				{
					strToRet += board2D[i][j];
				}
			}
		}
	}



	return strToRet;
}

string Queen::getRightDiagonal(string pos, int posTo, string board)
{
	string strToRet = "";
	int i = 0;
	int j = 0;
	int letterOfPos = pos[0] - ASCII_SMALL_A;
	int posIndex = (int(pos[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(pos[1]) - ASCII_0)));
	int boardI = 0;
	char board2D[LEN_OF_ROW_OR_COLUMN][LEN_OF_ROW_OR_COLUMN] = { 0 };
	int ownJ = pos[0] - ASCII_SMALL_A;
	int ownI = (LEN_OF_ROW_OR_COLUMN - (int(pos[1]) - ASCII_0));
	boardI = i * LEN_OF_ROW_OR_COLUMN + j;

	//orginize the board in a 2d array
	for (i = 0; i < LEN_OF_ROW_OR_COLUMN; i++)
	{
		for (j = ownJ; j < LEN_OF_ROW_OR_COLUMN; j++)
		{
			boardI = i * LEN_OF_ROW_OR_COLUMN + j;
			board2D[i][j] = board[boardI];
			if ((j - ownJ == ownI - i) || (-1 * (j - ownJ) == ownI - i))
			{
				if (board2D[i][j] == this->_role)
				{
					strToRet += OWN_PIECE;
				}
				else if (posTo == boardI)
				{
					strToRet += PIECE_TO_REACH;
				}
				else
				{
					strToRet += board2D[i][j];
				}
			}
		}
	}

	return strToRet;
}

bool Queen::checktDiagonal(int from, int to, string board, string diagonal)
{
	int i = 0;
	bool isLegal = true;

	if (from < to)
	{
		for (i = from; (i < to) && (isLegal == true); i++)
		{
			if (diagonal[i] != EMPTY)
			{
				if (diagonal[i] == OWN_PIECE)
				{
					isLegal = true;
				}
				else
				{
					isLegal = false;
				}
			}
		}
	}
	else
	{
		for (i = from; (i > to) && (isLegal == true); i--)
		{
			if ((diagonal[i] != OWN_PIECE) && (diagonal[i] != EMPTY))
			{
				isLegal = false;
			}
		}
	}

	return isLegal;
}
