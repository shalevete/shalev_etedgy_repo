#pragma once

enum answerToFrontend { res_goodMove, res_goodMoveAndChess, res_ErrNoCurrPiece, res_ErrCurrPieceInDst, res_ErrOwnChess, res_ErrIllegalIndex, res_ErrIllegalMove, res_ErrSamePlaces, res_goodMoveWin, res_goodMoveAndWChess, res_goodMoveAndBChess};

//pieces
#define BLACK_KING 'k'
#define WHITE_KING 'K'
#define BLACK_QUEEN 'q'
#define WHITE_QUEEN 'Q'
#define BLACK_ROOK 'r'
#define WHITE_ROOK 'R'
#define BLACK_KNIGHT 'n'
#define WHITE_KNIGHT 'N'
#define BLACK_BISHOP 'b'
#define WHITE_BISHOP 'B'
#define BLACK_PAWN 'p'
#define WHITE_PAWN 'P'
#define EMPTY '#'
#define OWN_PIECE '!'
#define PIECE_TO_REACH '@'

//ASCII
#define ASCII_SMALL_A 'a'
#define ASCII_SMALL_Z 'z'
#define ASCII_BIG_A 'A'
#define ASCII_BIG_Z 'Z'
#define ASCII_SMALL_H 'h'
#define ASCII_0 '0'
#define ASCII_1 '1'

//board
#define LEN_OF_ROW_OR_COLUMN 8
#define START_OF_BOARD "a8"
#define BOXES_IN_BOARD 64
#define NOTHING -1
#define START_BOARD_MSG "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"
/*
rnbkqbnr
pppppppp
########
########
########
########
PPPPPPPP
RNBKQBNR
*/

//chessCheck
#define KEEP_CHECK_CHESS (isChess == res_goodMove)
