#include "Pipe.h"
#include "Game.h"
#include <iostream>
#include <thread>

using std::string;
using std::cout;

void main()
{
	srand(time_t(NULL));

	try {
		Game chess;
		chess.startGame();
		chess.playGame();
	}
	catch (pipeErrors &e)
	{
		if (e = PE_cantConnect)
		{
			cout << "can't connect!!!";
		}
	}
	catch (...)
	{
		printf("caught a bad exception. continuing as usual\n");
	}

	system("pause");
}