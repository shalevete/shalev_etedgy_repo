#pragma once
#include <iostream>
#include <string>
#include "Pipe.h"
#include "Defines.h"
#include "Board.h"
#include "Player.h"

enum pipeErrors { PE_cantConnect };

using  std::string;

class Game
{
public:
	Game();
	~Game();

	//startGame & endGame
	void startGame();
	void playGame();
	void endGame();
	string whatHappened(answerToFrontend TurnValid);

	//Pipe
	void connectToPipe();	
	bool isPipeConnected();	
	void sendMsg(string boardMsg);	
	string getMsg();	

	//turn
	void setTurn(bool _isWhite);	
	bool getTurn();
	void nextTurn();	


private:
	Board _gameBoard;
	Player _white;
	Player _black;
	Pipe _p;
	bool _isPipeConnected;
	bool _isWhiteTurn;
	string _msgFromGraphics;
	string _msgToSend;

};
