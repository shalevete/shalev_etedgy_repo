#pragma once
#include <iostream>
#include <string>
#include "Defines.h"
#include "ChessPiece.h"

using std::string;

class Rook : public ChessPiece
{
public:
	Rook(bool isWhite);
	~Rook();

	//move
	virtual string move( string to, string board);

	//answer to fronted	
	virtual answerToFrontend canMove(string from, string to, string board);
	bool checkIfLegalMove(string from, string to, string board);
	string getYAxis(string pos, string board);
	string getXAxis(int pos, string board);
	bool checkAxis(int from, int to, string board, string axis);
};
 