#include "Pawn.h"

Pawn::Pawn(bool isWhite) : ChessPiece(isWhite, BLACK_PAWN)
{
	this->_has_moved = false;
}

Pawn::~Pawn()
{

}

answerToFrontend Pawn::canMove(string from, string to, string board)
{
	answerToFrontend answer = res_goodMove;


	if (from != this->_pos)
	{
		answer = res_ErrNoCurrPiece;
	}
	else if (from == to)
	{
		answer = res_ErrSamePlaces;
	}
	else if (!checkIfLegalMove(from, to, board))
	{
		answer = res_ErrIllegalMove;
	}

	return answer;
}

bool Pawn::checkIfLegalMove(string from, string to, string board)
{
	int letterFrom = from[0] - ASCII_SMALL_A;
	int letterTo = to[0] - ASCII_SMALL_A;
	int intFrom = int(from[1]) - ASCII_0;
	int intTo = int(to[1]) - ASCII_0;
	bool isLegalMove = false;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	bool isWhitePawn = (board[fromIndex] == WHITE_PAWN);
	
	if (isWhitePawn)
	{
		if ((letterTo == letterFrom) && (intTo - intFrom == 1) && (board[toIndex] == EMPTY))
		{
			isLegalMove = true;
		}
		else if ((!this->_has_moved) && ((letterTo == letterFrom) && (intTo - intFrom == 2) && (board[toIndex] == EMPTY)))
		{
			isLegalMove = true;
		}
		else if (((letterTo - letterFrom == -1) ||(letterTo - letterFrom == 1)) && (intTo - intFrom == 1) && (board[toIndex] != EMPTY))
		{
			isLegalMove = true;
		}
	}
	else
	{
		if ((letterTo == letterFrom) && (intTo - intFrom == -1) && (board[toIndex] == EMPTY))
		{
			isLegalMove = true;
		}
		else if ((!this->_has_moved) && ((letterTo == letterFrom) && (intTo - intFrom == -2) && (board[toIndex] == EMPTY)))
		{
			isLegalMove = true;
		}
		else if (((letterTo - letterFrom == -1) || (letterTo - letterFrom == 1)) && (intTo - intFrom == -1) && (board[toIndex] != EMPTY))
		{
			isLegalMove = true;
		}

	}

	return isLegalMove;
}

string Pawn::move(string to, string board)
{
	this->_has_moved = true;
	string from = this->_pos;
	int fromIndex = (int(from[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(from[1]) - ASCII_0)));
	int toIndex = (int(to[0]) - ASCII_SMALL_A) + (LEN_OF_ROW_OR_COLUMN * (LEN_OF_ROW_OR_COLUMN - (int(to[1]) - ASCII_0)));

	board[toIndex] = board[fromIndex];
	board[fromIndex] = EMPTY;

	this->_pos = to;

	return board;
}