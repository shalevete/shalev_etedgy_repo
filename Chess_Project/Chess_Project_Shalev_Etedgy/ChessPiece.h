#pragma once
#include <iostream>
#include <string>
#include "Defines.h"

using std::string;

class ChessPiece
{
public:
	ChessPiece(const bool isWhite, const char role);
	~ChessPiece();

	//move
	virtual answerToFrontend canMove(string from, string to, string board) = 0;
	virtual string move(string to, string board) = 0;

	//eat
	void eatMe();

	//position
	void setPosition(string pos);
	string getPosition();	

	//role
	void setRole(char role);
	char getRole();	
	void changeRoleColor(const bool isWhite);

	//king
	virtual answerToFrontend checkChess(string board, int ownPiece);
protected:
	char _role;
	string _pos;
};
