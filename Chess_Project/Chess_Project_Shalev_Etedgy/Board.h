#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Defines.h"
#include "ChessPiece.h"
#include "Rook.h"
#include "Bishop.h"
#include "King.h"
#include "Pawn.h"
#include "Queen.h"
#include "Knight.h"
#include "Empty.h"

using std::string;

class Board
{
public:
	Board();
	~Board();

	//orginize the board message
	void organizeBoard(bool isWhiteTurn);	
	void startBoardMsg();	
	string getBoardMsg(bool isWhiteTurn);	
	void removeAllPieces();
	void removePiece(int index);

	bool isPieceColorGood(char source, bool isWhiteTurn);
	bool goodIndex(string index);

	//addPieces
	void addPieces(string boardLayout);	
	void addRook(string pos, bool isWhite);
	void addBishop(string pos, bool isWhite);
	void addKing(string pos, bool isWhite);
	void addPawn(string pos, bool isWhite);
	void addQueen(string pos, bool isWhite);
	void addKnight(string pos, bool isWhite);
	void addEmpty(string pos);

	//Move
	answerToFrontend checkTurnValid(string moveStr, bool isWhiteTurn);	
	void callMove(string moveStr);

	void printBoard();	
	int findIndex(string boxIndex);
	int findIndexByRoleInMsg(char role);
	int findIndexByRole(char role);

	answerToFrontend checkChess(bool checkWhite);

private:
	string _boardMsg;
	bool _isValidTurn;
	std::vector<ChessPiece*> _allPieces;
	int _indexToEat;
	bool _needToEat;

};