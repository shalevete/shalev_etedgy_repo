#include "NameErrorException.h"

NameErrorException::NameErrorException(std::string name)
{
	this->_name = name;
}

NameErrorException::~NameErrorException()
{
}

const char* NameErrorException::what() const throw()
{
	std::string strToReturn = ("NameError : name " + this->_name + " is not defined");
	const char* cstr = strToReturn.c_str();
	return cstr;
}