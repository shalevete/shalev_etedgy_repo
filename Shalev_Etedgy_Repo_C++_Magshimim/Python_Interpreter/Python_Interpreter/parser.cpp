#include "parser.h"
#include <iostream>

std::unordered_map<std::string, Type*> Parser::_variables;

Type* Parser::parseString(std::string str) throw()
{
	Type* varFromStr;
	if (str.length() > 0)
	{
		if ((str[0] == '\t') || (str[0] == ' '))	//if command starts with tab or space - throw exception
		{
			throw IndentationException();
		}

		Helper::removeLeadingZeros(str);
		varFromStr = getType(str);
		if (varFromStr != nullptr)
		{
			return varFromStr;
		}
		else
		{
			if (makeAssignment(str))
			{
				varFromStr = new Void();
				varFromStr->setIsTemp(true);
				return varFromStr;
			}
		}
		std::cout << str << std::endl;
	}

	return NULL;
}

Type* Parser::getType(const std::string &str)
{
	Type* typeOfStr;
	if (Helper::isInteger(str))
	{
		typeOfStr = new Integer(stoi(str));
	}
	else if (Helper::isBoolean(str))
	{
		typeOfStr = new Boolean((str == "True"));
	}
	else if (Helper::isString(str))
	{
		typeOfStr = new String(str);
	}
	else
	{
		typeOfStr = NULL;
		return typeOfStr;
	}
	typeOfStr->setIsTemp(true);
	return typeOfStr;
}

bool Parser::isLegalVarName(const std::string& str)
{
	int i = 0;
	bool isLegal = true;
	for (i = 0; ((i < str.size()) && (isLegal)); i++)
	{
		if ((!(Helper::isDigit(str[i]))) && (!(Helper::isLetter(str[i]))) && (!(Helper::isUnderscore(str[i]))))
		{
			isLegal = false;
		}
	}
	return isLegal;
}

bool Parser::makeAssignment(const std::string& str)
{
	std::string nameOfVar = "";
	std::string valueOfVar = "";
	std::size_t whereisEqualSign = 0;
	Type* typeOfVar;

	if (checkIfgoodAmountOfEqual(str))
	{
		whereisEqualSign = str.find("=");
		nameOfVar = str.substr(0, whereisEqualSign);
		valueOfVar = str.substr(whereisEqualSign +1 , str.size());
		Helper::rtrim(nameOfVar);
		Helper::rtrim(valueOfVar);
		Helper::ltrim(valueOfVar);

		if (isLegalVarName(nameOfVar))
		{
			typeOfVar = getType(valueOfVar);
			if (typeOfVar != nullptr)
			{
				Parser::_variables.insert(std::make_pair(nameOfVar, typeOfVar));
			}
			else
			{
				throw SyntaxException();
			}
		}
		else
		{
			throw SyntaxException();
		}
	}
	else
	{
		return false;
	}
	return true;
}


bool Parser::checkIfgoodAmountOfEqual(std::string str)
{
	int i = 0;
	int equalsFound = 0;	
	for (i = 0; i < str.size(); i++)
	{
		if (str[i] == '=')
		{
			equalsFound++;
		}
	}
	if (equalsFound == 1)
	{
		return true;
	}
	else
	{
		return false;
	}
}