#include "type.h"

Type::Type()
{
	this->_isTemp = false;
}

Type::~Type()
{
}

//getters

bool Type::getIsTemp() const
{
	return this->_isTemp;
}

//setters

void Type::setIsTemp(const bool isTemp)
{
	this->_isTemp = isTemp;
}