#ifndef VOID_H
#define VOID_H

#include "type.h"

class Void : public Type
{
public:
	Void();
	~Void();

	//virtual functions
	bool isPrintable() const;
	std::string toString() const;

private:

};

#endif // VOID_H