#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"

class Integer : public Type
{
public:
	Integer(int value);
	~Integer();

	//virtual functions
	bool isPrintable() const;
	std::string toString() const;

private:
	int _value;

};

#endif // INTEGER_H