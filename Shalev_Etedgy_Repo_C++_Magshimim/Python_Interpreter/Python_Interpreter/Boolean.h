#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"

class Boolean : public Type
{
public:
	Boolean(bool value);
	~Boolean();

	//virtual functions
	bool isPrintable() const;
	std::string toString() const;

private:
	bool _value;

};

#endif // BOOLEAN_H