#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence
{
public:
	String(std::string str);
	~String();

	//virtual functions
	bool isPrintable() const;
	std::string toString() const;

private:
	std::string _str;
};

#endif // STRING_H