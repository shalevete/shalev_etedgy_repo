#ifndef TYPE_H
#define TYPE_H

#include <iostream>
#include <string>

class Type
{
public:
	Type();
	~Type();

	//getters
	bool getIsTemp() const;

	//setters
	void setIsTemp(const bool isTemp);

	//pure virtual functions
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;

private:
	bool _isTemp;
};

#endif //TYPE_H
