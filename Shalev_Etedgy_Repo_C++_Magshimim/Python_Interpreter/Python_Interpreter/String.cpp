#include "String.h"

String::String(std::string str)
{
	this->_str = str;
}

String::~String()
{
}

//virtual functions

bool String::isPrintable() const
{
	return true;
}

std::string String::toString() const
{
	std::string strToReturn;
	strToReturn = this->_str;
	int i = 1;
	bool found = false;
	for (i = 1; ((i < strToReturn.size()-1) && (!found)); i++)
	{
		if (strToReturn[i] == '\'')
		{
			found = true;
		}
	}
	if (found)
	{
		strToReturn[0] = '\"';
		strToReturn[strToReturn.size()-1] = '\"';
	}
	else
	{
		strToReturn[0] = '\'';
		strToReturn[strToReturn.size()-1] = '\'';
	}
	
	return strToReturn;
}
