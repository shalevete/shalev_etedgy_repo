#include "Boolean.h"

Boolean::Boolean(bool value)
{
	this->_value = value;
}

Boolean::~Boolean()
{

}

//virtual functions

bool Boolean::isPrintable() const
{
	return true;
}

std::string Boolean::toString() const
{
	return ((this->_value) ? "True" : "False");
}
