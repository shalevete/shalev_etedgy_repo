#ifndef PARSER_H
#define PARSER_H

#include "InterperterException.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "NameErrorException.h"
#include "type.h"
#include "Helper.h"
#include "Integer.h"
#include "void.h"
#include "boolean.h"
#include "String.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>

class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(const std::string &str);

private:
	static bool isLegalVarName(const std::string& str);
	static bool makeAssignment(const std::string& str);
	static Type* getVariableValue(const std::string &str);
	static bool checkIfgoodAmountOfEqual(std::string str);

	static std::unordered_map<std::string, Type*> _variables;
	 
};

#endif //PARSER_H
