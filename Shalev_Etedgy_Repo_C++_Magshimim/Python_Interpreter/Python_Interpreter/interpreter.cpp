#include "type.h"
#include "InterperterException.h"
#include "SyntaxException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Shalev"


int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;
	Type* variable;
	SyntaxException eSyntax;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		// prasing command
		try
		{
			variable = Parser::parseString(input_string);
			if (variable != nullptr)
			{
				if (variable->isPrintable())
				{
					std::cout << variable->toString() << std::endl;
				}
				if (variable->getIsTemp())
				{
					delete variable;
				}
				else
				{
					throw(eSyntax);
				}
			}
			else
			{
				delete variable;
			}
		}
		catch (IndentationException e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (SyntaxException e)
		{
			std::cout << e.what() << std::endl;
		}

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}


