#include "threads.h"
#include <fstream>

void primesExecute(int from, int to);

int main()
{
	//a
	call_I_Love_Threads();

	//b
	vector<int> primes1;
	getPrimes(58, 100, primes1);
	printVector(primes1);

	vector<int> primes3 = callGetPrimes(93, 289);
	printVector(primes3);

	//c
	//primesExecute(0, 1000);
	//primesExecute(0, 100000);
	//primesExecute(0, 1000000);

	callWritePrimesMultipleThreads(1, 1000, "E:/f.txt", 2);
	callWritePrimesMultipleThreads(1, 100000, "E:/f.txt", 2);
	callWritePrimesMultipleThreads(1, 1000000, "E:/f.txt", 2);

	system("pause");
	return 0;

} 

void primesExecute(int from, int to)
{
	cout << "primes from " << from << " to " << to << endl;
	vector<int> primes3 = callGetPrimes(from, to);
	printVector(primes3);
}