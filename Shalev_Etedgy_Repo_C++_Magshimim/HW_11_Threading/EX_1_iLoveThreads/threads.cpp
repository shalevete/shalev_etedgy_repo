#include "threads.h"


void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	t1.join();
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int i = 0;
	for (i = begin; i < end; i++)
	{
		if (checkIfPrime(i))
		{
			primes.push_back(i);
		}
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primesVec;
	std::thread t2(getPrimes, begin, end, std::ref(primesVec));
	t2.join();
	return primesVec;
}


bool checkIfPrime(int num)
{
	bool isPrime = true;
	int i = 0;

	if (num <= 1)
	{
		isPrime = false;
	}
	else
	{
		for (i = 2; i < num; i++)
		{
			if (num%i == 0)
			{
				isPrime = false;
			}
		}
	}

	return isPrime;
}

void printVector(vector<int> primes)
{
	vector<int>::iterator it;
	for (it = primes.begin(); it != primes.end(); ++it)
	{
		cout << *it << endl;
	}
}


void writePrimesToFile(int begin, int end, ofstream& file)
{
	int i = 0;
	for (i = begin; i < end; i++)
	{
		if (checkIfPrime(i))
		{
			file << i << endl;
		}
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	vector<thread> threads;
	int i = 0;
	int newEnd = 0;
	int newBegin = begin;
	
	ofstream myfile;
	myfile.open(filePath);

	if (myfile.is_open())
	{
		if (N <= 0)
		{
			cerr << ERR_N_IS_0 << endl;
		}
		else
		{
			for (int i = 1; i <= N; i++)
			{
				newEnd = end/N * i;
				threads.push_back(thread(writePrimesToFile, begin, end, std::ref(myfile)));
				newBegin = newBegin + newEnd;
			}

			vector<thread>::iterator it;
			for (it = threads.begin(); it != threads.end(); ++it)
			{
				it->join();
			}
		}

		myfile.close();
	}
	else
	{
		cerr << ERR_FILE_NOT_OPEN << endl;
	}
}