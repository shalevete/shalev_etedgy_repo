#include "readersWriters.h"

readersWriters::readersWriters(std::string fileName)
{
	this->_locker = std::unique_lock<std::mutex>(this->_mu);
	this->_readersNumber = 0;
	this->_writersNumber = 0;
	this->_fileName = fileName;
}

void readersWriters::readLock()
{
	if (this->_writersNumber > 0)
	{
		cout << "readLock wait";
		this->_condR.wait(this->_locker);
		cout << "readLock finish wait";
	}
	this->_readersNumber++;
	cout << "_readersNumberInc " << this->_readersNumber;
}

void readersWriters::writeLock()
{
	if ((this->_readersNumber > 0) || (this->_writersNumber > 0))
	{
		cout << "writeLock wait";
		this->_condW.wait(this->_locker);
		cout << "writeLock finish wait";
	}
	this->_writersNumber++;
	cout << "_writersNumberInc " << this->_readersNumber;
}

void readersWriters::readUnlock()
{
	this->_readersNumber--;
	cout << "_readersNumberDec " << this->_readersNumber;
	if ((_readersNumber == 0) && (this->_writersNumber == 0))
	{
		this->_condW.notify_one();
		cout << "_condW notify_one";
	}
}

void readersWriters::writeUnlock()
{
	this->_writersNumber--;
	cout << "_writersNumberDec " << this->_readersNumber;
	if (this->_writersNumber == 0)
	{
		this->_condR.notify_all();
		cout << "_condR notify_all";
	}
}

std::string readersWriters::readLine(int lineNumber)
{
	bool eofFlag = false;
	int i = 0;
	readLock();
	string line;
	std::ifstream myfile(this->_fileName);

	if (myfile.is_open())
	{
		for (i = 1; !eofFlag && i < lineNumber; i++)
		{
			if (!getline(myfile, line))
			{
				eofFlag = true;
			}
		}
		if (eofFlag)
		{
			std::cerr << ERR_LINE_DOESNT_EXISTS << endl;
		}
		else
		{
			cout << this->_readersNumber << " read: " << line << endl;
		}
		myfile.close();
	}
	else
	{
		std::cerr << ERR_CANT_OPEN_FILE << endl;
	}
	readUnlock();
	return line;
}

void readersWriters::WriteLine(int lineNumber, std::string newLine)
{
	writeLock();
	lineNumber--;
	int c = 0;
	int currentLine = 0;
	std::fstream myfile(this->_fileName);

	if (myfile.is_open())
	{
		while ((currentLine < lineNumber) && (c != EOF))
		{
			myfile.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			++currentLine;
			c = myfile.peek();
		}
		if (c == EOF)
		{
			if (currentLine < lineNumber)
			{
				std::cerr << ERR_INVALID_LINE << endl;
			}
			else if (currentLine == lineNumber)
			{
				myfile.close();
				myfile.open(this->_fileName, std::ios_base::app);
				myfile << newLine;
			}
		}
		else
		{
			myfile.seekp(myfile.tellg());
			myfile << newLine;
		}

		myfile.close();
	}
	else
	{
		std::cerr << ERR_CANT_OPEN_FILE << endl;
	}
	writeUnlock();
}

/*
void readersWriters::callReadAndWrite()
{
	std::thread t1(&readersWriters::readLine, 2);
	std::thread t2(&readersWriters::readLine, 3);
	std::thread t3(&readersWriters::readLine, 2);
	std::thread t4(&readersWriters::WriteLine, 2, "hello");
	std::thread t5(&readersWriters::readLine, 2);
	std::thread t6(&readersWriters::WriteLine, 5, "world");
	std::thread t7(&readersWriters::WriteLine, 3, "abc");
	std::thread t8(&readersWriters::readLine, 3);
	std::thread t9(&readersWriters::readLine, 4);
	std::thread t10(&readersWriters::WriteLine, 20, "abc");
	std::thread t11(&readersWriters::readLine, 9);
	std::thread t12(&readersWriters::WriteLine, 9, "hello world");
	std::thread t13(&readersWriters::readLine, 9);
	std::thread t14(&readersWriters::readLine, 20);

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	t6.join();
	t7.join();
	t8.join();
	t9.join();
	t10.join();
	t11.join();
	t12.join();
	t13.join();
	t14.join();
}
*/