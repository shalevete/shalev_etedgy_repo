#pragma once
#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <string>

#define FILE_NAME "E://f.txt"
#define ERR_LINE_DOESNT_EXISTS "the requested line doesn't exist"
#define ERR_CANT_OPEN_FILE "Unable to open file"
#define ERR_INVALID_LINE "the line you are trying to write to is invalid"

using std::string;
using std::cout;
using std::endl;
using std::cin;

class readersWriters {
private:
	std::mutex _mu;
	std::unique_lock<std::mutex> _locker;
	std::condition_variable _condW;
	std::condition_variable _condR;
	int _readersNumber;
	int _writersNumber;
	std::string _fileName;

public:
	readersWriters(std::string fileName);
	void readLock();
	void writeLock();
	void readUnlock();
	void writeUnlock();
	std::string readLine(int lineNumber); //lineNumber - line number to read
	void WriteLine(int lineNumber, std::string newLine);//lineNumber - line number to write 

	//void callReadAndWrite();
};