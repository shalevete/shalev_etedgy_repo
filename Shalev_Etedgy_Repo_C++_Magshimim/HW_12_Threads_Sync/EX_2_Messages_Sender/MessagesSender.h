#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include<set>
#include<list>
#include <thread>
#include <mutex>
#include <condition_variable>

#define FILE_NAME "E:/data.txt"
#define ERR_INVALID_INPUT "invalid input! try again"
#define MSG_ASK_FOR_USERNAME_INPUT "enter username: "
#define MSG_USER_IN_SET "user already exists in the system"
#define MSG_USER_NOT_IN_SET "user not found"

using std::string;
using std::cout;
using std::endl;
using std::cin;

enum messagesSenderOptions { MSO_signIn = 1 , MSO_signOut, MSO_connectedUsers, MSO_exit };

class MessagesSender
{
public:
	MessagesSender();
	~MessagesSender();

	void menu();

private:
	std::set<std::string> _connectedUsers;
	std::list<string> _messages;

	void signIn();
	void signOut();
	void printConnectedUsers();

	void printMenu();
	messagesSenderOptions getChoiceFromUser();
	void whatToDo(messagesSenderOptions choice);

};

void readDataFromFile(std::list<string>& messages);
void sendMsgToUsers(std::list<string>& messages, std::set<std::string>& users); 