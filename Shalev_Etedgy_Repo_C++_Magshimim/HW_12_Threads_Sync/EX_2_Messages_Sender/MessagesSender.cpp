#include "MessagesSender.h"

std::mutex mMessages;
std::condition_variable cvMessages;

bool isFinished;
bool usersEmpty;

MessagesSender::MessagesSender()
{
}

MessagesSender::~MessagesSender()
{
}

void MessagesSender::menu()
{
	usersEmpty = true;
	isFinished = false;
	std::thread t(readDataFromFile, std::ref(this->_messages));
	std::thread t2(sendMsgToUsers, std::ref(this->_messages), std::ref(this->_connectedUsers));
	messagesSenderOptions choice = MSO_signIn;
	while (choice != MSO_exit)
	{
		printMenu();
		choice = getChoiceFromUser();
		whatToDo(choice);
	}
	cout << " Loading . . . ";
	isFinished = true;
	cvMessages.notify_all();
	t2.join();
	t.join();
	cout << endl;
}


void MessagesSender::printMenu()
{
	cout << "choose option: "	 << endl
		 << "1.	Signin"			 << endl
		 << "2.	Signout"		 << endl
		 << "3.	Connected Users" << endl
		 << "4.	exit"			 << endl;
}

messagesSenderOptions MessagesSender::getChoiceFromUser()
{
	int ans = 0;
	while ((ans < 1) || (ans > 4))
	{
		cin >> ans;
		if (cin.fail())
		{
			cin.clear();
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cerr << ERR_INVALID_INPUT << endl;
		}
		if ((ans < 1) || (ans > 4))
		{
			std::cerr << ERR_INVALID_INPUT << endl;
		}
	}
	return (messagesSenderOptions)(ans);
}

void MessagesSender::whatToDo(messagesSenderOptions choice)
{
	switch (choice)
	{
	case MSO_signIn:
		signIn();
		break;
	case MSO_signOut:
		signOut();
		break;
	case MSO_connectedUsers:
		printConnectedUsers();
		break;
	case MSO_exit:
		break;
	default:
		break;
	}
}


void MessagesSender::signIn()
{
	string userName;

	cout << MSG_ASK_FOR_USERNAME_INPUT;
	cin >> userName;
	if (this->_connectedUsers.count(userName) != 0)
	{
		cout << MSG_USER_IN_SET << endl;
	}
	this->_connectedUsers.insert(userName);
	usersEmpty = false;
	system("pause");
	system("CLS");
}

void MessagesSender::signOut() 
{
	string userName;

	cout << MSG_ASK_FOR_USERNAME_INPUT;
	cin >> userName;
	if (this->_connectedUsers.count(userName) == 0)
	{
		cout << MSG_USER_NOT_IN_SET << endl;
	}
	else
	{
	this->_connectedUsers.erase(userName);
	}
	system("pause");
	system("CLS");
}

void MessagesSender::printConnectedUsers()
{
	std::cout << "connected users contains:" << endl;
	std::set<string>::iterator it;
	for (it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); ++it)
	{
		std::cout << *it << endl;
	}
	system("pause");
	system("CLS");
}


void readDataFromFile(std::list<string>& messages)
{
	std::this_thread::sleep_for(std::chrono::seconds(1));
	char message[256] = { 0 };
	while (!isFinished)
	{
		std::fstream dataFile;
		dataFile.open(FILE_NAME);
		if (dataFile.is_open())
		{
			if (dataFile.peek() != std::ifstream::traits_type::eof())
			{
				while (dataFile.getline(message, 256))
				{
					messages.push_back((string)(message)+"\n");
					memset(message, 0, 256);	//empty the array
				}
				std::lock_guard<std::mutex> lk(mMessages);
				cvMessages.notify_all();
			}
			dataFile.close();
		}
		
		//clear data in the file
		std::ofstream dataFileToDelete;
		dataFileToDelete.open(FILE_NAME, std::ofstream::out | std::ofstream::trunc);
		dataFileToDelete.close();

		//wait 60 seconds
		std::this_thread::sleep_for(std::chrono::seconds(60));
	}
}

void sendMsgToUsers(std::list<string>& messages, std::set<std::string>& users)
{
	std::list<string>::iterator it;
	std::set<string>::iterator it1;
	std::ofstream output;

	while (!isFinished)
	{
		output.open("output.txt", std::fstream::in | std::fstream::out | std::fstream::app);
		if (output.is_open())
		{
			std::unique_lock<std::mutex> lk(mMessages);
			cvMessages.wait(lk);

			for (it = messages.begin(); it != messages.end(); ++it)
			{
				for (it1 = users.begin(); it1 != users.end(); ++it1)
				{
					output << *it1 << ": " << *it;
				}
			}
			if (!usersEmpty)
			{
				messages.clear();
			}

		}
		output.close();
	}

}