#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <mutex>

#define ERR_FILE_NOT_OPEN "can't open file"
#define ERR_N_IS_0 "n can't be 0"

using namespace std;

void writePrimesToFile(int begin, int end, ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N);


bool checkIfPrime(int num);
