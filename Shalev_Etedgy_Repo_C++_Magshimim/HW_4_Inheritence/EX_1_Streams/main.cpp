#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"

int main(int argc, char **argv)
{
	void(*pf)(FILE *);
	pf = &endline;
	
	//test OutStream
	OutStream myOutStream;
	myOutStream << "I am the Doctor and I'm " << 1500 << " years old" << pf;


	//test FileStream
	//FileStream myFileStream("txtFile.txt");
	//myFileStream << "I am the Doctor and I'm " << 1500 << " years old" << pf;


	//test OutStreamEncrypted
	//OutStreamEncrypted myOutStreamEncrypted;
	//myOutStreamEncrypted.set_key(3);
	//myOutStreamEncrypted << "I am the Doctor and I'm " << 1500 << " years old" << pf;


	//test Logger
	Logger myLogger;
	Logger myLogger1;

	myLogger << "a" << 785  << pf;
	myLogger1 << "a" << 540  << pf;
	myLogger << "b" << 456 << pf;
	myLogger << "c" << 15087 << pf;
	myLogger1 << "b" << 150 << pf;
	myLogger << "d" << 150 << pf;
	myLogger1 << "c" << 150 << pf;


	return 0;
}
