#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "OutStreamEncrypted.h"

#define END_OF_STR '\0'
#define MAX_DIGITS_IN_INT 10	
#define MAX_string_len 1000	
#define MIN_CHAR_CYPER 32
#define MAX_CHAR_CYPER 128


OutStreamEncrypted::OutStreamEncrypted()
{
	this->_key = 0;
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	int i = 0;
	char ch = 0;
	char temp_str[MAX_string_len] = "\0";

	for (i = 0; str[i] != END_OF_STR; ++i)
	{
		ch = str[i];
		if ((ch >= MIN_CHAR_CYPER) && (ch <= MAX_CHAR_CYPER))
		{
			temp_str[i] = ch + this->_key;
		}
	}

	OutStream::operator<<(temp_str);

	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char num_str[MAX_DIGITS_IN_INT] = "\0";

	sprintf(num_str, "%d", num);

	OutStreamEncrypted::operator<<(num_str);

	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE *))
{
	pf(_fileToPrint);
	return *this;
}


void OutStreamEncrypted::set_key(const int key)
{
	this->_key = key;
}