#include <stdio.h>
#include "OutStream.h"

OutStream::OutStream()
{
	_fileToPrint = stdout;
}

OutStream::~OutStream()
{
}

OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->_fileToPrint, "%s", str);
	return *this;
}

OutStream& OutStream::operator<<(int num)
{
	fprintf(this->_fileToPrint, "%d", num);
	return *this;
}

OutStream& OutStream::operator<<(void(*pf)(FILE *))
{
	pf(stdout);
	return *this;
}


void endline(FILE * fileToPrint)
{
	fprintf(fileToPrint, "\n");
}
