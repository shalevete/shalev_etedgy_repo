#include <stdio.h>
#include "Logger.h"

Logger::Logger()
{
	_logCounter = 0;
}

Logger::~Logger()
{
}

void Logger::setStartLine()
{
	if (!this->_startLine)
	{
		this->_startLine = true;
		_logCounter++;
		this->os << _logCounter << (LOG_MSG);
	}
}

Logger& operator<<(Logger& l, const char *msg)
{
	l.setStartLine();

	l.os <<(msg);

	return l;
}

Logger& operator<<(Logger& l, int num)
{
	l.setStartLine();

	l.os << (num);

	return l;
}

Logger& operator<<(Logger& l, void(*pf)(FILE *))
{
	l._startLine = false;
	pf = &endline;
	l.os << pf;

	return l;
}