#pragma once

#include "OutStream.h"
#include <stdio.h>

class OutStreamEncrypted : public OutStream
{

public:
	OutStreamEncrypted();
	~OutStreamEncrypted();

	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE *));

	// setters
	void set_key(const int key);


private:
	int _key;
};