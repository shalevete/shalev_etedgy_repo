#pragma once

#include "OutStream.h"
#include <stdio.h>

#define LOG_MSG "LOG "

class Logger
{
	OutStream os;
	bool _startLine;
	void setStartLine();
	unsigned int _logCounter;
public:
	Logger();
	~Logger();
	friend Logger& operator<<(Logger& l, const char *msg);
	friend Logger& operator<<(Logger& l, int num);
	friend Logger& operator<<(Logger& l, void(*pf)(FILE *));
};
