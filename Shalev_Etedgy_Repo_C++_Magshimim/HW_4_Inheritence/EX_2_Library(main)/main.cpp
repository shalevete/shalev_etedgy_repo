#pragma comment(lib, "../debug/EX_2_Library.lib")

#include "../EX_2_Library/FileStream.h"
#include "../EX_2_Library/OutStream.h"

using namespace msl;


int main()
{
//	void(*pf)(FILE *);
//	pf = &endline;

	//test OutStream
	OutStream myOutStream;
	myOutStream << "I am the Doctor and I'm " << 1500 << " years old" ;


	//test FileStream
	FileStream myFileStream("txtFile.txt");
	myFileStream << "I am the Doctor and I'm " << 1500 << " years old" ;

	return 0;
}
