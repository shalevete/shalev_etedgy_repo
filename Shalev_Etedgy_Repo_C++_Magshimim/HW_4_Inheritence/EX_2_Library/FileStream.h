#pragma once

#include "OutStream.h"
#include <stdio.h>

namespace msl
{
	class FileStream : public OutStream
	{

	public:
		FileStream(const char * filename);
		~FileStream();

		FileStream& operator<<(const char *str);
		FileStream& operator<<(int num);
		FileStream& operator<<(void(*pf)(FILE *));

	};
}
