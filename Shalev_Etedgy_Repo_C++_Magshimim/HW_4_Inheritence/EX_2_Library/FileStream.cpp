#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "FileStream.h"

using namespace msl;


FileStream::FileStream(const char * filename)
{
	_fileToPrint = fopen(filename, "w");
}

FileStream::~FileStream()
{
	fclose(_fileToPrint);
}

FileStream& FileStream::operator<<(const char *str)
{
	OutStream::operator<<(str);

	return *this;
}

FileStream& FileStream::operator<<(int num)
{
	OutStream::operator<<(num);

	return *this;
}

FileStream& FileStream::operator<<(void(*pf)(FILE *))
{
	pf(_fileToPrint);

	return *this;
}
