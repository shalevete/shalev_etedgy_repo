#include"Customer.h"

Customer::Customer(string name)
{
	this->_name = name;
}

Customer::Customer()
{
	this->_name = "";
}

Customer::~Customer()
{

}

double Customer::totalSum() const
{
	double sum = 0;

	set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		sum += it->totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	this->_items.insert(item);
}

void Customer::removeItem(Item item)
{
	this->_items.erase(item);
}

void Customer::setName(const string name)
{
	this->_name = name;
}

string Customer::getName()
{
	return this->_name;
}

set<Item> Customer::getItems()
{
	return this->_items;
}

void Customer::setItems(set<Item> items)
{
	this->_items.clear();
	this->_items = items;
}
