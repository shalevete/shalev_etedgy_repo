#include"Item.h"

Item::Item(string name, string serialNumber, double unitPrice)
{
	this->_name = name;
	if (serialNumber.length() != 5)
	{
		throw(serialNumberNotFive);
	}
	this->_serialNumber = serialNumber;
	this->_count = 1;
	if (unitPrice <= 0)
	{
		throw(IE_unitPriceNeg);
	}
	this->_unitPrice = unitPrice;
}

Item::~Item()
{

}

double Item::totalPrice() const
{
	return (this->_count * this->_unitPrice);
}

bool Item::operator <(const Item& other) const
{
	return (stoi(this->_serialNumber) < stoi(other._serialNumber));
}

bool Item::operator >(const Item& other) const
{
	return (stoi(this->_serialNumber) > stoi(other._serialNumber));
}

bool Item::operator ==(const Item& other) const
{
	return (stoi(this->_serialNumber) == stoi(other._serialNumber));
}


//get and set functions
string Item::getName() const
{
	return this->_name;
}

string Item::getSerialNumber() const
{
	return this->_serialNumber;
}

int Item::getCount() const
{
	return this->_count;
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}


void Item::setName(const string name)
{
	this->_name = name;
}

void Item::setSerialNumber(const string serialNumber)
{
	this->_serialNumber = serialNumber;
}

void Item::incCount()
{
	this->_count++;
}

void Item::decCount()
{
	this->_count--;
}

void Item::setUnitPrice(const double unitPrice)
{
	this->_unitPrice = unitPrice;
}