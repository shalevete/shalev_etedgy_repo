#include"Customer.h"
#include<map>

#define MENU_STR "Welcome to MagshiMart!\n1. to sign as customer and buy items\n2. to uptade existing customer's items\n3. to print the customer who pays the most\n4.      to exit\n"
#define ERR_BAD_INPUT "invalid input"
#define ERR_NAME_TAKEN "this name is already in the customers map"
#define PRINT_UPDATE_OPTIONS "1. Add items\n2. Remove items\n3. Back to menu"
#define ASK_FOR_ITEM_TO_DELETE "select item to delete: (0 to exit)"
#define ASK_FOR_NAME_INPUT "enter name: "
#define CHECK_IF_CUSTOMER_EXISTS (this->_abcCustomers.count(customerName) == 0)

enum menuErrors { ME_nameAlreadyInMap };
enum userOptions { UO_signUp = 1, UO_updateExisting, UO_printMostSpent, OU_exit};
//enum itemsChoices { IC_Milk = 1, IC_Cookies, IC_bread, IC_chocolate, IC_cheese, IC_rice, IC_fish, IC_chicken, IC_cucumber, IC_tomato};


class Menu
{
public:
	Menu();
	~Menu();
	void start();

private:
	Item _itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00007", 31.65),
		Item("chicken","00008",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32) };
	map<string, Customer> _abcCustomers;

	userOptions printMenu();
	void whatToDo();

	void signUp();
	void updateExistingCustomer();

	string insertNewCustomer();
	void getItemsFromCustomer(string customerName);
	void print_items();
	void printCustomerItems(string customerName);
	void deleteItem(string customerName);

	void printMostSpent();
};
