#include"Menu.h"

Menu::Menu()
{

}

Menu::~Menu()
{

}


void Menu::start()
{	
	whatToDo();

}

void Menu::whatToDo()
{
	int choice = 0;
	do
	{
		choice = printMenu();
		switch (choice)
		{
		case UO_signUp:
			signUp();
			break;
		case UO_updateExisting:
			updateExistingCustomer();
			break;
		case UO_printMostSpent:
			printMostSpent();
			break;
		case OU_exit:
			break;
		default:
			cerr << ERR_BAD_INPUT;
			break;
		}
	} while (choice != 4);
}

userOptions Menu::printMenu()
{
	int choice;
	cout << MENU_STR << endl;
	cin >> choice;
	return (userOptions)(choice);
}

void Menu::signUp()
{
	string customerName = "";

	try
	{
		customerName = insertNewCustomer();
		getItemsFromCustomer(customerName);
	}
	catch (menuErrors e)
	{
		if (e = ME_nameAlreadyInMap)
		{
			std::cerr << ERR_NAME_TAKEN << endl;
		}
	}
}

string Menu::insertNewCustomer()
{
	string customerName = "";
	cout << endl << ASK_FOR_NAME_INPUT << endl;
	std::cin >> customerName;
	

	if (CHECK_IF_CUSTOMER_EXISTS)
	{
		Customer newCustomer(customerName);

		this->_abcCustomers.insert(std::pair<string, Customer>(customerName, newCustomer));
	}
	else
	{
		throw(ME_nameAlreadyInMap);
	}
	return customerName;
}

void Menu::getItemsFromCustomer(string customerName)
{
	int choice = 0;

	do {
		set<Item> items = (this->_abcCustomers[customerName].getItems());
		print_items();
		std::cin >> choice;
		if((choice > 0) && (choice < 10))
		{
			if (this->_abcCustomers[customerName].getItems().count(this->_itemList[choice - 1]) == 0)	//check if the item is not in the customer's items list
			{
				this->_abcCustomers[customerName].addItem(this->_itemList[choice - 1]);	//if the item is not in the customer's items list add the choice
			}
			else	//if the item is already in the customer's items list update count
			{
				Item updatedItem = *(items.find(this->_itemList[choice - 1]));
				items.erase(this->_itemList[choice - 1]);
				updatedItem.incCount();
				items.insert(updatedItem);
				this->_abcCustomers[customerName].setItems(items);
			}
		}
		else
		{
			cerr << endl << ERR_BAD_INPUT << endl;
		}

	} while (choice != 0);
}


void Menu::updateExistingCustomer()
{
	string customerName = "";
	cout << endl << ASK_FOR_NAME_INPUT << endl;
	std::cin >> customerName;
	int choice = 0;

	if (!CHECK_IF_CUSTOMER_EXISTS)
	{
		do {
			cout << endl << PRINT_UPDATE_OPTIONS << endl;
			cin >> choice;

			switch (choice)
			{
			case(1):
				getItemsFromCustomer(customerName);
				break;
			case(2):
				deleteItem(customerName);
				printCustomerItems(customerName);
				break;
			case(3):
				printCustomerItems(customerName);
				break;
			default:
				cerr << ERR_BAD_INPUT;
				break;
			}

		} while (choice != 3);
	}
	else
	{
		std::cerr << "this name is not in the customers map" << endl;
	}
}

void Menu::print_items()
{
	cout << endl << "The items you can buy are: (0 to exit)" << endl;
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		cout << i+1 << ". " << this->_itemList[i].getName() << " price: " << this->_itemList[i].getUnitPrice() << endl;
	}
}

void Menu::printCustomerItems(string customerName)
{
	set<Item> items = this->_abcCustomers[customerName].getItems();
	int i = 1;

	cout << endl << "The items you selected:" << endl;

	set<Item>::iterator it;
	for (it = items.begin(); it != items.end(); ++it)
	{
		cout << i << ". " << it->getName() << " amount: " << it->getCount() << endl;
		i++;
	}
}

void Menu::deleteItem(string customerName)
{
	int itemToDelete = 0;
	int i = 1;

	do {
		printCustomerItems(customerName);

		set<Item> items = (this->_abcCustomers[customerName].getItems());
		cout << endl << ASK_FOR_ITEM_TO_DELETE << endl;
		cin >> itemToDelete;
		i = 1;
		if (itemToDelete != 0)
		{
			set<Item>::iterator it;
			for (it = items.begin(); it != items.end(); ++it)
			{
				if (i == itemToDelete)
				{
					if (it->getCount() > 1)
					{
						Item updatedItem = *it;
						items.erase(it);
						updatedItem.decCount();
						items.insert(updatedItem);
					}
					else
					{
						items.erase(it);
					}
					break;
				}
				i++;
			}
		}
	this->_abcCustomers[customerName].setItems(items);

	} while (itemToDelete != 0);

}

void Menu::printMostSpent()
{
	map<string, Customer> customers = this->_abcCustomers;
	int mostSpent = 0;
	int secondMostSpent = 0;
	string mostSpentName = "";

	map<string, Customer>::iterator itCustomers;
	for (itCustomers = customers.begin(); itCustomers != customers.end(); ++itCustomers)
	{
		secondMostSpent = 0;
		set<Item> items = itCustomers->second.getItems();

		set<Item>::iterator itItems;
		for (itItems = items.begin(); itItems != items.end(); ++itItems)
		{
			secondMostSpent += itItems->totalPrice();
		}
		if (secondMostSpent > mostSpent)
		{
			mostSpent = secondMostSpent;
			mostSpentName = itCustomers->first;
		}
	}
	cout << endl << "The customer who pays the most is: " << mostSpentName << endl << endl;
}