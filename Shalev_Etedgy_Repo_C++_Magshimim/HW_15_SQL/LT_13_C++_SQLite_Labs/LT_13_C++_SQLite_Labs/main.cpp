#include <iostream>
#include <io.h>
#include <string>
#include "sqlite3.h"
#include <time.h>       
#include "Person.h"

using std::string;
using std::cout;
using std::endl;
using std::cin;

persons personList;

bool insert_data(sqlite3* db);
bool create_tables(sqlite3* db);
bool update_data(sqlite3* db); 
bool delete_data(sqlite3* db);
int callback(void *data, int argc, char **argv, char **azColName);
void print();

int main(void)
{
	sqlite3* db;
	string dbFileName = "galleryDB1.sqlite";
	int res = sqlite3_open(dbFileName.c_str(), &db);
	int doesFileExist = _access(dbFileName.c_str(), 0);

	if (res != SQLITE_OK)
	{
		db = nullptr;
		cout << "Failed to open DB" << endl;
		return -1;
	}
	if (doesFileExist == 0)
	{
		res = create_tables(db);
		if (!res)
		{
			return false;
		}
		res = insert_data(db);	//learning task 22 - a
		if (!res)
		{
			return false;
		}
		res = update_data(db);			//learning task 22 - b
		if (!res)
		{
			return false;
		}
		res = delete_data(db);		// learning task 22 - c

		//Lab3 - Learning task 31
		const char* sqlStatement = "SELECT * FROM PERSON WHERE FIRST_NAME = 'Marcus';";
		char *errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement, callback, nullptr, &errMessage);
		if (res != SQLITE_OK) {
			return false;
		}
		print();
	}
		sqlite3_close(db);
		db = nullptr;

		system("pause");
		return 0;
	
}

bool create_tables(sqlite3* db)
{
	string sqlStatement;
	char *errMessage;
	int res;

	sqlStatement = "CREATE TABLE PERSON (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, LAST_NAME TEXT NOT NULL, FIRST_NAME TEXT NOT NULL, EMAIL TEXT NOT NULL); ";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}
	sqlStatement = "CREATE TABLE PHONE (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, PhonePrefixID INTGER NOT NULL, PhoneNumber TEXT NOT NULL, PersonID INTGER NOT NULL); ";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}
	sqlStatement = "CREATE TABLE PHONE_PREFIX (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, prefix TEXT NOT NULL); ";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}
	return true;
}

bool insert_data(sqlite3* db)
{
	string sqlStatement;
	char *errMessage;
	int res;
	int i = 0;
	string prefix[12] = { "02", "03" , "04", "08", "09", "050", "052" ,"053" ,"054" ,"055" ,"073" ,"077" };
	string names[5] = { "Katy", "Marcus", "Alice", "Zoe" , "Marcus"};
	string last_names[5] = { "Mcentyre", "Gomez", "Rupp", "Merwin", "Young" };
	string emails[5] = { "katy@z5cpw9psdfhwylva.com", "marcus@z5cpw9psdfhwylva.com", "alice@z5cpw9psdfhwylva.com", "zoe@z5cpw9psdfhwylva.com" , "marcus_young@z5cpw9psdfhwylva.com"};
	string phones[6] = { "3891015", "9609740", "4328972", "8237019", "1511514", "9204668" };

	for (i = 0; i < 12; i++)
	{
		sqlStatement = "INSERT INTO PHONE_PREFIX (PREFIX) VALUES ('" + prefix[i] + "');";
		errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			return false;
		}
	}

	for (i = 0; i < 5; i++)
	{
		sqlStatement = "INSERT INTO PERSON (LAST_NAME, FIRST_NAME, EMAIL) VALUES ('" + last_names[i] + "', '" + names[i] + "', '" + emails[i] + "');";
		errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			return false;
		}
	}

	srand(time(NULL));

	for (i = 0; i < 3; i++)
	{
		sqlStatement = "INSERT INTO PHONE (PhonePrefixID, PhoneNumber, PersonID) VALUES (" + std::to_string(rand() % 12) + ", '" + phones[i] + "', 1);";
		errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			return false;
		}
	}
	for (i = 0; i < 2; i++)
	{
		sqlStatement = "INSERT INTO PHONE (PhonePrefixID, PhoneNumber, PersonID) VALUES (" + std::to_string(rand() % 12) + ", '" + phones[i + 3] + "', 3);";
		errMessage = nullptr;
		res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			return false;
		}
	}
	sqlStatement = "INSERT INTO PHONE (PhonePrefixID, PhoneNumber, PersonID) VALUES (" + std::to_string(rand() % 12) + ", '" + phones[5] + "', 2);";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}
	return true;
}

bool update_data(sqlite3* db)
{
	string sqlStatement;
	char *errMessage;
	int res;

	sqlStatement = "INSERT INTO PHONE_PREFIX (PREFIX) VALUES ('089');";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}

	sqlStatement = "UPDATE PHONE_PREFIX set PREFIX = '078' where PREFIX = '089';";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}

	sqlStatement = "UPDATE PERSON set LAST_NAME = 'Elimelech' where ID=1;";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}
	sqlStatement = "UPDATE PERSON set FIRST_NAME = 'Sonia' where ID=1;";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}
	return true;
}

bool delete_data(sqlite3* db)
{
	string sqlStatement;
	char *errMessage;
	int res;

	sqlStatement = "DELETE FROM PHONE WHERE ID=3;";
	errMessage = nullptr;
	res = sqlite3_exec(db, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		return false;
	}
	return true;
}



int callback(void *data, int argc, char **argv, char **azColName)
{
	Person person;
	for (int i = 0; i < argc; i++)
	{
		if (string(azColName[i]) == "ID") 
		{
			person.setId(atoi(argv[i]));
		}
		else if (string(azColName[i]) == "LAST_NAME")
		{
			person.setLastName(argv[i]);
		}
		else if (string(azColName[i]) == "FIRST_NAME")
		{
			person.setFirstName(argv[i]);
		}
		else if (string(azColName[i]) == "EMAIL")
		{
			person.setEmail(argv[i]);
		}
	}
	personList.push_back(person);
	return 0;
}

void print()
{
	persons_iter iter = personList.begin();
	while (iter != personList.end())
	{
		cout << iter->getLastName() << " , "
			<< iter->getFirstName() << " , "
			<< iter->getEmail() << endl;
		++iter;
	}
}