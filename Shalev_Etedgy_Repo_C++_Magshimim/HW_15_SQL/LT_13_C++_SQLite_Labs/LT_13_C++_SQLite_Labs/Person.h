#include <iostream>
#include <io.h>
#include <string>
#include "sqlite3.h"
#include <time.h>    
#include <list>    

using std::string;
using std::list;

class Person;
typedef list<Person> persons;
typedef persons::iterator persons_iter;

class Person
{
public:
	Person();
	Person(int id, string lastName, string firstName);
	int getId() const;
	void setId(int id);
	string getFirstName() const;
	void setFirstName(const string& name);
	string getLastName() const;
	void setLastName(const string& name);
	string getEmail() const;
	void setEmail(const string& email);
private:
	int m_id;
	string m_firstName;
	string m_lastName;
	string m_email;
};