#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "linkedList.h"
#include "stack.h"
#include "utils.h"

int main()
{
	stack* myStack = new stack;
	initStack(myStack);
	
	push(myStack, 1);
	push(myStack, 4);
	push(myStack, 9);

	std::cout << "element popped: " << pop(myStack) << std::endl;
	std::cout << "element popped: " << pop(myStack) << std::endl;
	std::cout << "element popped: " << pop(myStack) << std::endl;

	std::cout << "element popped: " << pop(myStack) << std::endl;

	push(myStack, 9);
	std::cout << "element popped: " << pop(myStack) << std::endl;
	std::cout << "element popped: " << pop(myStack) << std::endl;

	push(myStack, 4);
	push(myStack, 9);
	std::cout << "element popped: " << pop(myStack) << std::endl;
	std::cout << "element popped: " << pop(myStack) << std::endl;
	

	cleanStack(myStack);
	delete(myStack);
	
	//---------------------------------------------------------------------------

	//section c of question 2
	int arrayOfNumbers[4] = { 1,2,3,4 };
	int i = 0;

	std::cout << "array: ";
	for (i = 0; i < 4; i++)
	{
		std::cout << arrayOfNumbers[i] << " ";
	}

	reverse(arrayOfNumbers, 4);

	std::cout << std::endl << "array reversed: ";
	for (i = 0; i < 4; i++)
	{
		std::cout << arrayOfNumbers[i] << " ";
	}

	//---------------------------------------------------------------------------

	//section d of question 2

	int * reversedArray = reverse10();

	std::cout << std::endl << "reversed 10: ";
	for (i = 0; i < 10; i++)
	{
		std::cout << reversedArray[i] << " ";
	}

	delete[] reversedArray;
}

//	std::cout << "element: " << curr->_element << "position: " << curr->_position << std::endl;
