#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "stack.h"

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>


elementNode* createStackElement(unsigned int newValue);
void insertAtStart(elementNode** lastNode, elementNode* newNode);
void freeList(elementNode** lastNode);
void printList(elementNode* lastNode);
int lastElement(elementNode** lastNode);

 
#endif /* LINKEDLIST_H */