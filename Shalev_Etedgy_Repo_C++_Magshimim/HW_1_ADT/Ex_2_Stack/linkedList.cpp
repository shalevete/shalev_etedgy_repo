#include "linkedList.h"

/**
Function creates element node
input: value to add
output:
none
*/
elementNode* createStackElement(unsigned int newValue)
{
	elementNode *newStackElement = new elementNode;

	newStackElement->_element = newValue;
	newStackElement->_prev = NULL;

	return newStackElement;
}

/**
Function insert an element node at the top of the stack
input: the list (the last element node)
output:
none
*/
void insertAtStart(elementNode** lastNode, elementNode* newNode)
{
	int i = 0;
	elementNode *temp = *lastNode;

	if (!*lastNode) // empty list!
	{
		newNode->_position = 0;
		*lastNode = newNode;
	}
	else
	{
		newNode->_prev = temp;
		newNode->_position = temp->_position + 1;
		*lastNode = newNode;
	}

}

/**
Function will free all memory of a list
input:
a list
output:
none
*/
void freeList(elementNode** lastNode)
{
	elementNode* temp = NULL;
	elementNode* curr = *lastNode;
	while (curr)
	{
		temp = curr;
		curr = (curr)->_prev;
		free(temp);
	}

	*lastNode = NULL;

}

/**
Function will print a list of stack
input: the list (the first element node)
output:
none
*/
void printList(elementNode* lastNode)
{
	elementNode* curr = lastNode;
	std::cout << std::endl;
	while (curr) // when curr == NULL, that is the end of the list, and loop will end (NULL is false)
	{
		std::cout << "element: " << curr->_element << "position: " << curr->_position << std::endl;
		curr = curr->_prev;
	}
	std::cout << std::endl;
}


/**
Function will return a the last element in the stack
input: the list (the first node)
output:
none
*/
int lastElement(elementNode** lastNode)
{
	elementNode* temp = NULL;
	elementNode* curr = *lastNode;
	int lastElement = 0;

	lastElement = curr->_element;

	temp = curr;
	curr = (curr)->_prev;
	delete(temp);
	
	*lastNode = curr;

	return lastElement;
}
