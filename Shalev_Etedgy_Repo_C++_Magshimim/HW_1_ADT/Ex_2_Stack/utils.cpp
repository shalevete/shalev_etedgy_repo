#include "utils.h"
#include "stack.h"
#include "linkedList.h"


void reverse(int* nums, unsigned int size)
{
	stack* tempStack = new stack;
	int i = 0;

	initStack(tempStack);
	
	for (i = 0; i < size; i++)
	{
		push(tempStack, nums[i]);
	}

	for (i = 0; i < size; i++)
	{
		nums[i] = pop(tempStack);
	}

	cleanStack(tempStack);
	delete(tempStack);
}


int* reverse10()
{
	int* arrayOfInputs = new int[10];
	int i = 0;

	std::cout << "insert 10 numbers: " << std::endl;
	for (i = 0; i < 10; i++)
	{
		std::cin >> arrayOfInputs[i];
	}

	reverse(arrayOfInputs, 10);
	return arrayOfInputs;
}
