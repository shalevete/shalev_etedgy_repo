#include "stack.h"
#include "linkedList.h"


void initStack(stack* s)
{
	s->_stackElement = new elementNode;
	s->_stackElement = NULL;
}

void cleanStack(stack* s)
{
	freeList(&(s->_stackElement));
}

void push(stack* s, unsigned int element)
{
	insertAtStart(&(s->_stackElement), createStackElement(element));
}

int pop(stack* s)
{
	int poppedElement = 0;
	if (!(s->_stackElement))
	{
		poppedElement = -1;
	}
	else
	{
		poppedElement = lastElement(&(s->_stackElement));
	}

	return poppedElement;
}

