#include "queue.h"

/*
Function check if queue is empty
input:
queue
output:
true if the queue empty or false if the queue is not empty
*/
bool isEmpty(queue *q)
{
	bool isQueueEmpty = false;
	
	if (q->_count <= 0)
	{
		isQueueEmpty = true;
	}

	return isQueueEmpty;
}

/*
Function check if queue is full
input:
queue
output:
true if the queue full or false if the queue is not full
*/
bool isFull(queue *q)
{
	bool isQueueFull = false;

	if (q->_count == q->_maxSize)
	{
		isQueueFull = true;
	}

	return isQueueFull;
}

/*
Function will init a queue
input:
queue, size of the queue
output:
none
*/
void initQueue(queue* q, unsigned int size)
{
	q->_maxSize = size;
	q->_count = 0;
	q->_elements = new int[q->_maxSize];
}

/*
Function will clean a queue
input:
queue
output:
none
*/
void cleanQueue(queue* q)
{
	q->_maxSize = 0;
	q->_count = 0;
	delete[] q->_elements;
}

/*
Function inert a number to the queue
input:
queu, value to add
output:
none
*/
void enqueue(queue* q, unsigned int newValue)
{
	if ((!(isFull(q))) && (int(newValue) >= 0))
	{
		q->_elements[q->_count] = newValue;
		q->_count++;
	}
}

/*
Function will clean a queue
input:
queue
output:
element in top of queue, or -1 if empty
*/
int dequeue(queue* q)
{
	unsigned int valueToReturn = 0;
	int i = 0;

	if (!(isEmpty(q)))
	{
		valueToReturn = q->_elements[0];
		
		for (i = 0; i < ((q->_count) - 1); i++)
		{
			q->_elements[i] = q->_elements[i + 1];
		}
		q->_elements[((q->_count) - 1)] = -1;

		q->_count--;
	}
	else
	{
		valueToReturn = -1;
	}

	return valueToReturn;
}
 