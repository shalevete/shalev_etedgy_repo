#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "queue.h"


int main()
{
	queue *myQueue = new queue;
	initQueue(myQueue, 3);

	enqueue(myQueue, 1);
	enqueue(myQueue, 4);
	enqueue(myQueue, -7);
	enqueue(myQueue, 9);

	std::cout << "Is the queue full? " << isFull(myQueue) << std::endl;

	while (myQueue->_count != 0)
	{
	std::cout << "element dequeued: " << dequeue(myQueue) << std::endl;
	}

	std::cout << "Is the queue empty? " << isEmpty(myQueue) << std::endl;
	std::cout << "Is the queue full? " << isFull(myQueue) << std::endl;


	enqueue(myQueue, 9);
	std::cout << "element dequeued: " << dequeue(myQueue) << std::endl;
	std::cout << "element dequeued: " << dequeue(myQueue) << std::endl;
	std::cout << "Is the queue empty? " << isEmpty(myQueue) << std::endl;

	enqueue(myQueue, 1);
	enqueue(myQueue, 9);

	std::cout << "Is the queue full? " << isFull(myQueue) << std::endl;

	std::cout << "element dequeued: " << dequeue(myQueue) << std::endl;

	std::cout << "Is the queue empty? " << isEmpty(myQueue) << std::endl;

	std::cout << "element dequeued: " << dequeue(myQueue) << std::endl;

	cleanQueue(myQueue);
	delete(myQueue);
}
