#include "Circle.h"


Circle::Circle(const Point& center, double radius, const string& type, const string& name) : _center(center.getX(), center.getY()), Shape(name, type)
{
	this->_radius = radius;
}

Circle::~Circle()
{

}


const Point& Circle::getCenter() const
{
	return this->_center;
}


double Circle::getRadius() const
{
	return this->_radius;
}

double Circle::getArea() const
{
	return PI*(this->_radius)*(this->_radius);
}

double Circle::getPerimeter() const
{
	return 2 * PI * (this->_radius);
}

void Circle::move(const Point& other)
{
	this->_center = this->_center + other;
}


void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}

void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}



void Circle::printDetails() const
{
	cout << "name: " << this->getName()
		<< "	type: " << this->getType()
		<< "	area: " << this->getArea()
		<< "	radius: " << this->getRadius()
		<< "	perimeter" << this->getPerimeter() <<endl;
}


