#include "Menu.h"


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		removeShape(i);
	}

	_disp->close();
	delete _board;
	delete _disp;
}


cimg_library::CImgDisplay* Menu::get_disp() const
{
	return this->_disp;
}

cimg_library::CImg<unsigned char>* Menu::get_board() const
{
	return this->_board;
}



void Menu::printMenu()
{
	int i = 0;
	int choice = 0;
	bool good_input = false;
	while (choice != 3)
	{
		good_input = false;
		cout << "Enter 0 to add a new shape." << endl
			<< "Enter 1 to modify or get information from a current shape." << endl
			<< "Enter 2 to delete all of the shapes." << endl
			<< "Enter 3 to exit." << endl;

		while (!good_input)
		{
			cin >> choice;
			switch (choice) {
			case 0:
				good_input = true;
				Menu::new_shape();
				break;
			case 1:
				if (this->_shapes.size() == 0)
				{
					good_input = false;
				}
				else
				{
					Menu::current_shape();
					good_input = true;
				}
				break;
			case 2:
				if (this->_shapes.size() == 0)
				{
					good_input = false;
				}
				else
				{
					Menu::clearScreen();
					for (i = 0; i < this->_shapes.size(); i++)
					{
						removeShape(i);
					}

					good_input = true;
				}
				break;
			case 3:
				//exit();
				good_input = true;
				break;
			default:
				break;
			}
		}
	}
}

//add a new shape

void Menu::new_shape()
{
	int choice = 0;
	bool good_input = false;
	cout << "Enter 0 to add a circle." << endl
		<< "Enter 1 to add an arrow." << endl
		<< "Enter 2 to add a triangle." << endl
		<< "Enter 3 to add a rectangle." << endl;

	while (!good_input)
	{
		cin >> choice;
		switch (choice) {
		case 0:
			good_input = true;
			Menu::new_circle();
			break;
		case 1:
			good_input = true;
			Menu::new_arrow();
			break;
		case 2:
			good_input = true;
			Menu::new_triangle();
			break;
		case 3:
			good_input = true;
			Menu::new_rectangle();
			break;
		default:
			good_input = false;
			break;
		}
	}
}

void Menu::new_circle()
{
	int x = 0;
	int y = 0;
	int radius = 0;
	string name;

	cout << "Please enter X:" << endl;
	cin >> x;
	
	cout << "Please enter Y:" << endl;
	cin >> y;
		
	cout << "Please enter radius:" << endl;
	cin >> radius;

	cout << "Please enter the name of the shape:" << endl;
	cin >> name;

	Point center(x, y);
	Shape* myCircle = new Circle(center, radius, "circle", name);
	this->_shapes.push_back(myCircle);
	myCircle->draw(*(this->_disp), *(this->_board));
}

void Menu::new_arrow()
{
	int x1 = 0;
	int x2 = 0;
	int y1 = 0;
	int y2 = 0;
	string name;

	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;

	cout << "Enter the Y of point number: 1:" << endl;
	cin >> y1;

	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;

	cout << "Enter the Y of point number: 2:" << endl;
	cin >> y2;

	cout << "Please enter the name of the shape:" << endl;
	cin >> name;

	Point x(x1, y1);
	Point y(x2, y2);

	Shape* myArrow = new Arrow(x, y, "arrow", name);
	this->_shapes.push_back(myArrow);
	myArrow->draw(*(this->_disp), *(this->_board));
}

void Menu::new_triangle()
{
	int x1 = 0;
	int x2 = 0;
	int x3 = 0;
	int y1 = 0;
	int y2 = 0;
	int y3 = 0;
	string name;

	cout << "Enter the X of point number: 1" << endl;
	cin >> x1;

	cout << "Enter the Y of point number: 1:" << endl;
	cin >> y1;

	cout << "Enter the X of point number: 2" << endl;
	cin >> x2;

	cout << "Enter the Y of point number: 2:" << endl;
	cin >> y2;

	cout << "Enter the X of point number: 3" << endl;
	cin >> x3;

	cout << "Enter the Y of point number: 3:" << endl;
	cin >> y3;

	cout << "Please enter the name of the shape:" << endl;
	cin >> name;

	Point a(x1, y1);
	Point b(x2, y2);
	Point c(x3, y3);

	Shape* myTriangle = new Triangle(a, b, c, "triangle", name);
	this->_shapes.push_back(myTriangle);
	myTriangle->draw(*(this->_disp), *(this->_board));
}

void Menu::new_rectangle()
{
	int x = 0;
	int y = 0;
	int len = 0;
	int width = 0;
	string name;

	cout << "Enter the X of the to left corner:" << endl;
	cin >> x;

	cout << "Enter the Y of the top left corner:" << endl;
	cin >> y;

	cout << "Please enter the length of the shape:" << endl;
	cin >> len;

	cout << "Please enter the width of the shape:" << endl;
	cin >> width;

	cout << "Please enter the name of the shape:" << endl;
	cin >> name;

	Point a(x,y);
	Shape* myRectangle = new myShapes::Rectangle(a, len, width, "Rectangle", name);
	this->_shapes.push_back(myRectangle);
	myRectangle->draw(*(this->_disp), *(this->_board));
}

//modify or get information from a current shape

void Menu::current_shape()
{
	int choice = 0;
	int choice2 = 0;
	bool good_input = false;
	int i = 0;

	for (i = 0; i < this->_shapes.size(); i++)
	{
		cout << "Enter "<< i << " for " << this->_shapes[i]->getName()	<< "(" << this->_shapes[i]->getType() << ")"	<< endl;
	}

	while (!good_input)
	{
		cin >> choice;
		if ((choice > _shapes.size()) || (choice < 0))
		{
			good_input = false;
		}
		else
		{
			good_input = true;
		}
	}		

		good_input = false;
		while (!good_input)
		{
			cout << "Enter 0 to move the shape." << endl
				<< "Enter 1 to get its details." << endl
				<< "Enter 2 to remove the shape." << endl;
			cin >> choice2;

			switch (choice2) {
			case 0:
				Menu::moveShape(choice);
				good_input = true;
				break;
			case 1:
				good_input = true;
				Menu::details(choice);
				break;
			case 2:
				good_input = true;
				Menu::deleteShape(choice);
				break;
			default:
				good_input = false;
				break;
			}
		}
	
}

void Menu::moveShape(int index)
{
	int x = 0;
	int y = 0;
	int i = 0;

	cout << "Please enter the X moving scale:" << endl;
	cin >> x;
	cout << "Please enter the Y moving scale:" << endl;
	cin >> y;

	this->_shapes[index]->clearDraw(*(this->_disp), *(this->_board));
	Point otherPoint(x, y);
	this->_shapes[index]->move(otherPoint);

	for (i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->draw(*(this->_disp), *(this->_board));
	}
}

void Menu::details(int index)
{
	char key = 0;
	this->_shapes[index]->printDetails();
	cout << "Press any key to continue . . ." << endl;
	cin >> key;
}


void Menu::deleteShape(int index)
{
	Menu::clearScreen();
	removeShape(index);
	Menu::drawAll();
}

void Menu::removeShape(int index)
{
	delete this->_shapes[index];
	this->_shapes.erase(this->_shapes.begin() + index);
}


void Menu::clearScreen()
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->clearDraw(*(this->_disp), *(this->_board));
	}

}

void Menu::drawAll()
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->draw(*(this->_disp), *(this->_board));
	}

}