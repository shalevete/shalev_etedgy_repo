#include "Triangle.h"



Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) :  Polygon(name, type)
{
	if (((a.getX() == b.getX()) && (a.getX() == c.getX())) || ((a.getY() == b.getY()) && (a.getY() == c.getY())))
	{
		std:cerr << SAME_AXIS_ERR;
	}
	this->_points[2] = c;
	this->_points[1] = b;
	this->_points[0] = a;

}

Triangle::~Triangle()
{

}

double Triangle::getArea() const
{
	double s = 0;
	double area = 0;

	double a = this->_points[0].distance(this->_points[1]);
	double b = this->_points[1].distance(this->_points[2]);
	double c = this->_points[2].distance(this->_points[0]);

	s = (a + b + c) / 2;
	area = sqrt(s*(s - a)*(s - b)*(s - c));

	return area;
}

double Triangle::getPerimeter() const
{
	double perim = 0;

	perim = this->_points[0].distance(this->_points[1]);
	perim += this->_points[1].distance(this->_points[2]);
	perim += this->_points[2].distance(this->_points[0]);

	return perim;
}

void Triangle::move(const Point& other)
{
	this->_points[0] = this->_points[0] + other;
	this->_points[1] = this->_points[1] + other;
	this->_points[2] = this->_points[2] + other;
}

void Triangle::printDetails() const
{
	cout << "name: " << this->getName()
		<< "	type: " << this->getType()
		<< "	area: " << this->getArea()
		<< "	perimeter" << this->getPerimeter() << endl;
}


void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
