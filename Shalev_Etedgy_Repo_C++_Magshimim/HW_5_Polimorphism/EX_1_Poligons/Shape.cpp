#include "Shape.h"

Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
	this->_graphicShape.resize(5);
}

void Shape::setType(const string& type)
{
	this->_type = type;
}

void Shape::setName(const string& name)
{
	this->_name = name;
}


string Shape::getType() const
{
	return this->_type;
}

string Shape::getName() const
{
	return this->_name;
}