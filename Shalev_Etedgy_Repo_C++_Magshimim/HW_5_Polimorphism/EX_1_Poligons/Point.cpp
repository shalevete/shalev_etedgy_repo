#include "Point.h"

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

Point::Point()
{
	this->_x = 0;
	this->_y = 0;
}

Point::~Point()
{

}

Point Point::operator+(const Point& other) const
{
	double x = this->_x + other._x;
	double y = this->_y + other._y;
	Point pointAdded(x, y);
	return pointAdded;
}

Point& Point::operator+=(const Point& other)
{
	Point pointAdded(this->operator+(other));
	return pointAdded;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point& other) const
{
	return sqrt(pow(this->_x - other._x, 2) + pow(this->_y - other._y, 2));
}