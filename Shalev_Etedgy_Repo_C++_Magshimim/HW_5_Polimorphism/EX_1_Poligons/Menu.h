#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>



class Menu
{
public:

	Menu();
	~Menu();

	// more functions..
	void printMenu();

	void new_shape();
	void new_circle();
	void new_arrow();
	void new_triangle();
	void new_rectangle();

	void current_shape();
	void moveShape(int index);
	void details(int index);
	void removeShape(int index);
	void deleteShape(int index);

	void clearScreen();
	void drawAll();


private: 
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;

	cimg_library::CImgDisplay* get_disp() const;
	cimg_library::CImg<unsigned char>* get_board() const;

	std::vector<Shape*> _shapes;
};

