#include "Rectangle.h"

using namespace myShapes;

Rectangle::Rectangle(const Point& a, double length, double width, const string& type, const string& name) : Polygon(name, type)
{
	this->_points[0] = a;
	Point b((a.getX() + length), (a.getY() + width));
	this->_points[1] = b;
}

Rectangle::~Rectangle()
{

}

double Rectangle::getArea() const
{
	double area = 0;

	double length = this->_points[1].getX() - this->_points[0].getX();
	double width = this->_points[1].getY() - this->_points[0].getY();

	area = length * width;
	return area;
}

double Rectangle::getPerimeter() const
{
	double perim = 0;

	double length = this->_points[1].getX() - this->_points[0].getX();
	double width = this->_points[1].getY() - this->_points[0].getY();

	perim = length + length + width + width;
	return perim;
}

void Rectangle::move(const Point& other)
{
	this->_points[0] = this->_points[0] + other;
	this->_points[1] = this->_points[1] + other;
}

void Rectangle::printDetails() const
{
	std::cout << "name: " << this->getName()
		<< "	type: " << this->getType()
		<< "	area: " << this->getArea()
		<< "	perimeter" << this->getPerimeter() << endl;
}



void myShapes::Rectangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char WHITE[] = { 255, 255, 255 };
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), WHITE, 100.0f).display(disp);
}

void myShapes::Rectangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0};
	board.draw_rectangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(), BLACK, 100.0f).display(disp);
}


