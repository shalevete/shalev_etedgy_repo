#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
using namespace std;


int main()
{
	BSNode<int>* myIntBST = new BSNode<int>(6);
	myIntBST->insert(1);
	myIntBST->insert(8);
	myIntBST->insert(3);
	myIntBST->insert(15);
	myIntBST->insert(9);
	myIntBST->insert(6);
	myIntBST->insert(2);

	BSNode<string>* myStringBST = new BSNode<string>("a");
	myStringBST->insert("t");
	myStringBST->insert("b");
	myStringBST->insert("s");
	myStringBST->insert("c");
	myStringBST->insert("e");
	myStringBST->insert("p");

	cout << "int BST before sort:" << endl;
	cout << "6 1 8 3 15 9 6 2";
	cout << endl << "string BST before sort:" << endl;
	cout << "a t b s c e p";

	cout << "int BST sorted:" << endl;
	myIntBST->printNodes();
	cout << endl << "string BST sorted:" << endl;
	myStringBST->printNodes();
	return 0;
}

