﻿#include "BSNode.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#pragma comment(lib, "printTreeToFile.lib")
#include "printTreeToFile.h"
using namespace std;


int main()
{
	BSNode* bs = new BSNode("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");
	
	bs->printNodes();
	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;

	BSNode* bs1 = new BSNode(*bs);
	bs1->printNodes();
	bs1->insert("abc");
	bs1->printNodes();
	*bs = *bs1;
	bs->printNodes();

	cout << "isLeaf(yes): " << bs->getLeft()->getRight()->getRight()->isLeaf() << endl;
	cout << "isLeaf(no): " << bs->getLeft()->getRight()->isLeaf() << endl;
	cout << "getData(3): " << bs->getLeft()->getRight()->getData() << endl;
	cout << "is 5 in the bst?(yes): " << bs->search("5")<< endl;


	string textTree = "BSTData.txt";
	printTreeToFile(bs, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());

	bs->insert("ddd");

	textTree = "BSTData.txt";
	printTreeToFile(bs1, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;
	delete bs1;

	return 0;
}

