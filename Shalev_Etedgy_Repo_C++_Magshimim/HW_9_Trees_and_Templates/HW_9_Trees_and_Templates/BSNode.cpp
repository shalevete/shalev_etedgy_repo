#include "BSNode.h"

BSNode::BSNode(string data)
{
	this->_right = nullptr;
	this->_left = nullptr;
	this->_data = data;
	this->_count = 1;
}


BSNode::BSNode(const BSNode& other)
{
	this->operator=(other);
}

BSNode::~BSNode()
{
	if (this->_right)
	{
		delete this->_right;
	}
	if (this->_left)
	{
		delete this->_left;
	}
}

void BSNode::insert(string value)
{
	string Mydata = this->_data;
	string valueGot = value;
	int compared = this->_data.compare(value);

	if (compared < 0)
	{
		if (this->_right)
		{
			this->_right->insert(value);
		}
		else
		{
			this->_right = new BSNode(value);
		}
	}
	else if (compared > 0)
	{
		if (this->_left)
		{
			this->_left->insert(value);
		}
		else
		{
			this->_left = new BSNode(value);
		}
	}
	else
	{
		this->_count++; 
	}
}

BSNode& BSNode::operator=(const BSNode& other)
{
	this->_data = other._data;
	this->_count = other._count;

	if (other._right)
	{
		this->_right = new BSNode(other._right->_data);
		this->_right->operator=(*other._right);
	}

	if (other._left)
	{
		this->_left = new BSNode(other._left->_data);
		this->_left->operator=(*other._left);
	}

	return *this;
}

bool BSNode::isLeaf() const
{
	if ((!this->_right) && (!this->_left) && (this->_data != ""))
	{
		return true;
	}
	return false;
}

string BSNode::getData() const
{
	return this->_data;
}

BSNode* BSNode::getLeft() const
{
	return this->_left;
}

BSNode* BSNode::getRight() const
{
	return this->_right;
}


bool BSNode::search(string val) const
{
	if (this == nullptr)
	{
		return false;
	}
	if (this->_data == val)
	{
		return true;
	}
	if (val < this->_data)
	{
		return this->_left->search(val);
	}

	return this->_right->search(val);
}

int BSNode::getHeight() const
{
	int lHeight = 0;
	int rHeight = 0;

	if (this == NULL)
		return -1;	//the root doesn't count
	else
	{
		lHeight = this->_left->getHeight();
		rHeight = this->_right->getHeight();

		if (lHeight > rHeight)
		{
			return(lHeight + 1);
		}
		else
		{
			return(rHeight + 1);
		}
	}
}

int BSNode::getDepth(const BSNode& root) const
{
	int count = 0;
	if (this->_data == root._data)
	{
		count = 0;
	}
	else if (this)
	{
		if (root.search(this->_data))
		{
			if (root._right->search(this->_data))
			{
				count = getDepth(*root._right);
			}
			else if (root._left->search(this->_data))
			{
				count = getDepth(*root._left);
			}
			count++;
		}
	}
	else
	{
		count = -1;
	}
	return count;
}


void BSNode::printNodes() const
{
	if (this != NULL)
	{
		this->_left->printNodes();
		std::cout << this->_data << " " << this->_count << std::endl;
		this->_right->printNodes();
	}
}