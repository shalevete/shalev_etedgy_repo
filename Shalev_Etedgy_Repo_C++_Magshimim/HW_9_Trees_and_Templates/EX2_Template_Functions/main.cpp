#include "functions.h"
#include <iostream>
#include "SimpleClass.h"

int main() {

	//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

	//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for (int i = 0; i < arr_size; i++) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;


	//check compare char
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<char>('a', 'b') << std::endl;
	std::cout << compare<char>('h', 'c') << std::endl;
	std::cout << compare<char>('d', 'd') << std::endl;

	//check bubbleSort char
	std::cout << "correct print is sorted array" << std::endl;

	const int char_arr_size = 5;
	char charArr[char_arr_size] = { 'g', 'a', 'q', 'b', 'c' };
	bubbleSort<char>(charArr, char_arr_size);
	for (int i = 0; i < char_arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray char
	std::cout << "correct print is sorted array" << std::endl;
	printArray<char>(charArr, char_arr_size);
	std::cout << std::endl;


	//check compare SimpleClass
	SimpleClass a(1);
	SimpleClass b(2);
	SimpleClass c(3);
	SimpleClass d(2);

	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<SimpleClass>(a, b) << std::endl;
	std::cout << compare<SimpleClass>(c, a) << std::endl;
	std::cout << compare<SimpleClass>(b, d) << std::endl;

	//check bubbleSort SimpleClass
	std::cout << "correct print is sorted array" << std::endl;

	const int SimpleClass_arr_size = 5;
	SimpleClass SimpleClassArr[SimpleClass_arr_size] = { SimpleClass(2), SimpleClass(7), SimpleClass(1), SimpleClass(87), SimpleClass(5)};
	bubbleSort<SimpleClass>(SimpleClassArr, SimpleClass_arr_size);
	for (int i = 0; i < SimpleClass_arr_size; i++) {
		std::cout << SimpleClassArr[i] << " ";
	}
	std::cout << std::endl;

	//check printArray SimpleClass
	std::cout << "correct print is sorted array" << std::endl;
	printArray<SimpleClass>(SimpleClassArr, SimpleClass_arr_size);
	std::cout << std::endl;

	
	return 1;
}