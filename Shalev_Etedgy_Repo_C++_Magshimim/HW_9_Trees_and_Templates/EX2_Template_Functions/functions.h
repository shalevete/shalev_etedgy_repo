#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <string>
#include <iostream>

using namespace std;


template<typename T>
int compare(T x, T y)
{
	if (x == y)
	{
		return 0;
	}
	else if (x < y)
	{
		return 1;
	}
	else 
	{
		return -1;
	}
}

template < typename T >
void bubbleSort(T myArray[], int len)
{
	int i = 0;
	int j = 0;
	T temp;

	for (i = 0; i < len; i++)
	{
		for (j = i + 1; j < len; j++)
		{
			if (myArray[i] > myArray[j])
			{
				temp = myArray[i];
				myArray[i] = myArray[j];
				myArray[j] = temp;
			}
		}
	}
}

template < typename T >
void printArray(T myArray[], int len)
{
	int i = 0;

	for (i = 0; i < len; i++)
	{
		cout << myArray[i] << endl;
	}
}


#endif