#ifndef SIMPLE_CLASS
#define SIMPLE_CLASS

#include <string>
#include <iostream>

using namespace std;

class SimpleClass
{
public:
	SimpleClass(int num);
	SimpleClass();

	~SimpleClass();

	bool operator==(const SimpleClass& other);
	bool operator<(const SimpleClass& other);
	bool operator>(const SimpleClass& other);
	friend std::ostream& operator<< (std::ostream& stream, const SimpleClass& SimpleClass);

	int _num;

};

#endif