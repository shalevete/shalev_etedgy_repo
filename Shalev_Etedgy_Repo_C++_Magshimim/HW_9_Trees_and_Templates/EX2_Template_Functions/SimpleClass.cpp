#include "SimpleClass.h"

SimpleClass::SimpleClass(int num)
{
	this->_num = num;
}

SimpleClass::SimpleClass()
{
	this->_num = 0;
}

SimpleClass::~SimpleClass()
{

}

bool SimpleClass::operator>(const SimpleClass& other)
{
	return (this->_num > other._num);
}

bool SimpleClass::operator<(const SimpleClass& other)
{
	return (this->_num < other._num);
}

bool SimpleClass::operator==(const SimpleClass& other)
{
	return (this->_num == other._num);
}

std::ostream& operator<< (std::ostream& stream, const SimpleClass& SimpleClass)
{
	stream << SimpleClass._num;
	return stream;
}