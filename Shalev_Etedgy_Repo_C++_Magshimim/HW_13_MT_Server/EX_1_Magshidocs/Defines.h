#pragma once

#include "Helper.h"

#define NOTHING -999

//file
#define FILE_NAME "E://shared_file.txt"
#define ERR_CANT_OPEN_FILE "Unable to open file"

//messages
#define MSG_START "Starting..."
#define MSG_BINDED "binded"
#define MSG_lISTENING "listening..."
#define MSG_ACCEPTING_CLIENT "accepting client..."
#define MSG_CLIENT_ACCEPTED "Client accepted !"
#define MSG_CLIENT_ADDED "ADDED new client %d, %s to clients list"
#define MSG_SENT_UPDATE "Send update message to all clients"
#define MSG_RECV_FINISH "Recieved finish message from current client"
#define MSG_RECV_UPDATE "Recieved update message from current client"
#define MSG_RECV_EXIT "Recieved exit message from client"
#define MSG_REMOVED "REMOVED %d, userName from clients list"

//requests
#define REQ_USERNAME_SIZE 2
#define REQ_FILE_CONTENT_SIZE 5

//client recv struct
struct clientRecvDetails
{
	MessageType typeCode;
	std::string stringPart;
	int intPart;
};
