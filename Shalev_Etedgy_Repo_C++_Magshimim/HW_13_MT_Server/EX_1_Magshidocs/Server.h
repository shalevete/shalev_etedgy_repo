#pragma once

#include "Helper.h"
#include "Defines.h"
#include <WinSock2.h>
#include <Windows.h>
#include <exception>
#include <iostream>
#include <string>
#include <deque>
#include <utility>  
#include <thread>
#include <mutex>
#include <condition_variable>
#include <fstream>
#include <queue> 

using std::string;
using std::cout;
using std::endl;
using std::deque;
using std::pair;
using std::queue;

class Server
{
public:
	Server();
	~Server();
	void start();

private:
	//variables
	SOCKET _serverSocket;
	bool _isAliveFlag;
	int _numOfThreads;
	deque<pair<string, SOCKET>> _users;
	string _currUser;
	string _nextUser;
	queue<pair<clientRecvDetails, SOCKET>> _messages;

	//clientHandler and helper functions
	void clientHandler(SOCKET clientSocket);

	//messages handler
	void msgHandler();
	void sendBack(struct clientRecvDetails msg, SOCKET clientSocket);
	void clientLogInFunc(SOCKET clientSocket, struct clientRecvDetails msg);
	void clientFinishFunc(SOCKET clientSocket, struct clientRecvDetails msg);
	void clientExitFunc(SOCKET clientSocket, struct clientRecvDetails msg);
	void send101ToAll(string fileContent);

	void writeToFile(string what);
	string readFromFile();

	//threads - mutex, condition variables, locks
	std::mutex _msgMu;
	std::unique_lock<std::mutex> _msgLocker;
	std::condition_variable _condMsg;

	std::mutex _queueMsgMu;

};

