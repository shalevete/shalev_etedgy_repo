#include "Server.h"

Server::Server()
{
	this->_msgLocker = std::unique_lock<std::mutex>(this->_msgMu);
	this->_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	this->_numOfThreads = 0;
	if (this->_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
	this->_isAliveFlag = true;
}

Server::~Server()
{
	try
	{
		closesocket(this->_serverSocket);
	}
	catch (...) {}
}

void Server::start()
{
	cout << MSG_START << endl;

	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(6951); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
	cout << MSG_BINDED << endl;

	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	cout << MSG_lISTENING << endl;

	SOCKET client_socket;
	std::thread t1(&Server::msgHandler, this);
	while (this->_isAliveFlag)
	{
		cout << MSG_ACCEPTING_CLIENT << endl;
		client_socket = ::accept(this->_serverSocket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
		{
			throw std::exception(__FUNCTION__);
		}
		cout << MSG_CLIENT_ACCEPTED << endl;
		// the function that handle the conversation with the client
		//clientHandler(client_socket);
		std::thread t(&Server::clientHandler, this, client_socket);
		this->_numOfThreads++;
		t.detach();
	}
	t1.join();


}

void Server::clientHandler(SOCKET clientSocket)
{
	Helper serverHelper;
	bool isThreadAlive = true;
	int bytesNum;
	try
	{
		while (isThreadAlive)
		{

			struct clientRecvDetails recvDetails;
			recvDetails.typeCode = (MessageType)(serverHelper.getMessageTypeCode(clientSocket));
			switch (recvDetails.typeCode)
			{
			case MT_CLIENT_LOG_IN:
				bytesNum = REQ_USERNAME_SIZE;
				break;
			case MT_CLIENT_UPDATE:
				bytesNum = REQ_FILE_CONTENT_SIZE;
				break;
			case MT_CLIENT_FINISH:
				bytesNum = REQ_FILE_CONTENT_SIZE;
				break;
			case MT_CLIENT_EXIT:
				cout << MSG_RECV_EXIT << endl;
				clientExitFunc(clientSocket, recvDetails);
				send101ToAll(readFromFile());
				break;
			default:
				break;
			}
			if (recvDetails.typeCode != MT_CLIENT_EXIT)
			{
				cout << "	msg " << recvDetails.typeCode << "	bytesNum " << bytesNum;
				recvDetails.intPart = serverHelper.getIntPartFromSocket(clientSocket, bytesNum);
				cout << "	intPart " << recvDetails.intPart;
				recvDetails.stringPart = serverHelper.getStringPartFromSocket(clientSocket, recvDetails.intPart);
				cout << "	stringPart " << recvDetails.stringPart << endl;
				this->_queueMsgMu.lock();
				this->_messages.push(std::make_pair(recvDetails, clientSocket));
				this->_queueMsgMu.unlock();
				this->_condMsg.notify_one();
			}
			else
			{
				closesocket(clientSocket);
				this->_numOfThreads--;
				if (this->_numOfThreads == 0)
				{
					this->_isAliveFlag = false;
				}
				isThreadAlive = false;
			}
		}
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
		this->_numOfThreads--;
		if (this->_numOfThreads == 0)
		{
			this->_isAliveFlag = false;
		}
		isThreadAlive = false;
	}
};

void Server::msgHandler()
{
	struct clientRecvDetails msg;
	SOCKET clientSocket;
	while (this->_isAliveFlag)
	{
		this->_condMsg.wait(this->_msgLocker);
		this->_queueMsgMu.lock();
		msg = this->_messages.front().first;
		clientSocket = this->_messages.front().second;
		this->_messages.pop();
		this->_queueMsgMu.unlock();
		sendBack(msg, clientSocket);
	}

}

void Server::sendBack(struct clientRecvDetails msg, SOCKET clientSocket)
{
	Helper serverHelper;
	std::fstream myfile(FILE_NAME);
	switch (msg.typeCode)
	{
	case MT_CLIENT_LOG_IN:
		clientLogInFunc(clientSocket, msg);
		break;
	case MT_CLIENT_UPDATE:
		cout << MSG_RECV_UPDATE << endl;
		writeToFile(msg.stringPart);
		send101ToAll(msg.stringPart);
		break;
	case MT_CLIENT_FINISH:
		cout << MSG_RECV_FINISH << endl;
		clientFinishFunc(clientSocket, msg);
		break;
	default:
		break;
	}
}

void Server::clientLogInFunc(SOCKET clientSocket, struct clientRecvDetails msg)
{
	int placeInLine;
	Helper serverHelper;
	int length;

	string fileText = readFromFile();

	placeInLine = this->_users.size() + 1;
	this->_users.push_back(std::make_pair(msg.stringPart, clientSocket));
	this->_currUser = this->_users.front().first;
	if (this->_users.size() == 1)
	{
		this->_nextUser = this->_currUser;
	}
	else
	{
		this->_nextUser = this->_users[1].first;
	}
	serverHelper.sendUpdateMessageToClient(clientSocket, fileText, this->_currUser, this->_nextUser, placeInLine);
	cout << (MSG_CLIENT_ADDED, clientSocket, this->_currUser.c_str()) << endl;
}

string Server::readFromFile()
{
	string line;
	string fileText = "";

	std::ifstream myfile(FILE_NAME);
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			fileText += line + '\n';
		}
	}
	else
	{
		std::cerr << ERR_CANT_OPEN_FILE << endl;
	}
	return fileText;
}
void Server::writeToFile(string what)
{
	std::ofstream myfile(FILE_NAME);
	if (myfile.is_open())
	{
		myfile << what;
		myfile.close();
	}
	else
	{
		std::cerr << ERR_CANT_OPEN_FILE << endl;
	}
}

void Server::clientFinishFunc(SOCKET clientSocket, struct clientRecvDetails msg)
{
	int placeInLine;
	string username;
	Helper serverHelper;
	
	writeToFile(msg.stringPart);
	
	username = this->_users.front().first;
	this->_users.pop_front();
	placeInLine = this->_users.size() + 1;
	this->_users.push_back(std::make_pair(username, clientSocket));

	this->_currUser = this->_users.front().first;
	if (this->_users.size() == 1)
	{
		this->_nextUser = this->_currUser;
	}
	else
	{
		this->_nextUser = this->_users[1].first;
	}
	send101ToAll(msg.stringPart);
}

void Server::send101ToAll(string fileContent)
{
	Helper serverHelper;
	int i = 0;
	deque<pair<string, SOCKET>>::iterator it;
	for (it = this->_users.begin(); it != this->_users.end(); ++it)
	{
		i++;
		serverHelper.sendUpdateMessageToClient(it->second, fileContent, this->_currUser, this->_nextUser, i);
	}
	cout << MSG_SENT_UPDATE << endl;
}

void Server::clientExitFunc(SOCKET clientSocket, struct clientRecvDetails msg)
{
	string line;
	string fileText = "";
	bool found = false;
	string username;
	int i = 0;

	for (i = 0; ((i < this->_users.size()) && (!found)); i++)
	{
		if(this->_users[i].second == clientSocket)
		{
			this->_users.erase(this->_users.begin() + i);
			cout << (MSG_REMOVED, clientSocket) << endl;
			found = true;
		}
	}

	if (this->_users.size() != 0)
	{
		username = this->_users.front().first;
		this->_users.pop_front();
		this->_users.push_back(std::make_pair(username, clientSocket));
	}
	else
	{
		this->_isAliveFlag = false;
	}
}