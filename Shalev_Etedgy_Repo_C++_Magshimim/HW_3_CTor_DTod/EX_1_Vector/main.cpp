#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "Vector.h"

using std::cout;
using std::endl;
using std::string;

int main()
{
	//A
	Vector my_vec(5);
	my_vec.print();

	cout << endl;
	//B
	my_vec.push_back(5);
	my_vec.print();

	my_vec.push_back(7);
	my_vec.push_back(10);
	my_vec.print();

	my_vec.pop_back();
	my_vec.print();

	my_vec.pop_back();
	my_vec.push_back(9);
	my_vec.print();

	my_vec.push_back(10);
	my_vec.push_back(11);
	my_vec.push_back(12);
	my_vec.push_back(13);
	my_vec.print();

	my_vec.reserve(23);
	my_vec.print();

	my_vec.reserve(20);
	my_vec.print();

	my_vec.resize(29);
	my_vec.print();
	my_vec.resize(10);
	my_vec.print();

	my_vec.resize(15, 20);
	my_vec.print();

	my_vec.assign(50);
	my_vec.print();

	cout << endl;
	//C
	Vector my_vec2(my_vec);
	my_vec2.print();
	cout << "is my_vec2 and my_vec equals? " << (my_vec2 == my_vec) << endl;

	Vector my_vec3(7);
	my_vec2 = my_vec3;
	my_vec2.print();
	cout << "is my_vec2 and my_vec3 equals? " << (my_vec2 == my_vec3) << endl;
	cout << "is my_vec3 and my_vec equals? " << (my_vec3 == my_vec) << endl;
	
	cout << endl;
	//D

	cout << "element in the 5 place in my_vec: " << my_vec[5] << endl;
	cout << "element in the 50 place in my_vec: " << my_vec[50] << endl;

	cout << endl;

	system("pause");
	return 0;
}
