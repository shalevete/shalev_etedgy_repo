#include "Vector.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;

#define ERR_INDEX "error: n is higher than the capacity of the vector"
#define ERR_EMPTY_VEC "error: pop from empty vector"

void Vector::print() const
{
	cout << "size of the vector: " << this->size() << ", capacity of vector: "
		<< this->capacity() << ", resizeFactor of the vector: "
		<< this->resizeFactor()
		<< ", is the vector empty? " << this->empty() << endl;

	for (int i = 0; i < this->_size; i++)
	{
		cout << "element number " << i+1 << " : " << this->_elements[i] << endl;
	}

}

// A
Vector::Vector(int n)
{
	int i = 0;

	if (n < 2)
	{
		n = 2;	//If the value is less than 2 then the constructor will refer to it as 2

	}


	// allocates memory and initializes the intgers
	this->_elements = new int[n];
	for (i = 0; i < n; i++)
	{
		this->_elements[i] = EMPTY;
	}
	
	this->_capacity = n;
	this->_size = 0;
	this->_resizeFactor = n;
}

Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	return (this->_size == 0);
}

// B
void Vector::push_back(const int& val)
{
	int n = this->_size;
	int i = 0;

	if (this->_size < this->_capacity)
	{
		this->_elements[n] = val;
		this->_size++;
	}
	else
	{
		int* temp = new int[(this->_capacity + this->_resizeFactor)];
		for (i = 0; i < n; i++)
		{
			temp[i] = this->_elements[i];
		}

		delete[] _elements;
		_elements = nullptr;

		this->_elements = new int[(this->_capacity + this->_resizeFactor)];
		for (i = 0; i < n; i++)
		{
			 this->_elements[i] = temp[i];
		}

		delete[] temp;
		temp = nullptr;

		this->_elements[n] = val;
		this->_size++;
		this->_capacity = this->_capacity + this->_resizeFactor;
	}
}

int Vector::pop_back()
{
	int return_value = 0;

	if (this->_size > 0)
	{
		return_value = this->_elements[this->_size-1];
		this->_elements[this->_size - 1] = EMPTY;
		this->_size--;
	}
	else
	{
		std::cerr << ERR_EMPTY_VEC << endl;
		return_value = EMPTY;
	}

	return return_value;
}

void Vector::reserve(int n)
{
	int i = 0;
	if (n > this->_capacity)
	{
		int value_to_increase_in = this->_capacity + (this->_resizeFactor * ((n - this->_capacity) / this->_resizeFactor + bool((n - this->_capacity) % this->_resizeFactor)));	//we may need to increase the _capacity in the multiplication of _resizeFactor

		int *temp = new int[value_to_increase_in];
		
		for (i = 0; i < this->_capacity; i++)
		{
			temp[i] = this->_elements[i];
		}
		for (i = this->_capacity; i < n; i++)
		{
			temp[i] = EMPTY;
		}
		
		delete[] _elements;
		_elements = nullptr;

		this->_elements = new int[value_to_increase_in];

		for (i = 0; i < n; i++)
		{
			this->_elements[i] = temp[i];
		}

		delete[] temp;
		temp = nullptr;


		this->_capacity = value_to_increase_in;
	}
}

void Vector::resize(int n)
{

	if (n > this->_capacity)
	{
		this->reserve(n);

		while (this->_size < n)
		{
			this->push_back(EMPTY);
		}

	}
	else
	{
		while (n < this->_size)
		{
			this->pop_back();
			this->_size--;
		}
		while (this->_size < n)
		{
			this->push_back(EMPTY);
		}


	}
}

void Vector::assign(int val)
{
	while (this->_size < this->_capacity)
	{
		this->push_back(val);
	}
}

void Vector::resize(int n, const int& val)
{
	int size_before = this->_size;
	int i = 0;

	this->resize(n);

	for (i = size_before; i < this->_capacity; i++)
	{
		this->_elements[i] = val;
	}

}

// C
Vector::Vector(const Vector& other)
{
	*this = other; // uses copy operator - copy other object to this object
}

bool Vector::operator==(const Vector& other) const
{
	int i = 0;
	bool equal_flag = true;

	if (this->_size == other._size)
	{
		for (i = 0; i < this->_size; i++)
		{
			if (this->_elements[i] != other._elements[i])
			{
				equal_flag = false;
			}
		}
	}
	else
	{
		equal_flag = false;
	}

	return equal_flag;
}

Vector& Vector::operator=(const Vector& other)
{

	if (this == &other) // tries to copy the object to itself
	{
		return *this;
	}

	delete[] this->_elements; // release old memory

	// shallow copy fields
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;

	// deep copy dynamic fields (pointers/arrays)
	this->_elements = new int[_capacity];
	for (int i = 0; i < _capacity; i++)
	{
		// copies cell by cell
		this->_elements[i] = other._elements[i];
	}

	return *this;
}

// D
int& Vector::operator[](int n) const
{
	int return_value = 0;
	int i = 0;

	if (this->_capacity < n)
	{
		std::cerr << ERR_INDEX << endl;
		return_value = this->_elements[0];
	}
	else
	{
		return_value = this->_elements[n];
	}

	return return_value;
}
