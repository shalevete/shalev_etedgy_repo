#include <iostream>

bool isUniqueName(const std::string& name)
{
	// in a real word scenario we would have checked against a list of names
	return true;
}
bool isNotValid(char ch)
{
	return !isalpha(ch) && !isdigit(ch);
}
bool isValidUserName(const std::string& username)
{
	bool valid = false;
	if (username.length() >= MIN_NAME_LENGTH)
	{
		if (username.length() <= MAX_NAME_LENGTH)
		{
			if (isalpha(username[0]))
			{
				bool foundNotValidChar = std::find_if(username.begin(),
					username.end(), isNotValid) != username.end();
				if (!foundNotValidChar)
				{
					valid = isUniqueName(username);
				}
			}
		}
	}
	return valid;
}

/*
1. cheking if the the username is valid.
3. this code is longer than he supposed to be and one function does everything.
*/