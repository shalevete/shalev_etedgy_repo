#include <iostream>

class Instrument
{
public:
	virtual void play() const = 0;

};

class stringed : Instrument
{
	virtual void play() const = 0;
	virtual void playChord() const = 0;
};

class percussions : Instrument
{
	virtual void play() const = 0;
};

class Guitar : public stringed
{
public:
	void play() const
	{
		playChord();
	}

	void playChord() const
	{
		std::cout << "Am" << std::endl;
	}
};

class Drum : public percussions
{
	void play() const
	{
		std::cout << "Tom  tom" << std::endl;
	}
};

void Musician(const Instrument& instrument)
{
	instrument.play();
}/*EX4 - LSP1. yes because Guitar is an Instrument and Drum is an Instrument.*/