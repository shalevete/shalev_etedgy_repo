#include <iostream>

class Person
{
	std::string _firstName;
	std::string _lastName;
	std::string _email;
	bool validateEmail(const std::string& email)
	{
		if (email.find('@') == std::string::npos)
			return false;
		return true;
	}
public:
	Person(const std::string& firstName, const std::string& lastName, const std::string& email)
		: _firstName(firstName), _lastName(lastName)
	{
		if (!validateEmail(email))
			throw std::invalid_argument("email address not valid");
		_email = email;
	}
};


/*
in my opinion this code is not by the SRP because the class check the email.
*/