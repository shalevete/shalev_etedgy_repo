#include <iostream>

struct IPhone
{
	virtual void call() const = 0;
};struct IPhoneWfax : IPhone
{	virtual void call() const = 0;
	virtual void fax() const = 0;};struct IPhoneWsms : IPhone
{
	virtual void call() const = 0;
	virtual void sms() const = 0;
};

struct SmartPhone : public IPhoneWsms
{
public:
	virtual void call()
	{
		std::cout << "E.T. smartPhone" << std::endl;
	}
	virtual void sms()
	{
		std::cout << "E.T. sms" << std::endl;
	}
	virtual void fax()
	{
		throw std::runtime_error("unsupported instrument");
	}
};

/*
EX5 - ISP
2. not any phone have a fax or sms ability.
*/