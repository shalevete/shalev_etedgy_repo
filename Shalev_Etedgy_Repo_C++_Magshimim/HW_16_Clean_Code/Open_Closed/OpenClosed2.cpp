#include <iostream>

#define PI 3.14159265359 

#define TYPE_CIRCLE 1
#define TYPE_RECTANGLE 2 

struct Shape 
{
	virtual int getType() const = 0;
};

struct Circle : public Shape
{
	int x;
	int y;
	int radius;

	int getType() const { return TYPE_CIRCLE; }
};
struct Rectangle : public Shape
{
	int x1;
	int y1;
	int x2;
	int y2;

	int getType() const { return TYPE_RECTANGLE; }
};

double calcArea(Shape *shape) {
	double tmp = 0;
	switch (shape->getType())
	{
	case TYPE_CIRCLE:
	{
		Circle *pCircle = (Circle*)shape;
		tmp = PI * std::pow(pCircle->radius, 2);
		break;
	}

	case TYPE_RECTANGLE:
	{
		Rectangle *pRectangle = (Rectangle*)shape;
		tmp = (pRectangle->x2 - pRectangle->x1) * (pRectangle->y2 - pRectangle->y1);
		break;
	}
	default:
		throw std::invalid_argument("invalid shape");
	} 
	return tmp;
}