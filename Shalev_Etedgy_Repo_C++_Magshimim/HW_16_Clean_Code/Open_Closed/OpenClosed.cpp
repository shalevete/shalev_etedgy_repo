#include <iostream>

#define PI 3.14159265359

struct Circle
{
	int x;
	int y;
	int radius;
};

struct Rectangle
{
	int x1;
	int y1;
	int x2;
	int y2;
};

double calcArea(void *shape, int type)
{
	double tmp = 0;
	switch (type)
	{
	case 1:
	{
		Circle *pCircle = (Circle*)shape;
		tmp = PI * std::pow(pCircle->radius, 2);
		break;
	}
	case 2:
	{
		Rectangle *pRectangle = (Rectangle*)shape;
		tmp = (pRectangle->x2 - pRectangle->x1) * (pRectangle->y2 - pRectangle->y1);
		break;
	}
	}
	return tmp;
}



/*
ex3 - OCP 
1. There is a violation of OCP because it will be harder to add another shape to this code.
2. what happened if the type is not 1 or 0. MAGIC NUMBERS.
*/