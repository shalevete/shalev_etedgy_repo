#include <iostream>
#include "dry.cpp"


double distance(const Point& A, const Point& B)
{
	int x = B.x - A.x;
	int y = B.y - A.y;
	return std::sqrt(pow(x, 2) + pow(y, 2));
}

double calcTrianglePerimeter(const Point& A, const Point& B, const Point& C)
{
	return	distance(A, B) +
			distance(C, B) +
			distance(C, A);
}


int main()
{
	Point Point1;
	Point1.x = 7;
	Point1.y = 10;
	Point Point2;
	Point2.x = 20;
	Point2.y = 15;	
	Point Point3;
	Point3.x = 1;
	Point3.y = 2;

	std::cout << calcTrianglePerimeter(Point1, Point2, Point3);

	return 0;
}

/*
EX2 - DRY

2.  a) it will be easier to debug this code because if i want to change something i need to change this only in one place.

*/