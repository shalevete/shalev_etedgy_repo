#include <iostream>
#include <vector>

float a(const std::vector<int>& numbers)
{
	if (numbers.empty())
		throw std::exception("invalid");

	float sum = 0.0;
	for (std::vector<int>::const_iterator it = numbers.begin(), endOfVec = numbers.end(); it != endOfVec; ++it)
		sum += *it;
	return sum / numbers.size();
}/*the code sums the vector values.*/