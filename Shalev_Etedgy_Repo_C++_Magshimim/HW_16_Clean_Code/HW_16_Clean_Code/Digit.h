#pragma once
#include <iostream> 

class Digit
{
public:

	Digit(char digit);
	~Digit();

private:

	char _digit;
};