#include <iostream>
#include "Digit.h"

bool isNum(std::string number)
{
	for (char& dig : number)
	{
		try {
			Digit digit(dig);
		}
		catch (const std::exception& e) {
			return false;
		}
	}
	return true;

}

int main()
{
	if (isNum("78654"))
	{
		std::cout << "78654 is number\n";
	}
	if (!isNum("sa4"))
	{
		std::cout << "sa4 is not number\n";
	}
	if (!isNum("242as"))
	{
		std::cout << "242as is not number\n";
	}
	return 0;
}


/*
EX1
4. the code is not simple. the Digit class ia unnecessary I could simply implement the isNum() function like this:

bool isNum(std::string number)
{
	for (char& dig : number)
	{
		if(!isdigit(dig))
		{
			return false;
		}
	}
return true;
}
-------------------------------------------------------

*/