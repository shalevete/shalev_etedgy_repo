#include <iostream>

class Email
{
public:
	Email()
	{
	}
	void to(const std::string& to) {}
	void from(const std::string& from) {}
	void subject(const std::string& subject) {}
	void content(const std::string& content) {}
	void sendEmail() const
	{
		// Send email
	}
};

class Reminder
{
	Email *_pEmail;
public:
public:
	Reminder(const std::string& to,
		const std::string& from,
		const std::string& subject,
		const std::string& content) : _pEmail(nullptr)
	{
		_pEmail = new Email();
		_pEmail->to(to);
	}
	~Reminder()
	{
		delete _pEmail;
	}
	void sendReminder() const
	{
		_pEmail->sendEmail();
	}
};

/*
1. this implemention is not open to additions. it is hard to add another implementions and to understand the code.
*/