#include "Ribosome.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;

Protein * Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein protein;
	protein.init();
	Protein* proteinList = &protein;

	AminoAcid aminoAcidToAdd;

	string amino_acid_codon = "";

	int len = RNA_transcript.length();
	while(len > 2)
	{
		amino_acid_codon = RNA_transcript.substr(0, 3);

		RNA_transcript = RNA_transcript.substr(3);
		aminoAcidToAdd = get_amino_acid(amino_acid_codon);
		len = RNA_transcript.length();

		if(aminoAcidToAdd == UNKNOWN)
		{
			protein.init();
			return nullptr;
		}
		else
		{
			protein.add(aminoAcidToAdd);
		}
	}
	return proteinList;
}
