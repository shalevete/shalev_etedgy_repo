#include "Mitochondrion.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcidNode* PNodeOfAminoAcid;
	PNodeOfAminoAcid = protein.get_first();

	AminoAcidNode NodeOfAminoAcid;
	NodeOfAminoAcid = *PNodeOfAminoAcid;


	AminoAcid AminoAcid_to_glocuse_receptor[NUM_OF_AMINOACID_NEEDED] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END };

	int i = 0;
	bool all_aminoAcids_received = true;

	for (i = 0; i < NUM_OF_AMINOACID_NEEDED && all_aminoAcids_received; i++)
	{
		if (AminoAcid_to_glocuse_receptor[i] != NodeOfAminoAcid.get_data())
		{
			all_aminoAcids_received = false;
		}
		else
		{
			PNodeOfAminoAcid = NodeOfAminoAcid.get_next();
			if (PNodeOfAminoAcid)
			{
			NodeOfAminoAcid = *PNodeOfAminoAcid;
			}
		}
	}

	this->_has_glocuse_receptor = all_aminoAcids_received;

	if (all_aminoAcids_received)
	{
		this->_glocuse_level = 50;
	}

}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	bool glocuse_receptor = this->_has_glocuse_receptor;
	int glocuse_level = this->_glocuse_level;

	if ((glocuse_receptor == true) && (glocuse_level >= 50))
	{
		return true;
	}
	else
	{
		return false;
	}
}
