#ifndef RIBOSOME_H  
#define RIBOSOME_H  

#include <string>   // for std::string
#include "Protein.h"

class Ribosome
{
public:

	Protein * create_protein(std::string &RNA_transcript) const;

private:


};

#endif /* RIBOSOME_H */
