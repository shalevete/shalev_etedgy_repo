#include "Nucleus.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;

//Gene class

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

// helper method
void Gene::print() const
{
	cout << "Start: " << this->_start << endl
		<< "End: " << this->_end << endl
		<< this->get_on_complementary_dna_strand_string() << endl;

}

string Gene::get_on_complementary_dna_strand_string() const
{
	if (this->_on_complementary_dna_strand == false)
	{
		return "the gene is not on the complementary dna strand";
	}
	else
	{
		return "the gene is on the complementary dna strand";
	}

}

// getters
unsigned int Gene::get_start() const
{
	return this->_start;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

// setters
void Gene::setStart(const unsigned int newStart)
{
	this->_start = newStart;
}

void  Gene::setEnd(const unsigned int newEnd)
{
	this->_end = newEnd;
}

void Gene::set_on_complementary_dna_strand(const bool new_on_complementary_dna_strand)
{
	this->_on_complementary_dna_strand = new_on_complementary_dna_strand;
}

//-------------------------------------------------------------------------------------------------------

//Nucleus class

void Nucleus::init(const string dna_sequence)
{
	int i = 0;
	int errFlag = 0;

	for (i = 0; (i < dna_sequence.length()) || (errFlag == 1); i++)
	{
		if ((dna_sequence[i] != 'A') && (dna_sequence[i] != 'T') && (dna_sequence[i] != 'C') && (dna_sequence[i] != 'G'))
		{
			errFlag = 1;
		}
	}

	if (errFlag == 0)
	{
	this->_dna_strand = dna_sequence;
	this->_complementary_dna_strand = get_reversed_DNA_strand();;
	}
	else
	{
		std::cerr << "Invalid sequence (sequence including invalid chars not 'A', 'T', 'C', 'G')" << endl;
		_exit(1);
	}
}

/*
void Nucleus::print() const
{
	cout << "dna strand: " << this->_dna_strand << endl
		<< "complementary dna strand: " << this->_complementary_dna_strand << endl;
}

string Nucleus::get_dna_strand() const
{
	return this->_dna_strand;
}

string Nucleus::get_complementary_dna_strand() const
{
	return this->_complementary_dna_strand;
}

void Nucleus::set_dna_strand(const unsigned int new_dna_strand)
{
	this->_dna_strand = new_dna_strand;
}

void Nucleus::set_complementary_dna_strand(const unsigned int new_complementary_dna_strand)
{
	this->_complementary_dna_strand = new_complementary_dna_strand;
}
*/

string Nucleus::get_reversed_DNA_strand() const
{
	int i = 0;
	string nucleus_dna = this->_dna_strand;
	string reversed_DNA_strand = "";

	for (i = 0; i < this->_dna_strand.length(); i++)
	{
		if (nucleus_dna[i] == 'G')
		{
			reversed_DNA_strand += 'C';
		}
		else if (nucleus_dna[i] == 'C')
		{
			reversed_DNA_strand += 'G';
		}
		else if (nucleus_dna[i] == 'A')
		{
			reversed_DNA_strand += 'T';
		}
		else
		{
			reversed_DNA_strand += 'A';
		}
	}
	return reversed_DNA_strand;
}

string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string rna_transcript = "";
	string nucleus_dna = this->_dna_strand;
	string complementary_dna = this->_complementary_dna_strand;
	int i = 0;
	int start_of_gene = gene.get_start();
	int end_of_gene = gene.get_end();

	if (gene.is_on_complementary_dna_strand())
	{
		for (i = start_of_gene; i <= end_of_gene; i++)
		{
			if (complementary_dna[i] == 'T')
			{
				rna_transcript += 'U';
			}
			else
			{
				rna_transcript += complementary_dna[i];
			}
		}
	}
	else
	{
		get_reversed_DNA_strand();
		for (i = start_of_gene; i <= end_of_gene; i++)
		{
			if (nucleus_dna[i] == 'T')
			{
				rna_transcript += 'U';
			}
			else
			{
				rna_transcript += nucleus_dna[i];
			}
		}
	}
	return rna_transcript;
}


unsigned int Nucleus::get_num_of_codon_appearances(const string& codon) const
{
	int i = 0;
	int count = 0;
	std::size_t  codon_index = this->_dna_strand.find(codon);

	while (codon_index != string::npos)
	{
		count++;
		codon_index = this->_dna_strand.find(codon, codon_index + 1);
	}

	return count;
}
