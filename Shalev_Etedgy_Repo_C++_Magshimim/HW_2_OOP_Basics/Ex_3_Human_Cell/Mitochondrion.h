#ifndef MITOCHONDRION_H  
#define MITOCHONDRION_H  

#include <string>   // for std::string
#include "Ribosome.h"

//need protein that made from 7 amino acids in order to get glucose receptor
#define NUM_OF_AMINOACID_NEEDED 7

class Mitochondrion
{
public:

	void init();

	//setters
	void set_glucose(const unsigned int glocuse_units);
	void insert_glucose_receptor(const Protein & protein);

	// methods
	bool produceATP() const;

private:

	// fields
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;


};

#endif /* MITOCHONDRION_H */
