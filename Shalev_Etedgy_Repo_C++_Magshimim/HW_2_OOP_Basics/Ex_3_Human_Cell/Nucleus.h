#ifndef NUCLEUS_H  
#define NUCLEUS_H  

#include <string>   // for std::string



// initial value
#define EMPTY -1

class Gene
{
public:

	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);

	// getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;

	// setters
	void setStart(const unsigned int newStart);
	void setEnd(const unsigned int newEnd);
	void set_on_complementary_dna_strand(const bool new_on_complementary_dna_strand);

	// methods

private:

	// fields
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;

	// helper method
	std::string get_on_complementary_dna_strand_string() const;
	void print() const;

};


class Nucleus
{
public:

	void init(const std::string dna_sequence);

	// getters
//	std::string get_dna_strand() const;
//	std::string get_complementary_dna_strand() const;

	// setters
//	void set_dna_strand(const unsigned int new_complementary_dna_strand);
//	void set_complementary_dna_strand(const unsigned int new_complementary_dna_strand);

	// methods
	std::string get_reversed_DNA_strand() const;
	std::string get_RNA_transcript(const Gene& gene) const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
private:

	// fields
	std::string _dna_strand;
	std::string _complementary_dna_strand;


	// helper method
//	void print() const;

};
#endif /* NUCLEUS_H */
