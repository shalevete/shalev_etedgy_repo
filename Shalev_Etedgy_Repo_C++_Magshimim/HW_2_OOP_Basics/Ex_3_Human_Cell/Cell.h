#ifndef CELL_H  
#define CELL_H  

#include <string>   // for std::string
#include "Mitochondrion.h"


class Cell
{
public:

	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);

	// getters

	// setters

	// methods
	bool get_ATP();

private:

	// fields
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;

	// helper method

};


#endif /* CELL_H */
