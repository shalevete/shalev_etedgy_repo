#include "Cell.h"
#include <iostream>

using std::cout;
using std::endl;
using std::string;


void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
	this->_atp_units = 0;
}

bool Cell::get_ATP()
{
	string RNA_transcrip = "";
	Gene glocus_receptor_gene = this->_glocus_receptor_gene;
	RNA_transcrip = this->_nucleus.get_RNA_transcript(glocus_receptor_gene);

	Protein* Psuitable_protein;
	Psuitable_protein = this->_ribosome.create_protein(RNA_transcrip);
	
	if (Psuitable_protein == nullptr)
	{
		std::cerr << "The ribosome could not produce the protein" << endl;
		_exit(1);
	}

	Protein suitable_protein;
	suitable_protein = *Psuitable_protein;

	this->_mitochondrion.insert_glucose_receptor(suitable_protein);
	
	if (this->_mitochondrion.produceATP())
	{
		this->_atp_units = 100;
		return true;
	}
	else
	{
		return false;
	}


}