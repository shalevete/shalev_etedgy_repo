#pragma once
#ifndef HEXAGON_H
#define HEXAGON_H
#include "shape.h"
#include <iostream>

#define GOOD_ANGLES ((ang < 0) || (ang > 180) || (ang2 < 0) || (ang2 > 180))

class Hexagon : public Shape {

public:
	Hexagon(std::string nam, std::string col, int side);
	void draw();

	void setSide(int side);
	double getSide();
	double CalArea();

private:
	int _side;

};
#endif;