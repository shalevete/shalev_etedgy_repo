#pragma once
#pragma once
#ifndef PENTAGON_H
#define PENTAGON_H
#include "shape.h"
#include <iostream>

#define GOOD_ANGLES ((ang < 0) || (ang > 180) || (ang2 < 0) || (ang2 > 180))

class Pentagon : public Shape {

public:
	Pentagon(std::string nam, std::string col, int side);
	void draw();

	void setSide(int side);
	double getSide();
	double CalArea();

private:
	int _side;

};
#endif;