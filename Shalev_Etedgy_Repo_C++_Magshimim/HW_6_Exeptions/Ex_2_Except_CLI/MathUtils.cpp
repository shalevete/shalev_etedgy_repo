#include "MathUtils.h"
#include <iostream>

double MathUtils::CalPentagonArea(double side)
{
	//		     ______________________
	//	1       /			      ___  
	//	-  X   /  5 X ( 5 + 2 X \/ 5  )   X a X a
	//	4    \/

	 return (sqrt(5 * (5 + 2 * (sqrt(5)))) * side * side) / 4;
}

double MathUtils::CalHexagonArea(double side)
{
	//       __
	//(3 X \/3  X (side) X 2 ) / 2

	return ((3 * sqrt(3) * (side * side)) / 2);
}