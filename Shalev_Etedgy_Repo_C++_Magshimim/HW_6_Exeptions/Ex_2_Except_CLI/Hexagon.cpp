#include "Hexagon.h"
#include "shape.h"
#include "shapeException.h"
#include "MathUtils.h"
#include <iostream>

Hexagon::Hexagon(std::string nam, std::string col, int side) :Shape(nam, col) {
	setSide(side);
}
void Hexagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl 
		<< "Area is " << CalArea() << std::endl;
}

double Hexagon::CalArea() {

	return MathUtils::CalHexagonArea(this->_side);
}
void Hexagon::setSide(int side) {
	if (side < 0)
	{
		throw shapeException();
	}

	this->_side = side;
}

double Hexagon::getSide()
{
	return this->_side;
}