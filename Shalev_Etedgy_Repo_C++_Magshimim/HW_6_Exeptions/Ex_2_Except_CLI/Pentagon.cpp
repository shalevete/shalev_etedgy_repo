#include "Pentagon.h"
#include "shape.h"
#include "shapeException.h"
#include "MathUtils.h"
#include <iostream>

Pentagon::Pentagon(std::string nam, std::string col, int side) :Shape(nam, col) {
	setSide(side);
}
void Pentagon::draw()
{
	std::cout << getName() << std::endl << getColor() << std::endl
		<< "Area is " << CalArea() << std::endl;
}

double Pentagon::CalArea() {

	return MathUtils::CalPentagonArea(this->_side);
}
void Pentagon::setSide(int side) {
	if (side < 0)
	{
		throw shapeException();
	}

	this->_side = side;
}

double Pentagon::getSide()
{
	return this->_side;
}