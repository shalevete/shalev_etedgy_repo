#include <iostream>

#define CHECK_REQUIREMENTS ((num1 >= 8200) || (num2 >= 8200) || (add(num1, num2) == 8200) || (multiply(num1, num2) == 8200) || (pow(num1, num2) == 8200))
#define ERR_8200 "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"

int add(int a, int b) {
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void) {

	int num1 = 0;
	int num2 = 0;

	std::cout << "enter 2 numers" << std::endl;
	std::cin >> num1;
	std::cin >> num2;

	if (CHECK_REQUIREMENTS)
	{
		std::cerr << ERR_8200 << std::endl;
	}
	else
	{
		std::cout << pow(num1, num2) << std::endl;
	}

	system("pause");
	return 0;
}