#include <iostream>

#define ERR_8200 "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year"

enum Errors { numEqual8200 , intInFuncIs8200 };

int add(int a, int b) {
	if (a == 8200 || b == 8200)
	{
		throw(intInFuncIs8200);
	}

	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;

	if (a == 8200 || b == 8200)
	{
		throw(intInFuncIs8200);
	}

	for (int i = 0; i < b; i++) {
		if (i == 8200 || sum == 8200)
		{
			throw(intInFuncIs8200);
		}
		sum = add(sum, a);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;

	if (a == 8200 || b == 8200)
	{
		throw(numEqual8200);
	}

	for (int i = 0; i < b; i++) {
		if (i == 8200 || exponent == 8200)
		{
			throw(intInFuncIs8200);
		}
		exponent = multiply(exponent, a);
	};
	return exponent;
}

int main(void) {

	int num1 = 0;
	int num2 = 0;

	std::cout << "enter 2 numers" << std::endl;
	std::cin >> num1;
	std::cin >> num2;

	try
	{
		std::cout << pow(num1, num2) << std::endl;
	}
	catch (const Errors e)
	{
		std::cerr << ERR_8200 << std::endl;;
		switch (e)
		{
		case numEqual8200:
			std::cerr << "one of the numbers is 8200" << std::endl;
			break;

		case intInFuncIs8200:
			std::cerr << "one of the the integers that declared in a function is 8200" << std::endl;
			break;

		default:
			break;
		}
		
	}

	system("pause");
	return 0;
}
