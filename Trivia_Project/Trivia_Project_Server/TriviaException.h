#pragma once
#include <exception>
#include <iostream>

class TriviaException : public std::exception
{
public:
	TriviaException(std::string err);
	TriviaException();

	~TriviaException();
	virtual const char* what() const throw()
	{
		return this->_err.c_str();
	}
private:
	std::string _err;
};

