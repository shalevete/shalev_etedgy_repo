#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Helper.h"
#include "WSAInitializer.h"
#include "LoginResponse.h"
#include "JsonResponsePacketSerializer.h"
#include "ThreadPool.h"
#include <map>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <boost/asio/thread_pool.hpp>
#include <boost/asio/strand.hpp>
#include "TriviaException.h"

#define THREAD_POOL_NUMBER 3
#define DATA_FILE "data.txt"
#define SERVE_ERR __FUNCTION__ " - socket"
#define UNK_ERR "Unknown Error \n"
#define BIND_ERR __FUNCTION__ " - bind"
#define LISTEN_ERR __FUNCTION__ " - listen"
#define SERVER_PORT 70
#define SLEEP_TIME 500
#define ERR_REQ_ID -3


class Communicator
{
public:
	Communicator();
	~Communicator();
	void bindAndListen(); //Turn on server
	void handleRequests(); //Handle server request's
	void handleRequest(SOCKET clientSocket); //Handle request of client
private:
	
	void accept(); //Accept client
	std::map<SOCKET, IRequestHandler*> m_clients; //Map of clients
	std::map<SOCKET, bool> _handled; //map of handled client ot not
	std::map<SOCKET, LoggedUser> *_m_loggedUsers;
	RequestHandlerFactory * m_handleFactory; 
	SOCKET _serverSocket; //Server socket
	WSAInitializer _wsaInit; //Wsa
	std::mutex _clientMutex; //Mutex's  
	std::mutex _mongoMutex;
	std::mutex _mapHandled;
	std::unique_lock<std::mutex> _mapAdd;
	bool _serverRun; //Server run
	void changeClientHandledState(SOCKET clientSocket, bool state); //Change user state in map
	void disconnectClient(SOCKET clientSocket,std::string errorMsg);
	bool checkIfSocketHasData(SOCKET clientSocket);
	Request getRequest(SOCKET clientSocket);

};

