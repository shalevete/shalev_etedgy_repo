#include "LoginRequestHandler.h"




LoginRequestHandler::LoginRequestHandler(LoginManager * loginManager, RequestHandlerFactory * handlerManager, SOCKET user)
{
	this->m_handlerFactory = handlerManager;
	this->_user = user;
	this->m_loginManager = loginManager;
}

LoginRequestHandler::~LoginRequestHandler()
{

}

bool LoginRequestHandler::isRequestRelevant(Request req)
{
	switch (packetStateCode(req.requestId))
	{
	case P_ERROR:
		
		return false;
		break;
	case LOGIN:
		return true;
		break;
	case SIGN_UP:
		return true;
		break;
	default:
		return false;
		break;
	}
}

RequestResult LoginRequestHandler::handleRequest(Request req)
{
	if (req.requestId == LOGIN)
	{
		return login(req);
	}
	else if(req.requestId == SIGN_UP)
	{
		return signup(req);
	}
	else
	{
		throw  TriviaException("Handle request error");
	}
}

RequestResult LoginRequestHandler::login(Request req)
{
	char * buff = { 0 };
	 buff = reinterpret_cast<char*>(&req.buffer[0]);
	std::cout << buff << std::endl;
	JsonRequestPacketDeserializer Deserialize;
	LoginRequest LoginReq = Deserialize.deserializeLoginRequest((byte*)buff);
	std::vector<byte> buffRes;
	JsonResponsePacketSerializer LoginSerialize;

	if (this->m_loginManager->login(LoginReq.username, LoginReq.password,this->_user))
	{
		buffRes = LoginSerialize.serializeResponse(LoginResponse(1));
		RequestResult requestResLogin(buffRes, this->m_handlerFactory->createMenuRequestHandler(new LoggedUser(LoginReq.username)));
		
		return requestResLogin;
	}
	else
	{
		buffRes = LoginSerialize.serializeResponse(LoginResponse(0));
		RequestResult requestResLogin(buffRes, this);
		return requestResLogin;
	}
}

RequestResult LoginRequestHandler::signup(Request req)
{
	char * buff = { 0 };
	int signUpSuccess = 0;
	buff = reinterpret_cast<char*>(&req.buffer[0]);
	JsonRequestPacketDeserializer Deserialize;
	SignupRequest SignupReq = Deserialize.deserializeSignupRequest((byte*)buff);
	IRequestHandler * handler = this;
	if (this->m_loginManager->signup(SignupReq.username, SignupReq.password, SignupReq.email, this->_user))
	{
		handler = this->m_handlerFactory->createMenuRequestHandler(new LoggedUser(SignupReq.username));
		signUpSuccess = 1;
	}
	
	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	buffRes = resSerialize.serializeResponse(SignUpResponse(signUpSuccess));
	RequestResult requestResSignup(buffRes, handler);
	
	return requestResSignup;

}
