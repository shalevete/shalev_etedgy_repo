#pragma once
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>

class Question
{
public:
	Question();
	Question(std::string question, std::vector<std::string> wrongAns, std::string rightAns);
	~Question();
	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	std::string getCorrectAns();
private:
	std::string _m_question;
	std::vector<std::string> _m_possible_answers;
	std::string _rightAns;
};

