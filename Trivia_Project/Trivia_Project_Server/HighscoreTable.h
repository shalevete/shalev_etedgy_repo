#pragma once
#include "IDatabase.h"

class HighscoreTable
{
public:
	HighscoreTable(IDatabase * db);
	~HighscoreTable();
	std::map<LoggedUser, int> getHighScores();
	std::vector<int> getStatus(LoggedUser user);
	bool resetScores();
private:
	IDatabase * _db;
};

