#include "GameManager.h"

GameManager::GameManager(IDatabase* db)
{
	this->m_database = db;
}


GameManager::~GameManager()
{
}

//Game(std::vector<Question>, std::map<LoggedUser, GameData>);
Game* GameManager::createGame(Room* room)
{
	std::map<LoggedUser, GameData> usersMap;
	std::vector<Question> questions = this->m_database->getQuestions(room->getRoomMetaData().questionsCount);
	for (LoggedUser user : room->getAllUsers()) 
	{
		usersMap.insert(std::pair<LoggedUser, GameData>(user, GameData(questions[0],0,0,0)));
	}
	return new Game(questions, usersMap);

}

void GameManager::deleteGame(Game *game) 
{
	this->m_games.erase(std::remove(this->m_games.begin(), this->m_games.end(), game), this->m_games.end());
	delete(game);
}
