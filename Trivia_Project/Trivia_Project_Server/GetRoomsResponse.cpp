#include "GetRoomsResponse.h"

GetRoomsResponse::GetRoomsResponse(unsigned int status, std::vector<Room*> rooms)
{
	this->_status = status;
	this->_rooms = rooms;
}


GetRoomsResponse::GetRoomsResponse()
{
	this->_status = 0;
}

GetRoomsResponse::~GetRoomsResponse()
{

}
