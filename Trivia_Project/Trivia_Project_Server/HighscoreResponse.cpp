#include "HighscoreResponse.h"

HighscoreResponse::HighscoreResponse(unsigned int status, std::map<LoggedUser, int> highscores)
{
	this->_highscores = highscores;
	this->_status = status;
}


HighscoreResponse::HighscoreResponse()
{
	this->_status = 0;
}

HighscoreResponse::~HighscoreResponse()
{

}
