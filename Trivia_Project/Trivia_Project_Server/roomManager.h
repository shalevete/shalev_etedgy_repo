#pragma once
#include "Room.h"
#include <map>
#include <iostream>

class roomManager
{
public:
	roomManager();
	~roomManager();
	Room * createRoom(LoggedUser user, std::string name, std::string maxPlayers, unsigned int timeForQuestion, unsigned int questionCount);
	void deleteRoom(int roomId);
	byte getRoomState(int roomId);
	std::vector<Room*> getRooms();
private:
	std::map<byte, Room*> _m_rooms;
};
