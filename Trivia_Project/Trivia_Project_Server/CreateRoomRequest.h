#pragma once
#include <iostream>

class CreateRoomRequest
{
public:
	CreateRoomRequest(std::string roomName, unsigned int maxUsers, unsigned int questionCount, unsigned int answerTimeout);
	CreateRoomRequest();
	~CreateRoomRequest();

	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;

};