#pragma once
#include "LoggedUser.h"
#include "IDatabase.h"
#include <algorithm>
#include "WSAInitializer.h"


class LoginManager
{
public:
	LoginManager(IDatabase* db, std::map<SOCKET, LoggedUser> * m_users);
	~LoginManager();

	bool signup(std::string, std::string, std::string, SOCKET  user);
	bool login(std::string, std::string,  SOCKET  user);
	bool logout();

private:
	IDatabase * _m_database;
	std::map<SOCKET, LoggedUser> *_m_loggedUsers;
	bool checkIfUserConnected(LoggedUser user);

};
