#pragma once
#include "LoginManager.h"
#include "IRequestHandler.h"
#include "IDatabase.h"
#include "roomManager.h"
#include "HighscoreTable.h"
#include "GameManager.h"
#include <iostream>
#include <map>

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(std::map<SOCKET, LoggedUser> *_m_loggedUsers);
	~RequestHandlerFactory();
	IRequestHandler* createLoginRequestHandler(SOCKET client);
	IRequestHandler* createMenuRequestHandler(LoggedUser * user);
	IRequestHandler* createRoomAdminRequestHandler(LoggedUser * user, Room *room);
	IRequestHandler* createRoomMemberRequestHandler(LoggedUser * user, Room *room);
	IRequestHandler* createGameRequestHandler(LoggedUser * user, Room *room);

private:
	roomManager* _m_roomManager;
	IDatabase* _m_db;
	HighscoreTable* _m_highScore;
	GameManager * _m_gameManager;
	LoginManager* _m_loginManager;
	std::map<Room*, Game*> _roomGameMap;
};