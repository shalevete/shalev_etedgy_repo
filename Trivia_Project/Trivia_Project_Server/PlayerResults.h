#pragma once
#include <iostream>
#include <string>
class PlayerResults
{
public:
	PlayerResults(std::string username , unsigned int correctAnswerCount, unsigned int wrongAnswerCount, unsigned int averageAnswerTime);
	~PlayerResults();
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;


};

