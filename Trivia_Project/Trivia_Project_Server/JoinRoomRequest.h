#pragma once
#include <iostream>

class JoinRoomRequest
{
public:
	JoinRoomRequest(unsigned int roomId);
	JoinRoomRequest();
	~JoinRoomRequest();

	unsigned int roomId;
};

