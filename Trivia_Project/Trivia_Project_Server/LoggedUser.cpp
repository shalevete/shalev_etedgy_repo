#include "LoggedUser.h"


/*
C'tor
*/
LoggedUser::LoggedUser(std::string username)
{
	this->_username = username;
}

/*
D'tor
*/
LoggedUser::LoggedUser() 
{
}
LoggedUser::~LoggedUser()
{
	return;
}
/*
Get username from class
*/
std::string LoggedUser::getUsername() const
{
	return this->_username;
}
bool LoggedUser::operator==(const LoggedUser& user) const
{
	return user.getUsername() == this->_username;
}
bool LoggedUser::operator<(const LoggedUser& user) const
{
	return user.getUsername() < this->_username;
}