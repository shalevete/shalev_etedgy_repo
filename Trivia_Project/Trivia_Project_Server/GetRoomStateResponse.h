#pragma once
#include <vector>

class GetRoomStateResponse
{
public:
	GetRoomStateResponse();
	GetRoomStateResponse(unsigned int status, bool gameState, std::vector<std::string> players, unsigned int questionCount, unsigned int answerTimeout);
	~GetRoomStateResponse();
	unsigned int _status;
	bool _hasGameBegun;
	std::vector<std::string> _players;
	unsigned int _questionCount;
	unsigned int _answerTimeout;

};

