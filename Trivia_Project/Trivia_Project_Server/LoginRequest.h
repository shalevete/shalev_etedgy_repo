#pragma once
#include <iostream>
class LoginRequest
{
public:
	LoginRequest(std::string username, std::string password);
	LoginRequest();
	~LoginRequest();

	std::string username;
	std::string password;
};

