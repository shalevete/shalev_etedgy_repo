#pragma once
#include <iostream>

class LoggedUser
{
public:
	LoggedUser(std::string username); //C'tor of LoggedUser
	LoggedUser();
	~LoggedUser(); //D'tor
	std::string getUsername() const; //Get LoggedUser username
	bool operator==(const LoggedUser& user) const;
	bool operator<(const LoggedUser& user) const;
private:
	std::string _username;
};

