#include "Question.h"

Question::Question()
{
}

Question::Question(std::string question, std::vector<std::string> wrongAns, std::string rightAns)
{
	this->_m_question = question;
	this->_m_possible_answers = wrongAns;
	this->_m_possible_answers.push_back(rightAns);
	this->_rightAns = rightAns;
	std::random_shuffle(_m_possible_answers.begin(), _m_possible_answers.end());

}


Question::~Question()
{
}
std::string Question::getQuestion() 
{
	return this->_m_question;
}
std::vector<std::string> Question::getPossibleAnswers() 
{
	return this->_m_possible_answers;
}
std::string Question::getCorrectAns() 
{
	return this->_rightAns;
}
