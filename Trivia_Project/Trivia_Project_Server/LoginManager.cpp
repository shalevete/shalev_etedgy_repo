#include "LoginManager.h"




LoginManager::LoginManager(IDatabase * db, std::map<SOCKET,LoggedUser> * m_users)
{
	this->_m_database = db;
	this->_m_loggedUsers = m_users;
}

LoginManager::~LoginManager()
{

}

bool LoginManager::signup(std::string username, std::string password, std::string email, SOCKET userSocket)
{
	bool signUp = false;
	if (!this->_m_database->doesUsernameExsits(username)) 
	{
		signUp = true;
		this->_m_database->addUserToDb(username, password, email);
		LoggedUser user(username) ;
		this->_m_loggedUsers->insert(std::pair<SOCKET, LoggedUser>(userSocket, user));
	}
	return signUp;
	
}

bool LoginManager::login(std::string username, std::string password, SOCKET userSocket)
{
	LoggedUser user(username);
	if (this->_m_database->login(username, password) && !checkIfUserConnected(user))
	{
		this->_m_loggedUsers->insert(std::pair<SOCKET, LoggedUser>(userSocket, user));
	}
	else
	{
		return false;
	}
	return true;
}

bool LoginManager::logout()
{
	return true;
}
bool LoginManager::checkIfUserConnected(LoggedUser user)
{
	bool connected = false;
	for (auto it = this->_m_loggedUsers->begin(); it != this->_m_loggedUsers->end(); ++it)
	{
		if (it->second.getUsername() == user.getUsername()) 
	    {
			connected = true; 
			break;
	    }
	}
	return connected;
}