#include "GameRequestHandler.h"
#include "JsonResponsePacketSerializer.h"



GameRequestHandler::GameRequestHandler(Game* g, Room * room, roomManager* roomM, LoggedUser* user, GameManager* gManager, RequestHandlerFactory* handler, IDatabase* db)

{
	this->_m_db = db;
	this->m_user = user;
	this->_room = room;
	this->m_room = roomM;
	this->m_game = g;
	this->m_handlerFacroty = handler;
	this->m_gameManager = gManager;

}

GameRequestHandler::~GameRequestHandler()
{
}

bool GameRequestHandler::isRequestRelevant(Request req)
{
	bool relevant = false;
	
	switch (packetStateCode(req.requestId))
	{
	case P_ERROR:
		relevant = false;
		break;
	case Get_Question_Response:
		relevant = true;
		break;
	case Submit_Answer_Response:
		relevant = true;
		break;
	case Get_Game_Results_Response:
		relevant = true;
		break;
	default:
		relevant = false;
		break;
	}
	return relevant;
}

RequestResult GameRequestHandler::handleRequest(Request req)
{
	switch (packetStateCode(req.requestId))
	{
	case P_ERROR:
		throw std::runtime_error("error in request");
		break;
	case Get_Question_Response:
		return this->getQuestion(req);
		break;
	case Submit_Answer_Response:
		return this->submitAnswer(req);
		break;
	case Get_Game_Results_Response:
		return this->getGameResults(req);
		break;
	default:
		throw std::runtime_error("error in request");
		break;
	}
}


RequestResult GameRequestHandler::getQuestion(Request req)
{
	IRequestHandler * handler = this;
	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	
	Question question = this->m_game->getQuestionForUser(*(this->m_user));
	std::string q = question.getQuestion();
	std::map<unsigned int, std::string> answers;
	int rightAnsIndex = 0;
	
	int i = 0;
	for (i = 0; i < question.getPossibleAnswers().size(); i++)
	{
		if (question.getCorrectAns() == question.getPossibleAnswers()[i]) 
		{
			rightAnsIndex = i;
		}
		answers.insert(std::make_pair(i, question.getPossibleAnswers()[i]));
	}

	buffRes = resSerialize.serializeResponse(GetQuestionResponse(1, rightAnsIndex,q, answers));

	RequestResult requestRes(buffRes, handler);
	return requestRes;
}

RequestResult GameRequestHandler::submitAnswer(Request req)
{
	char * buff = { 0 };
	buff = reinterpret_cast<char*>(&req.buffer[0]);
	IRequestHandler * handler = this;
	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	SubmitAnswerRequest submit =  JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest((byte*)buff);
	
	buffRes = resSerialize.serializeResponse(SubmitAnswerResponse(1,this->m_game->submitAnswer(*this->m_user,submit.answerTrue)));


	RequestResult requestRes(buffRes, handler);
	return requestRes;
}

bool comparePlayerRes(PlayerResults p1, PlayerResults p2) 
{
	return p1.correctAnswerCount > p2.correctAnswerCount;
}


void GameRequestHandler::addToHighscores(std::vector<PlayerResults> pls) 
{
	for (auto p : pls) 
	{
		this->_m_db->addHighscoreVal(p.correctAnswerCount, p.wrongAnswerCount, p.username);
	}
}
RequestResult GameRequestHandler::getGameResults(Request req)
{
	IRequestHandler * handler = this;
	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	std::vector<PlayerResults> playerRes;
	
	for (std::pair<LoggedUser, GameData> user : this->m_game->getUsersGameStats()) 
	{
		playerRes.push_back(PlayerResults(user.first.getUsername(), user.second.correctAnswerCount, user.second.wrongAnswerCount, user.second.averangeAnswerTime));
	}
	std::sort(playerRes.begin(), playerRes.end(), comparePlayerRes);
	leaveGame(playerRes);

	buffRes = resSerialize.serializeResponse(GetGameResultsResponse(1,playerRes));
	RequestResult requestRes(buffRes, this->m_handlerFacroty->createMenuRequestHandler(this->m_user));
	
	return requestRes;
}


void GameRequestHandler::leaveGame(std::vector<PlayerResults> pls)
{
	try
	{
		this->m_room->deleteRoom(this->_room->getRoomMetaData().id);
		delete(this->_room);
		this->_room = nullptr;
		addToHighscores(pls);
	}
	catch (...)
	{
		return;
	}
		
	

}

