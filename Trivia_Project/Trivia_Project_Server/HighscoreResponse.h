#pragma once
#include <iostream>
#include <vector>
#include "LoginResponse.h"
#include <map>
#include "LoggedUser.h"
//#include "Highscore.h"

class HighscoreResponse
{
public:
	HighscoreResponse(unsigned int status, std::map<LoggedUser, int> h);
	HighscoreResponse();
	~HighscoreResponse();

	unsigned int _status;
	std::map<LoggedUser, int> _highscores;
};