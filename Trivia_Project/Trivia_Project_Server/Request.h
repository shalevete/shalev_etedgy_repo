#pragma once
#include <iostream>
#include <vector>
typedef unsigned char byte;
class Request
{
public:
	Request(); //C'tor
	Request(int requestId, int rTime,byte * data); //C'tor
	~Request(); //D'tor
	//In the UML this feild's are public
	int requestId; 
	int reciveTime;
	std::vector<byte> buffer;
};

