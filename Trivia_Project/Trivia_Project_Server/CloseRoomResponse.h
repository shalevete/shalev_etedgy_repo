#pragma once
class CloseRoomResponse
{
public:
	CloseRoomResponse(unsigned int status);
	~CloseRoomResponse();
	unsigned int _status;
};

