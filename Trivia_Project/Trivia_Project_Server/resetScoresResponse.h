#pragma once
class resetScoresResponse
{
public:
	resetScoresResponse(unsigned int status, bool issucceeded);
	~resetScoresResponse();
	unsigned int _status;
	bool isSucceeded;
};

