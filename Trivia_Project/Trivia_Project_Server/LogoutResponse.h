#pragma once
#include <iostream>
#include "LoginResponse.h"

class LogoutResponse
{
public:
	LogoutResponse(unsigned int status);
	LogoutResponse();
	~LogoutResponse();

	unsigned int _status;
};

