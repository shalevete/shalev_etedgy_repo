#include "RoomMemberRequestHandler.h"
#include "JsonResponsePacketSerializer.h"



RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser* user, roomManager * roomManagers, RequestHandlerFactory * handler, Room* room)
{
	this->_m_user = user;
	this->_m_room = roomManagers;
	this->_m_handler_factory = handler;
	this->_room = room;

}

RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}

bool RoomMemberRequestHandler::isRequestRelevant(Request req)
{
	bool relevant = false;
	switch (packetStateCode(req.requestId))
	{
	case P_ERROR:
		relevant = false;
		break;
	case LEAVE_ROOM_RES:
		relevant = true;
		break;
	case GET_ROOM_STATE:
		relevant = true;
		break;
	case START_GAME_RES:
		relevant = true;
		break;
	default:
		relevant = false;
		break;
	}
	return relevant;
}

RequestResult RoomMemberRequestHandler::handleRequest(Request req)
{
	switch (packetStateCode(req.requestId))
	{
	case LEAVE_ROOM_RES:
		return leaveRoom(req);
		break;
	case GET_ROOM_STATE:
		return getRoomState(req);
		break;
	case START_GAME_RES:
		return startGame(req);
		break;
	default:
		throw  TriviaException("Handle request error");

		break;
	}

}
RequestResult RoomMemberRequestHandler::startGame(Request req) 
{
	std::vector<byte> buffRes;
	buffRes = JsonResponsePacketSerializer::serializeResponse(StartGameResponse(STATUS_POS));
	return RequestResult(buffRes, this->_m_handler_factory->createGameRequestHandler(this->_m_user,this->_room));
}

RequestResult RoomMemberRequestHandler::leaveRoom(Request req)
{
	std::vector<byte> buffRes;
	try // If room deleted
	{
		this->_room->getRoomMetaData().isActive;
		this->_room->removeUser(this->_m_user->getUsername());
	}
	catch (...) 
	{

	}
	buffRes = JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse(STATUS_POS));
	return RequestResult(buffRes, this->_m_handler_factory->createMenuRequestHandler(this->_m_user));
}

RequestResult RoomMemberRequestHandler::getRoomState(Request req)
{
	std::vector<byte> buffRes;
	std::vector<std::string> players;
	try // If room deleted
	{
		for (auto elem : this->_room->getAllUsers())
		{
			players.push_back(elem.getUsername());
		}
		GetRoomStateResponse res(STATUS_POS, this->_room->getRoomMetaData().isActive == 0, players, this->_room->getRoomMetaData().questionsCount, this->_room->getRoomMetaData().timePerQuestion);
		buffRes = JsonResponsePacketSerializer::serializeResponse(res);
	}
	catch (...) 
	{
		GetRoomStateResponse res(0, false , players, 0, 0);
		buffRes = JsonResponsePacketSerializer::serializeResponse(res);
	}
	return RequestResult(buffRes, this);
}
