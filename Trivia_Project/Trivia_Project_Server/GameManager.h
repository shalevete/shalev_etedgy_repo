#pragma once
#include "Game.h"
#include "IDatabase.h"
#include "Room.h"
#include <map>
#include <algorithm>
class GameManager
{
public:
	GameManager(IDatabase *);
	~GameManager();

	Game* createGame(Room*);
	void deleteGame(Game *game);

private:
	IDatabase* m_database;
	std::vector<Game*> m_games;


};
