#pragma once
#include "LoggedUser.h"
#include "Request.h"
#include <algorithm>
#include <string>     // std::string, std::stoi

#define ACTIVE_ROOM 1
#define NOT_ACTIVE_ROOM 0 

struct roomData
{
	unsigned int id;
	std::string name;
	std::string maxPlayers;
	unsigned int timePerQuestion;
	unsigned int isActive;
	unsigned int questionsCount;
};
class Room
{
public:
	Room(LoggedUser user, std::string name, std::string  maxPlayers, unsigned int timeForQuestion, unsigned int id, unsigned int questionCount);
	Room(LoggedUser user, byte roomId);
	Room();
	~Room();
	bool addUser(std::string username); //Add user to room
	void removeUser(std::string username); // Remove user from room
	std::vector<LoggedUser> getAllUsers();
	byte getRoomState();
	roomData getRoomMetaData() const;
	void startGame();
private:
	roomData _m_metadata;
	std::vector<LoggedUser> _m_users;
};
