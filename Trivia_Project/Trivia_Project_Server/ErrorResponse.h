#pragma once
#include <iostream>

class ErrorResponse
{
public:
	ErrorResponse(std::string message);
	ErrorResponse();
	~ErrorResponse();

	std::string _message;
};

