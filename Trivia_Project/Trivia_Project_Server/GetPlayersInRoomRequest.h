#pragma once
#include <iostream>

class GetPlayersInRoomRequest
{
public:
	GetPlayersInRoomRequest(unsigned int roomId);
	GetPlayersInRoomRequest();
	~GetPlayersInRoomRequest();

	unsigned int roomId;
};