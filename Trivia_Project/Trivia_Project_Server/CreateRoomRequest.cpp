#include "CreateRoomRequest.h"

CreateRoomRequest::CreateRoomRequest(std::string roomName, unsigned int maxUsers, unsigned int questionCount, unsigned int answerTimeout) //C'tor
{
	this->roomName = roomName;
	this->maxUsers = maxUsers;
	this->questionCount = questionCount;
	this->answerTimeout = answerTimeout;
}

CreateRoomRequest::CreateRoomRequest()
{

}
CreateRoomRequest::~CreateRoomRequest()
{

}
