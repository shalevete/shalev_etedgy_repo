#pragma once
#include <iostream>
#include "LoginResponse.h"

class CreateRoomResponse
{
public:
	CreateRoomResponse(unsigned int status);
	CreateRoomResponse();
	~CreateRoomResponse();

	unsigned int _status;
};

