
#pragma once
#include "GameManager.h"
#include "RequestHandlerFactory.h"
#include "roomManager.h"
#include "IDatabase.h"
#include "RequestResult.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include <algorithm> 


class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(Game*, Room *, roomManager* roomM,LoggedUser*, GameManager*, RequestHandlerFactory* , IDatabase*);
	~GameRequestHandler();

	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);

private:
	void addToHighscores(std::vector<PlayerResults>);
	Game* m_game;
	LoggedUser* m_user;
	GameManager* m_gameManager;
	RequestHandlerFactory* m_handlerFacroty;
	roomManager * m_room;
	Room * _room;
	IDatabase* _m_db;
	RequestResult getQuestion(Request);
	RequestResult submitAnswer(Request);
	RequestResult getGameResults(Request);
	void leaveGame(std::vector<PlayerResults> pls);

};
