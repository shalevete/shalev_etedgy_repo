import  requests
import  json
from pymongo import MongoClient
import base64

client = MongoClient("mongodb+srv://admin:afeckshalev123@cluster0-khsv2.mongodb.net/test?retryWrites=true&w=majority")

def addQuestionToDb(questions):
 global client
 mydb = client["triviaDB"]
 mycol = mydb["Questions"]
 mycol.insert_many(questions)



def getQuestions():
 question = {}
 questionList = []
 url = 'https://opentdb.com/api.php?amount=50&type=multiple&encode=&encode=base64'
 resp = requests.get(url=url)
 jsonDict = resp.json()["results"]
 for dict in jsonDict:
  question["question"] = base64.b64decode(dict["question"]).decode("utf-8")
  question["rightAns"] = base64.b64decode(dict["correct_answer"]).decode("utf-8")
  question["wrongAns"] = decodeList(dict["incorrect_answers"])
  questionList.append(question.copy())
 return  questionList

def decodeList(list):
 newList = []
 for item in list:
  newList.append(str(base64.b64decode(item).decode("utf-8")))
 return  newList
def main():
    questions = getQuestions()
    addQuestionToDb(questions)
    print("Done")



if __name__ == '__main__':
    main()
