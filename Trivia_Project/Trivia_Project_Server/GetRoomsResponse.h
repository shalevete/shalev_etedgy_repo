#pragma once
#include <iostream>
#include <vector>
#include "LoginResponse.h"
#include "Room.h"

class GetRoomsResponse
{
public:
	GetRoomsResponse(unsigned int status,std::vector<Room*> ); 
	GetRoomsResponse();
	~GetRoomsResponse();

	unsigned int _status;
	std::vector<Room*> _rooms;
};

