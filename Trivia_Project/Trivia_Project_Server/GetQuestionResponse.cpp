#include "GetQuestionResponse.h"



GetQuestionResponse::GetQuestionResponse()
{
}

GetQuestionResponse::GetQuestionResponse(unsigned int status, int rightAnsIndex,std::string question, std::map<unsigned int, std::string> answers)
{
	this->status = status;
	this->question = question;
	this->answers = answers;
	this->rightAnsIndex = rightAnsIndex;
}

GetQuestionResponse::~GetQuestionResponse()
{
}
