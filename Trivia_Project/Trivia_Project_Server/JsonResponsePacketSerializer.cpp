#include "JsonResponsePacketSerializer.h"
#include <iostream>
#include <string> 
#include <string.h>
/*
Create error response serialized
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(ErrorResponse err)
{
	
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(err._message.c_str());
	writer.EndObject();
	
	return finaleSerialize(response.GetString(), P_ERROR);

}
/*
Create login response serialized
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(LoginResponse loginRes)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(loginRes._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();
	
	return finaleSerialize(response.GetString(), LOGIN);
}
/*
Create signup response serialized
*/
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(SignUpResponse signupRes)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(signupRes._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();

	return finaleSerialize(response.GetString(),SIGN_UP);
}

std::vector<byte> JsonResponsePacketSerializer::finaleSerialize(const char* data, int idCode) 
{
	std::vector<byte> res;
	res.push_back('0' + idCode);
	int dataLength = strlen(data); //data length
	std::stringstream ss; 
	ss << std::setw(4) << std::setfill('0') << dataLength; //Transfer 4 byte data length to str
	std::string s = ss.str(); //Create string 
	for (int i = 1; i < 5; i++) //Data length to data msg
	{
		res.push_back(s[i - 1]);
	}
	std::cout << data << std::endl;
	
	res.insert(res.end(), (byte*)data, (byte*)data + strlen(data)+1);
	std::cout << res.data() << std::endl;
	return res; //Return the data
}
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(LogoutResponse res) 
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(res._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();

	return finaleSerialize(response.GetString(), LOG_OUT);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	rapidjson::Document roomDoc;

	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.StartArray();
	for (auto room : res._rooms)
	{
		writer.StartObject();
		writer.Key("Id");
		writer.Int(room->getRoomMetaData().id);
		writer.Key("maxPlayers");
		writer.String(room->getRoomMetaData().maxPlayers.c_str());
		writer.Key("isActive");
		writer.Bool(room->getRoomMetaData().isActive);
		writer.Key("name");
		writer.String(room->getRoomMetaData().name.c_str());
		writer.Key("questionsCount");
		writer.Int(room->getRoomMetaData().questionsCount);
		writer.Key("timePerQuestion");
		writer.Int(room->getRoomMetaData().timePerQuestion);
		writer.EndObject();

	}
	writer.EndArray();
	writer.EndObject();
	return finaleSerialize(response.GetString(), GET_ROOMS);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse res)
{
	std::string usernameList = "";
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.StartArray();
	for (auto username : res.users) 
	{
		writer.StartObject();
		writer.Key("username");
		writer.String(username.c_str());
		writer.EndObject();
	}
	writer.EndArray();
	writer.EndObject();

	return finaleSerialize(response.GetString(), GET_PLAYERS_IN_ROOM);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(HighscoreResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.StartArray();
	for (auto pair : res._highscores)
	{
		writer.StartObject();
		writer.Key("username");
		writer.String(pair.first.getUsername().c_str());
		writer.Key("rightAns");
		writer.Int(pair.second);
		writer.EndObject();
	}
	writer.EndArray();
	writer.EndObject();
	return finaleSerialize(response.GetString(), HIGH_SCORE_RES);

}
std::vector<byte> JsonResponsePacketSerializer::serializeResponse(resetScoresResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(res._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();
	return finaleSerialize(response.GetString(), LEAVE_ROOM_RES);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(res._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();
	return finaleSerialize(response.GetString(), CLOSE_ROOM_RES);
	
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(StartGameResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(res._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();
	return finaleSerialize(response.GetString(), START_GAME_RES);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(res._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.StartObject();
	writer.Key("status");
	writer.String(resData.c_str());
	writer.Key("answerTimeOut");
	writer.Int(res._answerTimeout);
	writer.Key("questionCount");
	writer.Int(res._questionCount);
	writer.Key("hasGameBegun");
	writer.Bool(res._hasGameBegun);
	writer.Key("Players");
	writer.StartArray();
	for(auto player : res._players)
	{
		writer.String(player.c_str());
	}
	writer.EndArray();
	writer.EndObject();
	writer.EndObject();
	return finaleSerialize(response.GetString(), GET_ROOM_STATE);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(res._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();
	return finaleSerialize(response.GetString(), LEAVE_ROOM_RES);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.StartObject();
	writer.Key("question");
	writer.String(res.question.c_str());
	writer.Key("rightIndex");
	writer.Int(res.rightAnsIndex);
	writer.Key("answers");
	writer.StartArray();
	for(auto ans : res.answers)
	{
		writer.String(ans.second.c_str());
	}

	writer.EndArray();
	writer.EndObject();
	writer.EndObject();
	return finaleSerialize(response.GetString(), Get_Question_Response);

}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.StartObject();
	writer.Key("correctAnswerId");
	writer.Int(res.correctAnswerId);
	writer.EndObject();
	writer.EndObject();
	return finaleSerialize(response.GetString(), Submit_Answer_Response);

}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	writer.StartObject();
	writer.Key("gameData");
	writer.StartArray();
	for(auto playerRes : res.results)
	{
		writer.StartObject();
		writer.Key("username");
		writer.String(playerRes.username.c_str());
		writer.Key("averageAnswerTime");
		writer.Int(playerRes.averageAnswerTime);
		writer.Key("correctAnswerCount");
		writer.Int(playerRes.correctAnswerCount);
		writer.Key("wrongAnswerCount");
		writer.Int(playerRes.wrongAnswerCount);
		writer.EndObject();

	}
	writer.EndArray();
	writer.EndObject();
	return finaleSerialize(response.GetString(), Get_Game_Results_Response);


}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(statusResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.StartArray();
	for(int score : res.scores)
	{
		writer.Int(score);
	}
	writer.EndArray();
	writer.EndObject();
	return finaleSerialize(response.GetString(), MY_STATUS);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(res._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();

	return finaleSerialize(response.GetString(), JOIN_ROOM);
}

std::vector<byte> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse res)
{
	rapidjson::StringBuffer response; //String buffer
	rapidjson::Writer<rapidjson::StringBuffer> writer(response); //Json obj
	std::string resData = std::to_string(res._status);
	writer.StartObject(); //Start write message
	writer.Key("Data");
	writer.String(resData.c_str());
	writer.EndObject();
	
	return finaleSerialize(response.GetString(), CREATE_ROOM);
}

