#include "PlayerResults.h"



PlayerResults::PlayerResults(std::string username, unsigned int correctAnswerCount, unsigned int wrongAnswerCount, unsigned int averageAnswerTime)
{
	this->username = username;
	this->correctAnswerCount = correctAnswerCount;
	this->averageAnswerTime = averageAnswerTime;
	this->wrongAnswerCount = wrongAnswerCount;
}


PlayerResults::~PlayerResults()
{
}
