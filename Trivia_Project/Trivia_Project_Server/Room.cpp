#include "Room.h"



Room::Room()
{
}


Room::Room(LoggedUser user, std::string name, std::string maxPlayers, unsigned int timeForQuestion, unsigned int id, unsigned int questionCount)
{
	this->_m_metadata.id = id;
	this->_m_metadata.name = name;
	this->_m_metadata.timePerQuestion = timeForQuestion;
	this->_m_metadata.maxPlayers = maxPlayers;
	this->_m_metadata.isActive = ACTIVE_ROOM;
	this->addUser(user.getUsername());
	this->_m_metadata.questionsCount = questionCount;
}
Room::Room(LoggedUser user, byte roomId)
{
	this->_m_metadata.id = roomId;
	this->addUser(user.getUsername());
	this->_m_metadata.isActive = ACTIVE_ROOM;
}

Room::~Room()
{

}
void Room::startGame()
{
	this->_m_metadata.isActive = 0;
}
//Add user to room 
bool Room::addUser(std::string username)
{
	bool added = false;
	if (std::stoi(this->_m_metadata.maxPlayers) > this->_m_users.size())
	{
		this->_m_users.push_back(LoggedUser(username)); //Adding user
		added = true;
	}
	return added;
}
/*
Remove user from room
*/
void Room::removeUser(std::string username)
{
	LoggedUser userR(username);
	auto toErease = std::find(this->_m_users.begin(), this->_m_users.end(), userR);
	if (toErease != this->_m_users.end())
	{
		this->_m_users.erase(toErease);
	}
}
//Return all the users in room
std::vector<LoggedUser> Room::getAllUsers()
{
	return this->_m_users;
}
byte Room::getRoomState()
{
	return this->_m_metadata.isActive;
}
roomData Room::getRoomMetaData() const
{
	return this->_m_metadata;
}
