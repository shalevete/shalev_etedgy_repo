#include "HighscoreTable.h"



HighscoreTable::HighscoreTable(IDatabase * db)
{
	this->_db = db;
}


HighscoreTable::~HighscoreTable()
{
}
/*
Get highscores map from db
*/
std::map<LoggedUser, int> HighscoreTable::getHighScores()
{
	return this->_db->getHighScores();
}

std::vector<int> HighscoreTable::getStatus(LoggedUser user) 
{
	return this->_db->getStatusForUser(user.getUsername());
}

bool HighscoreTable::resetScores()
{
	return this->_db->resetScoresInDB();
}