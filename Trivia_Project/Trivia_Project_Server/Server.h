#pragma once
#include "IDatabase.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#include <iostream>
#include <vector>


class Server
{
public:
	Server();
	~Server();
	void run();

private:
	Communicator _communicator;

};

