#include "Communicator.h"
#include "RequestResult.h"

Communicator::Communicator()
{
	this->_wsaInit = WSAInitializer(); //Wsa init
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
   // if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	this->_serverRun = true;
	this->_m_loggedUsers = new std::map<SOCKET, LoggedUser>();
	this->m_handleFactory = new RequestHandlerFactory(this->_m_loggedUsers);
	//Check if started 
	if (this->_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(SERVE_ERR);
	}
}


Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		closesocket(_serverSocket);
	}
	catch (std::exception &e)
	{
		e.what();
	}
	catch (...)
	{
		std::cout << UNK_ERR;
	}
}
void Communicator::bindAndListen() 
{
	struct sockaddr_in socket_addr = { 0 };

	socket_addr.sin_port = htons(SERVER_PORT); // port that server will listen for
	socket_addr.sin_family = AF_INET;   // must be AF_INET
	socket_addr.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&socket_addr, sizeof(socket_addr)) == SOCKET_ERROR)
	{
		std::cout << WSAGetLastError() << std::endl;
		throw std::exception(BIND_ERR);
	}

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(LISTEN_ERR);
	}
	std::thread t(&Communicator::handleRequests, this); //Create client thread
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}

}
/*
Loop over client maps, and creat thread for each client request.
*/
void Communicator::handleRequests()
{
	
	tpool::ThreadPool *threadPool = new tpool::ThreadPool(THREAD_POOL_NUMBER); //Create thread pool
 	threadPool->start(); // Start
	while (this->_serverRun)
	{
		for (auto pair : this->m_clients)
		{
			
			if (!this->_handled[pair.first] && checkIfSocketHasData(pair.first)) //Check if client already handled
			{
				try 
				{
					threadPool->execute(&Communicator::handleRequest, this, pair.first); //Execute new thread for client
				}
				catch (std::exception ex)
				{
					disconnectClient(pair.first,ex.what());
				}
				
				changeClientHandledState(pair.first, true); //Client is now handled
			}
			
		}
		Sleep(SLEEP_TIME);
	}

	//Shutdown the pool
	threadPool->shutdownGracefully();  // Call threadPool->shutdown() to shutdown IMMEDIATELY
}
/*
Change user handled state
*/
void Communicator::changeClientHandledState(SOCKET clientSocket, bool state) 
{
	this->_mapHandled.lock();
	this->_handled[clientSocket] = state;
	this->_mapHandled.unlock();
}
/*
Disconnect client by socket and send error msg.
*/
void Communicator::disconnectClient(SOCKET clientSocket, std::string errorMsg) 
{
	//Erease client from maps and disconnect from server
	this->_clientMutex.lock();
	this->m_clients.erase(clientSocket);
	this->_m_loggedUsers->erase(clientSocket);
	this->_clientMutex.unlock();
	this->_mapHandled.lock();
	this->_handled.erase(clientSocket);
	this->_mapHandled.unlock();
	try 
	{
		Helper::sendData(clientSocket, errorMsg); //Error msg
	}
	catch (...)
	{
		std::cout << "Client already disconnect" << std::endl;
	}
	closesocket(clientSocket); //Close socket
}
/*
Check if socket has data
*/
bool Communicator::checkIfSocketHasData(SOCKET clientSocket)
{
	bool hasData = true;
	int rv = Helper::isClientTalked(clientSocket);
	if (rv == SOCKET_ERROR)
	{
		hasData = false;
		disconnectClient(clientSocket, UNK_ERR);
	}
	else if (rv == 0)
	{
		hasData = false;
	}
	return hasData;
}


Request Communicator::getRequest(SOCKET clientSocket) 
{
	int msgType = 0; //Msg type
	int reqLen = 0; //Request len
	try
	{
		msgType = Helper::getMessageTypeCode(clientSocket); //Get the msg
	}
	catch (...)
	{
		disconnectClient(clientSocket, UNK_ERR);
		std::cout << "Client: " << clientSocket << "disconnect" << std::endl;
		return Request(ERR_REQ_ID,0,(byte*)"dd");
	}

	reqLen = Helper::getIntPartFromSocket(clientSocket, 4);  //Get the length of request
	Request req(msgType, 0, (byte *)Helper::getStringPartFromSocket(clientSocket, reqLen).c_str()); //Get the request
	return req;
}

/*
handle user request 
*/
void Communicator::handleRequest(SOCKET clientSocket) 
{
	RequestResult res;
	Request req = getRequest(clientSocket);
	if (req.requestId == ERR_REQ_ID) //Error 
	{
		return;
	}
	else if (this->m_clients[clientSocket] == NULL) // Error 
	{
		disconnectClient(clientSocket,"Error, invaild handler");
	}
	else if (this->m_clients[clientSocket]->isRequestRelevant(req))  //Check if request relevent
	{
		this->_mongoMutex.lock(); //Handle the request
		try 
		{
			res = this->m_clients[clientSocket]->handleRequest(req);
		}
		catch (TriviaException ex) 
		{
			res.newHandler = this->m_clients[clientSocket];
			res.responseBuffer.insert(res.responseBuffer.end(), (byte*)ex.what(), (byte*)ex.what() + strlen(ex.what()) + 1);
		}
		this->_mongoMutex.unlock();
		this->m_clients[clientSocket] = res.newHandler; //Change request handle
		Helper::sendData(clientSocket, std::string((char*)res.responseBuffer.data()).c_str()); //Send data
	}
	else
	{
		
			Helper::sendData(clientSocket, UNK_ERR); //Send data
	
		
		
	}
	changeClientHandledState(clientSocket, false); //Change client state
}

void Communicator::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client

	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}
	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handle the conversation with the clients
	//Insert client socket to maps
	this->_clientMutex.lock();
	this->m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, this->m_handleFactory->createLoginRequestHandler(client_socket)));
	this->_clientMutex.unlock();
	this->_mapHandled.lock();
	this->_handled.insert(std::pair<SOCKET, bool>(client_socket, false));
	this->_mapHandled.unlock();
}