#pragma once
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include "ErrorResponse.h"
#include "LoginResponse.h"
#include "SignupResponse.h"
#include "CreateRoomResponse.h"
#include "GetPlayersInRoomResponse.h"
#include "GetRoomsResponse.h"
#include "JoinRoomResponse.h"
#include "LogoutResponse.h"
#include "GetRoomsResponse.h"
#include "HighscoreResponse.h"
#include "resetScoresResponse.h"
#include "CloseRoomResponse.h"
#include "StartGameResponse.h"
#include "GetRoomStateResponse.h"
#include "LeaveRoomResponse.h"
#include "SubmitAnswerResponse.h"
#include "statusResponse.h"
#include "GetQuestionResponse.h"
#include "GetGameResultsResponse.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/document.h"

#define HEADER_MSG_LEN 9
typedef unsigned char byte;
enum packetStateCode
{
	P_ERROR = 0,
	LOGIN = 1,
	SIGN_UP = 2,
	LOG_OUT = 3,
	GET_ROOMS = 4,
	GET_PLAYERS_IN_ROOM = 5,
	JOIN_ROOM = 6,
	CREATE_ROOM = 7,
	HIGH_SCORE_RES = 8,
	CLOSE_ROOM_RES = 9,
	START_GAME_RES = 10,
	GET_ROOM_STATE = 11,
	LEAVE_ROOM_RES = 12,
	Get_Question_Response = 13,
	Submit_Answer_Response = 14,
	Get_Game_Results_Response = 15,
	MY_STATUS = 16,
	Reset_Scores = 17
};


class JsonResponsePacketSerializer
{
public:
	static std::vector<byte> serializeResponse(ErrorResponse err);
	static std::vector<byte> serializeResponse(LoginResponse loginRes);
	static std::vector<byte> serializeResponse(SignUpResponse signupRes);
	static std::vector<byte> serializeResponse(LogoutResponse);

	static std::vector<byte> serializeResponse(GetRoomsResponse);

	static std::vector<byte> serializeResponse(GetPlayersInRoomResponse);
		

	static std::vector<byte> serializeResponse(JoinRoomResponse);

	static std::vector<byte>	serializeResponse(CreateRoomResponse);

	static std::vector<byte>	serializeResponse(HighscoreResponse);
	static std::vector<byte>	serializeResponse(CloseRoomResponse  );
	static std::vector<byte>	serializeResponse(StartGameResponse);
	static std::vector<byte>	serializeResponse(GetRoomStateResponse);
	static std::vector<byte>	serializeResponse(LeaveRoomResponse);
	static std::vector<byte>	serializeResponse(GetQuestionResponse);
	static std::vector<byte>	serializeResponse(SubmitAnswerResponse);
	static std::vector<byte>	serializeResponse(GetGameResultsResponse);
	static std::vector<byte>	serializeResponse(statusResponse);
	static std::vector<byte>	serializeResponse(resetScoresResponse);

	static std::vector<byte> finaleSerialize(const char*, int idCode) ;
};

