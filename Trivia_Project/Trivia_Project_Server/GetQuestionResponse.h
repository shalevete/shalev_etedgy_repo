#pragma once
#include <iostream>
#include <map>

class GetQuestionResponse
{
public:

	GetQuestionResponse();
	GetQuestionResponse(unsigned int status, int rightAnsIndex,std::string question, std::map<unsigned int, std::string> answers);
	~GetQuestionResponse();
	unsigned int status;
	std::string question;
	int rightAnsIndex;
	std::map<unsigned int, std::string> answers;
};

