#pragma once
#include "Question.h"


class GameData
{
public:
	GameData(Question, unsigned int, unsigned int, unsigned int);
	GameData();
	~GameData();

	unsigned int correctAnswerCount;
	Question currentQuestion;
	unsigned int wrongAnswerCount;
	unsigned int averangeAnswerTime;

};
