#include "Game.h"

Game::Game(std::vector<Question> questions, std::map<LoggedUser, GameData> players)
{
	this->m_questions = questions;
	this->m_players = players;
}


Game::~Game()
{
}

Question Game::getQuestionForUser(LoggedUser user)
{
	return this->m_players[user].currentQuestion;
}

unsigned int Game::submitAnswer(LoggedUser user,bool answerTrue)
{
	std::string ans = getQuestionForUser(this->m_players.begin()->first).getCorrectAns();
	int currQ = 0;
	for (int i = 0; i < this->m_questions.size(); i++)
	{
		if (ans == this->m_questions[i].getCorrectAns())
		{
			currQ = i + 1;
		}
	}
	if (currQ != this->m_questions.size())
	{
		this->m_players[user].currentQuestion = this->m_questions[currQ];

	}
	
	if (answerTrue) 
	{
		this->m_players[user].correctAnswerCount++;
	}
	else 
	{
		this->m_players[user].wrongAnswerCount++;
	}
	return 0;
}

//void Game::removePlayer();
std::map<LoggedUser, GameData> Game::getUsersGameStats() 
{
	return this->m_players;
}
