#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler( roomManager* roomManager, HighscoreTable* highscoreTable, RequestHandlerFactory* handlerFacroty, LoggedUser* m_user)
{
	this->m_roomManager = roomManager;
	this->m_highscoreTable = highscoreTable;
	this->m_handlerFacroty = handlerFacroty;
	this->m_user = m_user;
}

MenuRequestHandler::~MenuRequestHandler()
{
}

bool MenuRequestHandler::isRequestRelevant(Request req)
{
	switch (req.requestId)
	{
	case P_ERROR:
		
		return false;
		break;
	case MY_STATUS:
		return true;
		break;
	case HIGH_SCORE_RES:
		return true;
		break;
	case LOG_OUT:
		return true;
		break;
	case GET_ROOMS:
		return true;
		break;
	case GET_PLAYERS_IN_ROOM:
		return true;
		break;
	case JOIN_ROOM:
		return true;
		break;
	case CREATE_ROOM:
		return true;
		break;
	case Reset_Scores:
		return true;
	default:
		return false;
		break;
	}
}

RequestResult MenuRequestHandler::handleRequest(Request req)
{
	switch (req.requestId)
	{
	case P_ERROR:
		throw  TriviaException("Handle request error");

		break;
	case HIGH_SCORE_RES:
		return getHighscores(req);
		break;
	case MY_STATUS:
		return getStatus(req);
		break;
	case LOG_OUT:
		return signout(req);
		break;
	case GET_ROOMS:
		return getRooms(req);
		break;
	case GET_PLAYERS_IN_ROOM:
		return getPlayersInRoom(req);
		break;
	case JOIN_ROOM:
		return joinRoom(req);
		break;
	case CREATE_ROOM:
		return createRoom(req);
		break;
	case Reset_Scores:
		return resetScores(req);
		break;
	default:
		throw  TriviaException("Handle request error");

		break;
	}
}

RequestResult MenuRequestHandler::getStatus(Request)
{
	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	buffRes = resSerialize.serializeResponse(statusResponse(this->m_highscoreTable->getStatus(*this->m_user)));
	RequestResult res(buffRes, this);

	return res;
}
RequestResult MenuRequestHandler::signout(Request req)
{

	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	buffRes = resSerialize.serializeResponse(LogoutResponse(1));
	RequestResult requestResSignout(buffRes, this);

	return requestResSignout;
}

RequestResult MenuRequestHandler::getRooms(Request req)
{
	std::vector<byte> buffRes;
	std::vector<Room*> rooms = this->m_roomManager->getRooms();
	JsonResponsePacketSerializer resSerialize;
	buffRes = resSerialize.serializeResponse(GetRoomsResponse(1, rooms));
	RequestResult requestResSignout(buffRes, this);
	return requestResSignout;
}

RequestResult MenuRequestHandler::getPlayersInRoom(Request req)
{
	char * buff = { 0 };
	buff = reinterpret_cast<char*>(&req.buffer[0]);
	std::cout << buff << std::endl;
	JsonRequestPacketDeserializer Deserialize;
	GetPlayersInRoomRequest getPlayersInRoomReq = Deserialize.deserializeGetPlayersRequest((byte*)buff);

	std::vector<std::string> users;
	std::vector<Room*> rooms = this->m_roomManager->getRooms();
	int i = 0;
	int j = 0;
	bool found = false;
	for (i = 0; (i < rooms.size()) && (!false); i++)
	{
		if (rooms[i]->getRoomMetaData().id == getPlayersInRoomReq.roomId)
		{
			found = true;
			for (i = 0; j < rooms.size(); i++)
			{
				users[i] = rooms[i]->getAllUsers()[j].getUsername();
			}
		}
	}

	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	GetPlayersInRoomResponse GetPlayersInRoomRes(users);
	buffRes = resSerialize.serializeResponse(GetPlayersInRoomRes);
	RequestResult requestResSignout(buffRes, this);
	return requestResSignout;
}

RequestResult MenuRequestHandler::getHighscores(Request req)
{
	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	buffRes = resSerialize.serializeResponse(HighscoreResponse(1, this->m_highscoreTable->getHighScores()));
	RequestResult requestResHighscore(buffRes, this);
	return requestResHighscore;
}

RequestResult MenuRequestHandler::joinRoom(Request req)
{
	char * buff = { 0 };
	buff = reinterpret_cast<char*>(&req.buffer[0]);
	std::vector<byte> buffRes;
	JoinRoomRequest joinRoomReq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest((byte*)buff);
	IRequestHandler * handler = this;
	std::vector<Room*> rooms = this->m_roomManager->getRooms();
	int i = 0;
	Room* room = nullptr;
	for (i = 0; (i < rooms.size()) && (!false); i++)
	{
		if (rooms[i]->getRoomMetaData().id == joinRoomReq.roomId)
		{
			if (rooms[i]->addUser(this->m_user->getUsername())) 
			{
				room = rooms[i];
			}
		}
	}

	if (room != nullptr )
	{
		buffRes = JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse(1));
		handler = this->m_handlerFacroty->createRoomMemberRequestHandler(this->m_user, room);
		
	}
	else
	{
		buffRes = JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse(0));
	}
	RequestResult requestResJoinRoom(buffRes, handler);
	return requestResJoinRoom;
}

RequestResult MenuRequestHandler::createRoom(Request req)
{
	char * buff = { 0 };
	buff = reinterpret_cast<char*>(&req.buffer[0]);
	std::cout << buff << std::endl;
	JsonRequestPacketDeserializer Deserialize;
	CreateRoomRequest CreateRoomReq = Deserialize.deserializeCreateRoomRequest((byte*)buff);

	std::vector<Room*> rooms = this->m_roomManager->getRooms();
	int i = 0;
	bool found = false;
	for (i = 0; (i < rooms.size()) && (!false); i++)
	{
		if (rooms[i]->getRoomMetaData().name == CreateRoomReq.roomName)
		{
			found = true;
		}
	}


	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	IRequestHandler * handler;
	if (found)
	{
		buffRes = resSerialize.serializeResponse(CreateRoomResponse(0));
		handler = this;
	}
	else
	{
		std::cout << this->m_user->getUsername();
		buffRes = resSerialize.serializeResponse(CreateRoomResponse(1));
		Room* room  = (Room*)this->m_roomManager->createRoom(LoggedUser(*this->m_user), CreateRoomReq.roomName, std::to_string(CreateRoomReq.maxUsers), CreateRoomReq.answerTimeout, CreateRoomReq.questionCount);
		handler = this->m_handlerFacroty->createRoomAdminRequestHandler(this->m_user, room);
	}
	RequestResult requestCreateRoom(buffRes, handler);
	return requestCreateRoom;

}

RequestResult MenuRequestHandler::resetScores(Request req)
{
	std::vector<byte> buffRes;
	JsonResponsePacketSerializer resSerialize;
	buffRes = resSerialize.serializeResponse(resetScoresResponse(1, this->m_highscoreTable->resetScores()));
	RequestResult requestResResetScoresResponse(buffRes, this);
	return requestResResetScoresResponse;
}