#pragma once
#include "Defines.h"


class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler( roomManager* roomManager, HighscoreTable* highscoreTable, RequestHandlerFactory* handlerFacroty, LoggedUser* m_user);
	~MenuRequestHandler();

	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);


private:
	RequestResult signout(Request);
	RequestResult getRooms(Request);
	RequestResult getPlayersInRoom(Request);
	RequestResult getHighscores(Request);
	RequestResult joinRoom(Request);
	RequestResult createRoom(Request);
	RequestResult getStatus(Request);
	RequestResult resetScores(Request);
	LoggedUser* m_user;
	roomManager* m_roomManager;
	HighscoreTable* m_highscoreTable;
	RequestHandlerFactory* m_handlerFacroty;

};
