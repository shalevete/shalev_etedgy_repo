#pragma once
#include "Request.h"

#define STATUS_POS 1

class RequestResult;
class IRequestHandler
{
public:
	virtual bool isRequestRelevant(Request) = 0; 
	virtual RequestResult handleRequest(Request) = 0;
 
};

