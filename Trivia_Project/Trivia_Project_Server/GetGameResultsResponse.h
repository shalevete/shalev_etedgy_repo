#pragma once
#include <vector>
#include "PlayerResults.h"
class GetGameResultsResponse
{
public:
	GetGameResultsResponse();
	GetGameResultsResponse(unsigned int status, std::vector<PlayerResults> results);
	~GetGameResultsResponse();
	unsigned int status;
	std::vector<PlayerResults> results;
};

