#include "roomManager.h"



roomManager::roomManager()
{
}


roomManager::~roomManager()
{
}
/*
Creating room by LoggedUser and add room into map
*/
Room * roomManager::createRoom(LoggedUser user, std::string name, std::string maxPlayers, unsigned int timeForQuestion, unsigned int questionCount)
{
	int roomId = 0; //Room id
	for (std::map<byte, Room*>::iterator it = this->_m_rooms.begin(); it != this->_m_rooms.end(); ++it)  //Find vacant room id
	{
		roomId++;
	}
	Room * newRoom = new Room(user, name, maxPlayers, timeForQuestion, roomId, questionCount);
	this->_m_rooms.insert(std::pair<byte, Room*>(roomId, newRoom)); //Add room to map
	return newRoom;
}
/*
Delete room by id
*/
void roomManager::deleteRoom(int roomId)
{
	this->_m_rooms.erase(roomId);

}
/*
Get room state by id
*/
byte roomManager::getRoomState(int roomId)
{
	return this->_m_rooms[roomId]->getRoomState();
}
/*
Get rooms in vec
*/
std::vector<Room*> roomManager::getRooms()
{
	std::vector<Room *> roomVec;
	for (const auto &pair : this->_m_rooms)  //Add rooms to vector rooms
	{
		roomVec.push_back(pair.second);
	}
	return roomVec;

}