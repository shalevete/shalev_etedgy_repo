#include "GetRoomStateResponse.h"



GetRoomStateResponse::GetRoomStateResponse()
{
}

GetRoomStateResponse::GetRoomStateResponse(unsigned int status, bool gameState, std::vector<std::string> players, unsigned int questionCount, unsigned int answerTimeout)
{
	this->_hasGameBegun = gameState;
	this->_players = players;
	this->_questionCount = questionCount;
	this->_answerTimeout = answerTimeout;
	this->_status = status;
}


GetRoomStateResponse::~GetRoomStateResponse()
{
}
