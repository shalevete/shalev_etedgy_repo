#include "JsonRequestPacketDeserializer.h"


std::string requestToParse(byte* req) 
{
	std::string edit((char*)req);
	edit = edit.substr(0, edit.find('}') + 1);
	return edit;
}

/*
Deserializer request by json
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(byte * request)
{
	LoginRequest req;
	rapidjson::Document login;
	
	login.Parse(requestToParse(request).c_str()); //Deserializer
	req.username = login["username"].GetString();
	req.password = login["password"].GetString();
	return req;
}

/*
Deserializer request by json
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(byte* request)
{
	SignupRequest req;
	rapidjson::Document login;
	login.Parse(requestToParse(request).c_str()); //Deserializer
	req.username = login["username"].GetString(); 
	req.password = login["password"].GetString();
	req.email = login["email"].GetString();
	return req;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(byte* request)
{
	CreateRoomRequest req;
	rapidjson::Document json;
	json.Parse(requestToParse(request).c_str());
	req.answerTimeout = json["answerTimeout"].GetInt(); //Deserializer
	req.maxUsers = json["maxUsers"].GetInt();
	req.questionCount = json["questionCount"].GetInt();
	req.roomName = json["roomName"].GetString();
	return req;
}
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(byte* request)
{
	JoinRoomRequest req;
	rapidjson::Document json;
	json.Parse(requestToParse(request).c_str());
	req.roomId = json["roomId"].GetInt(); //Deserializer
	return req;
}
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(byte* request)
{
	GetPlayersInRoomRequest req;
	rapidjson::Document json;
	json.Parse(requestToParse(request).c_str());
	req.roomId = json["roomId"].GetInt(); //Deserializer
	return req;
}
 SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(byte* request)
{
	 SubmitAnswerRequest req;
	 rapidjson::Document json;
	 json.Parse(requestToParse(request).c_str());
	 req.answerTrue = json["answerTrue"].GetBool(); //Deserializer
	 return req;

}