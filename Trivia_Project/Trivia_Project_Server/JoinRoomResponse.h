#pragma once
#include <iostream>
#include "LoginResponse.h"

class JoinRoomResponse
{
public:
	JoinRoomResponse(unsigned int status);
	JoinRoomResponse();
	~JoinRoomResponse();

	unsigned int _status;
};

