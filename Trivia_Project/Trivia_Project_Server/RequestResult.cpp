#include "RequestResult.h"


//Ctor
RequestResult::RequestResult()
{
	//Init
	this->newHandler = NULL;
}

//Dtor
RequestResult::~RequestResult()
{
	this->newHandler = NULL;
}
//Ctor
RequestResult::RequestResult(std::vector<byte> res, IRequestHandler* newHandler)
{
	this->responseBuffer = res;
	this->newHandler = newHandler;
}
//Ctord
RequestResult::RequestResult(byte* res, IRequestHandler* newHandler)
{
	this->responseBuffer.insert(this->responseBuffer.end(), res, res + strlen((char*)res));
	this->newHandler = newHandler;
}