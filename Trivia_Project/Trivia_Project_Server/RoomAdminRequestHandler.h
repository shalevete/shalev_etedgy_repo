#pragma once
#include "Room.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "roomManager.h"
#include "RequestResult.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "TriviaException.h"

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(LoggedUser *user, roomManager * roomManagers, RequestHandlerFactory* handler, Room *room);
	~RoomAdminRequestHandler();
	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);
private:
	LoggedUser *_m_user;
	roomManager * _m_room;
	RequestHandlerFactory* _m_handler_factory;
	Room* _room;
	RequestResult closeRoom(Request);
	RequestResult startGame(Request);
	RequestResult getRoomState(Request);
};

