#include "Request.h"
#include <string.h>


Request::Request() //C'tor
{
	this->reciveTime = 0; //Init
}
Request::Request(int requestId, int rTime, byte * data) //C'tor
{
	this->requestId = requestId; //Init 
	this->reciveTime = rTime;
	this->buffer.insert(this->buffer.end(), data, data + strlen((char*)data));
}

Request::~Request() //D'tor
{ 
	this->buffer.clear(); //Clear
}
