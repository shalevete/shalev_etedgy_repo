#include "IDatabase.h"



IDatabase::IDatabase()
{
	this->_inst =  new mongocxx::instance{}; //Create instance
	mongocxx::uri uri("mongodb+srv://admin:afeckshalev123@cluster0-khsv2.mongodb.net/test?retryWrites=true"); //Connect to database
	this->_conn = mongocxx::client(uri);  //Connect to dv
	this->_db = this->_conn["triviaDB"]; //Connenct to db and collection
}


IDatabase::~IDatabase()
{
}

/*
param: username , password
Adds user to data base
*/
void IDatabase::addUserToDb(std::string username, std::string password,std::string email)
{
	this->_coll = this->_db[USERS_COLL];
	auto builder = bsoncxx::builder::stream::document{}; //Build doucment
	bsoncxx::document::value doc_user = builder //Create doucment data
		<< "username" << username.c_str()
		<< "password" << password.c_str() 
		<< "email" << email.c_str()
	<< bsoncxx::builder::stream::finalize;
	bsoncxx::stdx::optional<mongocxx::result::insert_one> result =
		this->_coll.insert_one(doc_user.view()); //Insert Doucment
	this->addUserToHighScoreCollection(username);
}
/*
Check if user exsits by username and password
*/
bool IDatabase::login(std::string username, std::string password)
{
	this->_coll = this->_db[USERS_COLL];
	

	bool exsits = false;
	auto builder = bsoncxx::builder::stream::document{}; //Build doucment
	bsoncxx::document::value doc_user = builder //Create doucment data
		<< "username" << username.c_str()
		<< "password" << password.c_str()
		<< bsoncxx::builder::stream::finalize;
 	bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result = //Query for user exsits
		this->_coll.find_one(doc_user.view());
	exsits = maybe_result ? true : false;  //Clause for bool
	return exsits;
}
/*
Check if user exsits by username
*/
bool IDatabase::doesUsernameExsits(std::string username)
{
	this->_coll = this->_db[USERS_COLL];
	bool exsits = false;
	auto builder = bsoncxx::builder::stream::document{}; //Build doucment
	bsoncxx::document::value doc_user = builder //Create doucment data
		<< "username" << username.c_str()
		<< bsoncxx::builder::stream::finalize;
	bsoncxx::stdx::optional<bsoncxx::document::value> maybe_result = //Query for user exsits
		this->_coll.find_one(doc_user.view());
	exsits = maybe_result ? true : false;  //Clause for bool
	return exsits;
}
/*
Add right answer rate to highscore collection
*/
void IDatabase::addHighscoreVal(int rightAnswer, int wrongAns , std::string username)
{
	this->_coll = this->_db[HIGH_SCORE_COLL];
	this->_coll.update_one(bsoncxx::builder::stream::document{} << "username" << username << bsoncxx::builder::stream::finalize,
		bsoncxx::builder::stream::document{} << "$inc" << bsoncxx::builder::stream::open_document <<
		RIGHT_ANS_INDEX << rightAnswer << WRONG_ANS << wrongAns  << GAME_PLAYED << 1 << bsoncxx::builder::stream::close_document << bsoncxx::builder::stream::finalize); //Add rightAns

}
/*
Add user to highscore collection
*/
void IDatabase::addUserToHighScoreCollection(std::string username)
{
	this->_coll = this->_db[HIGH_SCORE_COLL];
	auto builder = bsoncxx::builder::stream::document{}; //Build doucment
	bsoncxx::document::value doc_user = builder //Create doucment data
		<< "username" << username.c_str()
		<< RIGHT_ANS_INDEX << 0
		<< WRONG_ANS << 0
		<< GAME_PLAYED << 0
		<< bsoncxx::builder::stream::finalize;
	bsoncxx::stdx::optional<mongocxx::result::insert_one> result =
		this->_coll.insert_one(doc_user.view()); //Insert Doucment
}
/*
Get from db all the highscores of users and insert them into a map of users and highscores
return the map
*/
std::map<LoggedUser, int> IDatabase::getHighScores()
{
	std::map<LoggedUser, int> highScoresMap;
	this->_coll = this->_db[HIGH_SCORE_COLL];
	auto opts = mongocxx::options::find{};
	auto order = bsoncxx::builder::stream::document{} << RIGHT_ANS_INDEX << -1 << bsoncxx::builder::stream::finalize;
	opts.sort(order.view());
	opts.limit(4);
	mongocxx::cursor cursor = this->_coll.find({}, opts); //Get all the doucments
	for (auto doc : cursor) //Get doc from doucments arr
	{
			std::string username = bsoncxx::string::to_string(doc["username"].get_utf8().value); //Get username from doucment
			int rightAns = doc[RIGHT_ANS_INDEX].get_int32().value; //get right ans rate from doucment
			LoggedUser user(username);
			highScoresMap.insert(std::pair<LoggedUser, int>(user, rightAns)); //Insert to map
	}
	return highScoresMap;
}
std::vector<int> IDatabase::getStatusForUser(std::string username)
{
	this->_coll = this->_db[HIGH_SCORE_COLL];
	std::vector<int> scores;
	auto builder = bsoncxx::builder::stream::document{}; //Build doucment
	bsoncxx::document::value doc_user = builder //Create doucment data
		<< "username" << username.c_str()
		<< bsoncxx::builder::stream::finalize;
	auto res = this->_coll.find(doc_user.view()); //Get all the doucments
	for (auto doc : res) 
	{
		scores.push_back(doc[GAME_PLAYED].get_int32().value);
		scores.push_back(doc[RIGHT_ANS_INDEX].get_int32().value);
		scores.push_back(doc[WRONG_ANS].get_int32().value);
	}
	return scores;
}
std::vector<Question> IDatabase::getQuestions(int number)
{
	std::vector<Question> questionsVector;
	this->_coll = this->_db[QUESTIONS_COLL];
	mongocxx::pipeline p{}; // Query
	p.sample(number);
	mongocxx::cursor cursor = this->_coll.aggregate(p, mongocxx::options::aggregate{}); 
	for(auto doc : cursor) //Iterate over doucments
	{
		std::string question = bsoncxx::string::to_string(doc["question"].get_utf8().value); //Get question from doucment
		std::string rightAns = bsoncxx::string::to_string(doc["rightAns"].get_utf8().value); //Get rightAns from doucment
		std::vector<std::string> wrongAns;
		bsoncxx::array::view subarray{ doc["wrongAns"].get_array().value }; //Array of wrong ans
		for(auto ans : subarray)  //Iterate array
		{
			wrongAns.push_back(bsoncxx::string::to_string(ans.get_utf8().value)); //Get wrong ans
		}
		 
		questionsVector.push_back(Question(question, wrongAns, rightAns)); //Add to question vector
	}
	return questionsVector; 
}

bool IDatabase::resetScoresInDB()
{
	std::map<LoggedUser, int> highScoresMap;
	this->_coll = this->_db[HIGH_SCORE_COLL];
	auto opts = mongocxx::options::find{};
	auto order = bsoncxx::builder::stream::document{} << RIGHT_ANS_INDEX << -1 << bsoncxx::builder::stream::finalize;
	opts.sort(order.view());
	opts.limit(100);
	mongocxx::cursor cursor = this->_coll.find({}, opts); //Get all the doucments
	for (auto doc : cursor) //Get doc from doucments arr
	{
		std::string username = bsoncxx::string::to_string(doc["username"].get_utf8().value); //Get username from doucment
		int rightAns = doc[RIGHT_ANS_INDEX].get_int32().value; //get right ans rate from doucment
		LoggedUser user(username);
		this->_coll.update_one(bsoncxx::builder::stream::document{} << "username" << username << bsoncxx::builder::stream::finalize,
			bsoncxx::builder::stream::document{} << "$set" << bsoncxx::builder::stream::open_document <<
			RIGHT_ANS_INDEX << 0 << WRONG_ANS << 0 << GAME_PLAYED << 0 << bsoncxx::builder::stream::close_document << bsoncxx::builder::stream::finalize); //Add rightAns
	}
	std::cout << "hello world";
	this->_coll = this->_db[HIGH_SCORE_COLL];
	return true;

}