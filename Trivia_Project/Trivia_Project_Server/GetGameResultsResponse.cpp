#include "GetGameResultsResponse.h"



GetGameResultsResponse::GetGameResultsResponse()
{
}

GetGameResultsResponse::GetGameResultsResponse(unsigned int status, std::vector<PlayerResults> results)
{
	this->status = status;
	this->results = results;
}

GetGameResultsResponse::~GetGameResultsResponse()
{
}
