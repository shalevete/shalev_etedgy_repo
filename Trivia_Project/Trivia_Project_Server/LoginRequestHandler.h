#pragma once
#include "RequestHandlerFactory.h"
#include "LoginManager.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestResult.h"
#include "LoginRequest.h"
#include "SignupRequest.h"
#include "SignUpResponse.h"
#include "TriviaException.h"
#include "WSAInitializer.h"

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager * loginManager , RequestHandlerFactory* handlerManager, SOCKET user);
	~LoginRequestHandler();

	bool isRequestRelevant(Request);
	RequestResult handleRequest(Request);
	RequestResult login(Request);
	RequestResult signup(Request);

private:
	LoginManager* m_loginManager;
	SOCKET _user;
	RequestHandlerFactory* m_handlerFactory;

};
