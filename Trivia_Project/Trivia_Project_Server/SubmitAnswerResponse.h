#pragma once
class SubmitAnswerResponse
{
public:
	SubmitAnswerResponse();
	SubmitAnswerResponse(unsigned int status, unsigned int correctAnswerId);
	~SubmitAnswerResponse();
	unsigned int status;
	unsigned int correctAnswerId;

};

