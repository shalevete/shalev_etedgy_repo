#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "GameRequestHandler.h"

RequestHandlerFactory::RequestHandlerFactory(std::map<SOCKET, LoggedUser> *_m_loggedUsers)
{
	this->_m_db = new IDatabase();
	this->_m_loginManager = new LoginManager(this->_m_db,_m_loggedUsers);
	this->_m_roomManager = new roomManager();
	this->_m_highScore = new HighscoreTable(this->_m_db);
	this->_m_gameManager = new GameManager(this->_m_db);

}
RequestHandlerFactory::~RequestHandlerFactory()
{

}
IRequestHandler* RequestHandlerFactory::createLoginRequestHandler(SOCKET client)
{
	return new LoginRequestHandler(_m_loginManager, this, client);
}
IRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser * user)
{
	return  new MenuRequestHandler(this->_m_roomManager, this->_m_highScore,this,user);
}
IRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser * user, Room *room)
{
	return new RoomAdminRequestHandler(user, this->_m_roomManager, this, room);
}
IRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser * user, Room *room)
{
	return new RoomMemberRequestHandler(user, this->_m_roomManager, this, room);
}

IRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser * user, Room *room )
{
	if (this->_roomGameMap.find(room) == this->_roomGameMap.end())  //Create new Game
	{
		Game * game = this->_m_gameManager->createGame((room));
		this->_roomGameMap.insert(std::pair<Room*, Game*>(room, game));
		return new GameRequestHandler(game,room,this->_m_roomManager, user, this->_m_gameManager, this,this->_m_db);
	}
	else 
	{
		return new GameRequestHandler(this->_roomGameMap[room],room,this->_m_roomManager,user,this->_m_gameManager, this, this->_m_db);
	}
}