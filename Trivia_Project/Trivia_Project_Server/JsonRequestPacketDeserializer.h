#pragma once
#include <string>
#include <fstream>
#include "LoginRequest.h"
#include "SignupRequest.h"
#include "CreateRoomRequest.h"
#include "JoinRoomRequest.h"
#include "SubmitAnswerRequest.h"
#include "GetPlayersInRoomRequest.h"
#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"


typedef unsigned char byte;
class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(byte *);
	static SignupRequest deserializeSignupRequest(byte*);
	static CreateRoomRequest deserializeCreateRoomRequest(byte*);
	static JoinRoomRequest deserializeJoinRoomRequest(byte*);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(byte*);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(byte*);
};

