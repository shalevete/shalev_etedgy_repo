#pragma once
#include "IRequestHandler.h"


class RequestResult
{
public:
	RequestResult();
	RequestResult(std::vector<byte> res, IRequestHandler* newHandler); //Ctor
	RequestResult(byte* res, IRequestHandler* newHandler);
	~RequestResult();//Dtor

	//Uml set this vars public
	std::vector<byte> responseBuffer;
	IRequestHandler *newHandler;

	
};

