#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "LoginResponse.h"

class GetPlayersInRoomResponse
{
public:
	GetPlayersInRoomResponse(std::vector<std::string> );
	~GetPlayersInRoomResponse();

	std::vector<std::string> users;
};

