#pragma once
#include <cstdint>
#include <iostream>
#include <map>
#include <mongocxx/client.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/instance.hpp>
#include <bsoncxx/builder/core.hpp>
#include <bsoncxx/builder/stream/key_context.hpp>
#include <bsoncxx/builder/stream/single_context.hpp>
#include <bsoncxx/document/value.hpp>
#include <bsoncxx/document/view.hpp>
#include <bsoncxx/config/prelude.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/string/to_string.hpp>
#include <mongocxx/pipeline.hpp>

#include"LoggedUser.h"
#include "Question.h"
#define RIGHT_ANS_INDEX "rightAns"
#define HIGH_SCORE_COLL "HighScores"
#define USERS_COLL "Users"
#define WRONG_ANS "wrongAns"
#define GAME_PLAYED "gamePlayed"
#define QUESTIONS_COLL "Questions"
class IDatabase
{
public:
	IDatabase();
	~IDatabase();
	bool login(std::string username, std::string password);
	bool doesUsernameExsits(std::string username);

	void addUserToDb(std::string username, std::string password,std::string email);
	std::map<LoggedUser,int> getHighScores();
	void addHighscoreVal(int rightAnswer,int wrongAns,  std::string username);
	std::vector<Question> getQuestions(int numeber);
	std::vector<int> getStatusForUser(std::string username);
	bool resetScoresInDB();

private:
	mongocxx::database _db;
	mongocxx::instance * _inst;
	mongocxx::client _conn;
	mongocxx::collection _coll;
	void addUserToHighScoreCollection(std::string username);
};



