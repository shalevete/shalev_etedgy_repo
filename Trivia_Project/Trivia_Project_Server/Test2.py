import socket
import sys
import time
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('127.0.0.1', 70)
print('connecting to {} port {}'.format(*server_address))
sock.connect(server_address)
time.sleep(0)

   #  Send data
message = b'010040{"username":"afeck" , "password":"123"}'
print('sending {!r}'.format(message))
   
sock.sendall(message)

data = sock.recv(18)
print('received {!r}'.format(data))
message = b'040030{}'
sock.sendall(message)
data = sock.recv(40)
print('received {!r}'.format(data))


print('closing socket')
sock.close()

