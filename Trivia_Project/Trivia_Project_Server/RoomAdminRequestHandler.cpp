#include "RoomAdminRequestHandler.h"
#include "JsonResponsePacketSerializer.h"



RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser* user, roomManager * roomManagers, RequestHandlerFactory * handler, Room* room)
{
	this->_m_user = user;
	this->_m_room = roomManagers;
	this->_m_handler_factory = handler;
	this->_room = room;

}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
}

bool RoomAdminRequestHandler::isRequestRelevant(Request req)
{
	bool relevant = false;
	switch (packetStateCode(req.requestId))
	{
	case P_ERROR:

		relevant = false;
		break;
	case START_GAME_RES:
		relevant =  true;
		break;
	case CLOSE_ROOM_RES:
		relevant =  true;
		break;
	case GET_ROOM_STATE:
		relevant =  true;
		break;
	default:
		relevant = false;
		break;
	}
	return relevant;
}

RequestResult RoomAdminRequestHandler::handleRequest(Request req)
{	
	switch (packetStateCode(req.requestId))
	{
	case START_GAME_RES:
		 return startGame(req);
		break;
	case CLOSE_ROOM_RES:
		return closeRoom(req);
		break;
	case GET_ROOM_STATE:
		return getRoomState(req);
		break;
	default:
		throw  TriviaException("Handle request error");

		break;
	}
	
}

RequestResult RoomAdminRequestHandler::closeRoom(Request req)
{
	std::vector<byte> buffRes;
	this->_m_room->deleteRoom(this->_room->getRoomMetaData().id);
	delete(this->_room);
	buffRes = JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse(STATUS_POS));
	return RequestResult(buffRes, this->_m_handler_factory->createMenuRequestHandler(this->_m_user));
}

RequestResult RoomAdminRequestHandler::startGame(Request req)
{
	std::vector<byte> buffRes;
	this->_room->startGame();
	buffRes = JsonResponsePacketSerializer::serializeResponse(StartGameResponse(STATUS_POS));
	return RequestResult(buffRes, this->_m_handler_factory->createGameRequestHandler(this->_m_user, this->_room)); //Need create game
}

RequestResult RoomAdminRequestHandler::getRoomState(Request req)
{
	std::vector<byte> buffRes;
	std::vector<std::string> players;
	for(auto elem : this->_room->getAllUsers())
	{
		players.push_back(elem.getUsername());
	}
	GetRoomStateResponse res(STATUS_POS, this->_room->getRoomState() == STATUS_POS, players, this->_room->getRoomMetaData().questionsCount, this->_room->getRoomMetaData().timePerQuestion);
	buffRes = JsonResponsePacketSerializer::serializeResponse(res);
	return RequestResult(buffRes, this);
}
