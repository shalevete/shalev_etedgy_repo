#pragma once
#include "Question.h"
#include "GameData.h"
#include "LoggedUser.h"
#include <map>

#define QUESTION_END "Run out of question"
class Game
{
public:
	Game(std::vector<Question>, std::map<LoggedUser, GameData>);
	~Game();

	Question getQuestionForUser(LoggedUser);
	unsigned int submitAnswer(LoggedUser user,bool answerTrue);
	std::map<LoggedUser, GameData> getUsersGameStats();
//	void removePlayer();


private:
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;


};
