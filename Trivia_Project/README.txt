This project simulates a trivia game.
The project objective is to create a multiplayer Trivia game. In addition, more than one multiplayer game can be played at the same time by different groups of players.
The game also saves the data of each user, thus each user can track their own game history. The game also contains a scoreboard of the highest scores that players achieved.
The project is designed for anyone interested in playing "Trivia" and signed up to the game.
any user can choose to create a room and be an Admin, or to be a player and join a room.

Main features:
*New user registration
*User Login to the system
*Creating a Room
*Game Room management
*View Leader board
*View the user history information

The server of the ptoject was written in C++ and the Client in C#.
I made this project with my classmate in Magshimim as final project of the second year in Magshimim.