﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace TriviaClient
{
    public partial class CreateRoomWindow : Window
    {
       public CreateRoomWindow()
        {
            
            InitializeComponent();
        }
        private void btnCreateRoom_Click(object sender, RoutedEventArgs e)  //Click to create room 
        {
            string roomName = RoomNameTextBox.Text;
            int maxUsers = Convert.ToInt32(MaxUsersTextBox.Text);
            int qNum = Convert.ToInt32(QNumTextBox.Text);
            int ansTime = Convert.ToInt32(AnsTimeTextBox.Text);
            if(qNum < 5 || qNum > 49)
            {
                MessageBox.Show("Question number must be above 5 or 5");
            }
            else if(Global.clientSocket.CreateRoomRequest(ansTime, maxUsers, qNum, roomName))    //input good , open admin Room screen
            {
                AdminRoom wnd = new AdminRoom(ansTime, maxUsers, qNum, roomName);
                this.Close();
                wnd.Show();
            }
            else
            {
                MessageBox.Show("create room failed | Bad Input");
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)  //Click to go back to manu
        {
            MainWindow wnd = new MainWindow();
            this.Close();
            wnd.Show();
        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        private void MaxUsersTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
             if(!MaxUsersTextBox.Text.ToString().All(Char.IsDigit))
             {
                MaxUsersTextBox.Text = MaxUsersTextBox.Text.Substring(0, MaxUsersTextBox.Text.Length - 1);
             }
            
        }

        private void QNumTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(!QNumTextBox.Text.ToString().All(Char.IsDigit))
            {
                QNumTextBox.Text = QNumTextBox.Text.Substring(0, QNumTextBox.Text.Length - 1);
            }
        }

        private void AnsTimeTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!AnsTimeTextBox.Text.ToString().All(Char.IsDigit))
            {
                AnsTimeTextBox.Text = AnsTimeTextBox.Text.Substring(0, AnsTimeTextBox.Text.Length - 1);
            }
        }

       
    }
}
