﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Threading;

namespace TriviaClient
{

    public partial class StatusWindow : Window
    {
        public StatusWindow(List<int> Status)
        {
            InitializeComponent();
            gamesCount.Text = "number of games: " + Status[0].ToString();
            rightCount.Text = "number of right answers: " + Status[1].ToString();
            wrongCount.Text = "number of wrong answers: " + Status[2].ToString();
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow wnd = new MainWindow();
            this.Close();
            wnd.Show();
        }
    }
}

