﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Threading;
using System.Text.RegularExpressions;
namespace TriviaClient
{

  public enum Answer { ansTimeout = 0 ,ansFirst, ansSecond, ansThird, ansFourth };

    public partial class Question : Window
    {
        private Mutex _isAnsweredMutex;
        private Mutex _isCorrectMutex;
        private Thread _t;

        private Answer _correctAns;
        private int _time;
        private int _currQuestion;
        private int _numOfQuestions;

        private Boolean _isAnswered;
        private Boolean _isCorrect;

        public Question(string roomName, string question ,List<string> answers, int correctAns, int maxQuestions, int ansTime)
        {
            this._isAnsweredMutex = new Mutex();
            this._isCorrectMutex = new Mutex();
            InitializeComponent();

            this._numOfQuestions = maxQuestions;
           

            this._currQuestion = 1;
            newQuestion(StripHTML(question), answers, correctAns);

            this._time = ansTime;
            this._t = new Thread(timer);
            this._t.Start();

        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }

        private void changeButtons(bool enable, Brush brush)
        {
            firstAnsBtn.IsEnabled = enable;
            secondAnsBtn.IsEnabled = enable;
            thirdAnsBtn.IsEnabled = enable;
            fourthAnsBtn.IsEnabled = enable;
            firstAnsBtn.Foreground = brush;
            secondAnsBtn.Foreground = brush;
            thirdAnsBtn.Foreground = brush;
            fourthAnsBtn.Foreground = brush;
        }

        public delegate void UpdateScoresCallback();
        private void scoresWindow()
        {

            System.Threading.Thread.Sleep(2000);
            List<Dictionary<string, object>> scores = Global.clientSocket.getGameResults();
            gameResult wnd = new gameResult(scores);
            this.Close();
            wnd.Show();
            this._t.Abort();
        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }
        public delegate void UpdateQuestionCallback(string question,List<string> answers, int correctAns);
        private void newQuestion(string question, List<string> answers, int correctAns)
        {
            setIsCorrect(false);
            setIsAnswered(false); ;

            this._correctAns = (Answer)correctAns;
            questionText.Text = question;
            changeButtons(true, Brushes.White);
     

            firstAnsBtn.Content = answers[0];
            secondAnsBtn.Content = answers[1];
            thirdAnsBtn.Content = answers[2];
            fourthAnsBtn.Content = answers[3];
            
            qustionNumber.Text = this._currQuestion.ToString() + "/" + this._numOfQuestions.ToString();
        }
        public delegate void UpdateTimeCallback(string time);
        private void UpdateTime(string time)
        {
            Time.Text = time;

        }
        private void timer()
        {
            int j = 0;
            for (j = 0; j < this._numOfQuestions; j++)
            {
                int i = 0;
                int temp = this._time;
                Time.Dispatcher.Invoke(new UpdateTimeCallback(this.UpdateTime), new object[] { temp.ToString() });


                for (i = 0; i < this._time; i++)
                {
                    System.Threading.Thread.Sleep(1000);
                    temp--;
                    Time.Dispatcher.Invoke(new UpdateTimeCallback(this.UpdateTime), new object[] { temp.ToString() });
                }

                if (!getIsAnswered())
                {
                    this.Dispatcher.Invoke(new UpdateUserClickedCallback(this.UserClicked), new object[] { Answer.ansTimeout });
                }
           
                Global.clientSocket.submitAnswer(getIsCorrect());
                //get question from server
                if(j+1 != this._numOfQuestions)
                {
                    Dictionary<string, object> question = Global.clientSocket.getQuestion();
                    this._currQuestion++;
                    this.Dispatcher.Invoke(new UpdateQuestionCallback(this.newQuestion), new object[] { StripHTML(question["question"].ToString()), JsonConvert.DeserializeObject<List<string>>(question["answers"].ToString()), int.Parse(question["rightIndex"].ToString()) + 1 });

                }

            }
            this.Dispatcher.Invoke(new UpdateScoresCallback(this.scoresWindow));
        }

        private void firstAnsBtn_Click(object sender, RoutedEventArgs e)
        {
            Answer ChosenAnswer = Answer.ansFirst;
            UserClicked(ChosenAnswer);
            if (this._correctAns == ChosenAnswer)
            {
                setIsCorrect(true);
            }
            else
            {
                setIsCorrect(false);
            }
        }

        private void secondAnsBtn_Click(object sender, RoutedEventArgs e)
        {
            Answer ChosenAnswer = Answer.ansSecond;
            UserClicked(ChosenAnswer);
            if (this._correctAns == ChosenAnswer)
            {
                setIsCorrect(true);
            }
            else
            {
                setIsCorrect(false);
            }
        }

        private void thirdAnsBtn_Click(object sender, RoutedEventArgs e)
        {
            Answer ChosenAnswer = Answer.ansThird;
            UserClicked(ChosenAnswer);
            if (this._correctAns == ChosenAnswer)
            {
                setIsCorrect(true);
            }
            else
            {
                setIsCorrect(false);
            }
        }

        private void fourthAnsBtn_Click(object sender, RoutedEventArgs e)
        {
            Answer ChosenAnswer = Answer.ansFourth;
            UserClicked(ChosenAnswer);
            if (this._correctAns == ChosenAnswer)
            {
                setIsCorrect(true);
            }
            else
            {
                setIsCorrect(false);
            }
        }
        public delegate void UpdateUserClickedCallback(Answer ChosenAnswer);
        private void UserClicked(Answer ChosenAnswer)
        {
            setIsAnswered(true);

            changeButtons(false, Brushes.Gray);
          
            switch (ChosenAnswer)
            {
                case Answer.ansFirst:
                    firstAnsBtn.Foreground = Brushes.Blue;
                    break;
                case Answer.ansSecond:
                    secondAnsBtn.Foreground = Brushes.Blue;
                    break;
                case Answer.ansThird:
                    thirdAnsBtn.Foreground = Brushes.Blue;
                    break;
                case Answer.ansFourth:
                    fourthAnsBtn.Foreground = Brushes.Blue;
                    break;
                default:
                    //ERROR
                    break;
            }
            System.Threading.Thread.Sleep(1000);
            switch (this._correctAns)
            {
                case Answer.ansFirst:
                    firstAnsBtn.Foreground = Brushes.Green;
                    break;
                case Answer.ansSecond:
                    secondAnsBtn.Foreground = Brushes.Green;
                    break;
                case Answer.ansThird:
                    thirdAnsBtn.Foreground = Brushes.Green;
                    break;
                case Answer.ansFourth:
                    fourthAnsBtn.Foreground = Brushes.Green;
                    break;
                default:
                    //ERROR
                    break;
            }
            
        }

        public void setIsAnswered(Boolean isAnswered)
        {
            this._isAnsweredMutex.WaitOne();   // Wait until it is safe to enter.
            this._isAnswered = isAnswered;
            this._isAnsweredMutex.ReleaseMutex();    // Release the Mutex.
        }
        public Boolean getIsAnswered()
        {
            Boolean isAnswered;
            this._isAnsweredMutex.WaitOne();   // Wait until it is safe to enter.
            isAnswered = this._isAnswered;
            this._isAnsweredMutex.ReleaseMutex();    // Release the Mutex.
            return isAnswered;
        }

        public void setIsCorrect(Boolean isCorrect)
        {
            this._isCorrectMutex.WaitOne();   // Wait until it is safe to enter.
            this._isCorrect = isCorrect;
            this._isCorrectMutex.ReleaseMutex();    // Release the Mutex.
        }
        public Boolean getIsCorrect()
        {
            Boolean isCorrect;
            this._isCorrectMutex.WaitOne();   // Wait until it is safe to enter.
            isCorrect = this._isCorrect;
            this._isCorrectMutex.ReleaseMutex();    // Release the Mutex.
            return isCorrect;
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }
    }
}
