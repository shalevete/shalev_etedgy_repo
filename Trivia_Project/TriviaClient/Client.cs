﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;
using System.Diagnostics;

namespace TriviaClient
{

    partial class SocketException : Exception
    {
        public string exceptionMessage;
        public SocketException(string message)
        {
            exceptionMessage = message;
        }

    }

    partial class loginReq
    {
        public string username;
        public string password;
        public loginReq(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

    }

    partial class signUpReq
    {
        public string username;
        public string password;
        public string email;
        public signUpReq(string username, string password, string email)
        {
            this.username = username;
            this.password = password;
            this.email = email;
        }

    }
    class joinRoomRequest
    {
        public int roomId;
        public joinRoomRequest(int roomId)
        {
            this.roomId = roomId;
        }
    }
    class createRoomReq
    {
        public int answerTimeout;
        public int maxUsers;
        public int questionCount;
        public string roomName;
        public createRoomReq(int ansTime, int maxUsers, int qCount, string roomName)
        {
            this.answerTimeout = ansTime;
            this.maxUsers = maxUsers;
            this.questionCount = qCount;
            this.roomName = roomName;
        }
}
    class submitAnsReq
    {
        public bool answerTrue;
        public submitAnsReq(bool ans)
        {
            this.answerTrue = ans;
        }
    }
    partial class Room
    {
        public int Id;
        public int maxPlayers;
        public bool isActive;
        public string name;
        public int questionsCount;
        public int timePerQuestion;
        public Room(int Id, int maxPlayers, bool isActive, string name, int qCount, int timeQ)
        {
            this.Id = Id;
            this.maxPlayers = maxPlayers;
            this.questionsCount = qCount;
            this.name = name;
            this.timePerQuestion = timeQ;
            this.isActive = isActive;
        }

    }

    partial class Client
    {
        private Socket clientSocket;
        public Client()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = System.Net.IPAddress.Parse("127.0.0.1");
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 70);
            this.clientSocket = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            try
            {
                clientSocket.Connect(remoteEP);
            }
            catch(Exception ex)
            {
                throw (ex);
            }
        }
        /*
         Requests creation
         */
        public bool login_request(string username, string password)
        {

            loginReq req = new loginReq(username, password); //Create request class
            string jReq = JsonConvert.SerializeObject(req); //Serialize class
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(1, jReq))); //Send and desrialize request
            var dataRes = desrializeToDict();
            return dataRes["Data"].ToString() == "1";
        }
        /*
         Same as login_request
        */
        public bool signUpRequest(string username, string password, string email)
        {

            signUpReq req = new signUpReq(username, password, email);
            string jReq = JsonConvert.SerializeObject(req);
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(2, jReq)));
            var dataRes = desrializeToDict();
            return dataRes["Data"].ToString() == "1";
        }
        public List<Room> getRoomsRequest()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(4, "")));
            var dataRes = desrializeToDict();
            return JsonConvert.DeserializeObject<List<Room>>(dataRes["Data"].ToString());
        }

        public bool joinRoomRequest(int roomId)
        {
            joinRoomRequest req = new joinRoomRequest(roomId);
            string jReq = JsonConvert.SerializeObject(req);
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(6, jReq)));
            var dataRes = desrializeToDict();
            return dataRes["Data"].ToString() == "1";
        }
        public void startGameRequest()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(10, "")));
            var dataRes = desrializeToDict();
             dataRes["Data"].ToString();
        }
        public void deleteRoomRequest()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(9, "")));
            var dataRes = desrializeToDict();
            dataRes["Data"].ToString();
        }
        public void logOut()
        {
            this.clientSocket.Close();

        }
        public Dictionary<string, object> getQuestion()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(13, "")));
            var dataRes = desrializeToDict();
            var questionDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(dataRes["Data"].ToString());
            return questionDict;
        }
        public bool leaveRoom()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(12, "")));
            var dataRes = desrializeToDict();
            return dataRes["Data"].ToString() == "1";
        }
        public List<Dictionary<string, object>> getGameResults()
        {

            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(15, "")));
            var dataRes = desrializeToDict();
            var gameRes = JsonConvert.DeserializeObject<List<Dictionary<string,object>>>(dataRes["gameData"].ToString());
            return gameRes;
        }
        public Dictionary<string, object> getRoomState()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(11, "")));
            var dataRes = desrializeToDict();
            var roomState = JsonConvert.DeserializeObject<Dictionary<string, object>>(dataRes["Data"].ToString());
            return roomState;

        }
        public void submitAnswer(bool right)
        {
            submitAnsReq req = new submitAnsReq(right);
            string jReq = JsonConvert.SerializeObject(req);
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(14, jReq)));
            var dataRes = desrializeToDict();
        }
        public List<Dictionary<string, object>> getHighscores()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(8, "")));
            var dataRes = desrializeToDict();
            var highscores = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(dataRes["Data"].ToString());
            return highscores;

        }
        public List<int> getStatus()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(16, "")));
            var dataRes = desrializeToDict();
            var status = JsonConvert.DeserializeObject<List<int>>(dataRes["Data"].ToString());
            return status;
        }
        public bool CreateRoomRequest(int ansTime, int maxUsers, int qCount, string roomName)
        {
            createRoomReq req = new createRoomReq(ansTime, maxUsers, qCount, roomName);
            string jReq = JsonConvert.SerializeObject(req);
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(7, jReq)));
            var dataRes = desrializeToDict();
            return dataRes["Data"].ToString() == "1";

        }
        public bool resetHighScores()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize(17, "")));
            var dataRes = desrializeToDict();
            return dataRes["Data"].ToString() == "1";
        }

        /*
      desriealizeRequest
      desrialize the response of the request 
     */
        private Dictionary<string, object> desrializeToDict()
        {
            byte[] bytes = new byte[1024]; //Buffer 
            try
            {
                int bytesRec = clientSocket.Receive(bytes); // Recive
            }
            catch (Exception ex)
            {
                throw (new Exception("Error in recive packet"));
            }
            string toDesrialize = System.Text.Encoding.UTF8.GetString(bytes); //Desrialize
            Dictionary<string, object> values = JsonConvert.DeserializeObject<Dictionary<string, object>>(toDesrialize.Substring(toDesrialize.IndexOf("{"), toDesrialize.Length - toDesrialize.IndexOf("}")));

            return values;
        }
        /*
         final Serialize of length and request type
         */
        private string finalSerialize(int requestType, string data)
        {
            string request = requestType.ToString("00") + data.Length.ToString("0000") + data;
            return request;
        }
    }
}
;