﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for gameResult.xaml
    /// </summary>
    public partial class gameResult : Window
    {
        public gameResult(List<Dictionary<string, object>> res)
        {
            InitializeComponent();
            
            foreach(var dictRes in res)
            {
                TextBlock txtUsername = new TextBlock();
                TextBlock txtCorrect = new TextBlock();
                TextBlock txtWrong = new TextBlock();
                txtUsername.Text = dictRes["username"].ToString();
                txtCorrect.Text = dictRes["correctAnswerCount"].ToString();
                 txtWrong.Text = dictRes["wrongAnswerCount"].ToString();
                stackPanelUsersName.Children.Add(txtUsername);
                stackPanelWrongAnswers.Children.Add(txtWrong);
                stackPanelCorrectAnswers.Children.Add(txtCorrect);
           

            }


        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }
        private void BtnExit_Copy2_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow wnd = new MainWindow();
            this.Close();
            wnd.Show();
        }
    }
}
