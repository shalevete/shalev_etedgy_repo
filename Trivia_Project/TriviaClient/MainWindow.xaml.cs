﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void CreateRoomButton_Click(object sender, RoutedEventArgs e)
        {
            //open create room screen
            CreateRoomWindow wnd = new CreateRoomWindow();
            this.Close();
            wnd.Show();

        }

        private void JoinRoomButton_Click(object sender, RoutedEventArgs e)
        {
            JoinRoom wnd = new JoinRoom();
            this.Close();
            wnd.Show();
        }

        private void highscores_Click(object sender, RoutedEventArgs e)
        {
            List<Dictionary<string, object>> highscores = Global.clientSocket.getHighscores();
            //get highscores from server
            ScoresWindow wnd = new ScoresWindow(highscores);
            this.Close();
            wnd.Show();
        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }
        private void myStatus_Click(object sender, RoutedEventArgs e)
        {
           
            //get highscores from server
            StatusWindow wnd = new StatusWindow(Global.clientSocket.getStatus());
            this.Close();
            wnd.Show();
        }
        


        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }
    }
}