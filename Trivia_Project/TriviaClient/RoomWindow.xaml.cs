﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Threading;
namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for RoomWindow.xaml
    /// </summary>
    public partial class RoomWindow : Window
    {
        private Dictionary<string, object> roomState;
        private Thread _t;
        public RoomWindow(string roomName)
        {
            InitializeComponent();
          
            roomState = Global.clientSocket.getRoomState();
            RoomNameText.Text = roomName;
            TimeToAnsText.Text = TimeToAnsText.Text + this.roomState["answerTimeOut"].ToString();
            QuestionCountText.Text = QuestionCountText.Text + this.roomState["questionCount"].ToString();
            this._t = new Thread(playersList);
            this._t.Start();
        }
        public delegate void UpdatePlayerCallback(List<string> playersNames);
        private void UpdatePlayer(List<string> playersNames)
        {
            playerList.Items.Clear();
            foreach(string player in playersNames)
            {
                playerList.Items.Add(player);
            }

        }
        public delegate void UpdateGameCallback();
        private void UpdateGame()
        {
            Dictionary<string, object> question = Global.clientSocket.getQuestion();
            List<string> answers = JsonConvert.DeserializeObject<List<string>>(question["answers"].ToString());
            Question wnd = new Question(RoomNameText.Text.ToString(), question["question"].ToString(), answers, int.Parse(question["rightIndex"].ToString()) + 1, int.Parse(this.roomState["questionCount"].ToString()), int.Parse(this.roomState["answerTimeOut"].ToString()));
            this.Close();
            wnd.Show();
            this._t.Abort();
        }

        public delegate void UpdateLeaveAdminCallback();
        public void deleteRoom()
        {
            MessageBox.Show("Room deleted.");
            Global.clientSocket.leaveRoom();
            MainWindow wnd = new MainWindow();
            this.Close();
            wnd.Show();
            this._t.Abort();
        }
       
        private void playersList()
        {
            bool checkPlayers = true;
            while (checkPlayers)
            {
                roomState = Global.clientSocket.getRoomState();
                if(roomState["hasGameBegun"].ToString() == "True")
                {
                    checkPlayers = false;
                    Global.clientSocket.startGameRequest();
                    this.Dispatcher.Invoke(new UpdateGameCallback(this.UpdateGame));
                }
                else if(roomState["status"].ToString() == "0")
                {

                    this.Dispatcher.Invoke(new UpdateLeaveAdminCallback(this.deleteRoom));
                }
                playerList.Dispatcher.Invoke(new UpdatePlayerCallback(this.UpdatePlayer), new object[] { JsonConvert.DeserializeObject<List<string>>(roomState["Players"].ToString()) });
                System.Threading.Thread.Sleep(1000);
            }
        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }
        private void BtnLeave_Click(object sender, RoutedEventArgs e)
        {
            Global.clientSocket.leaveRoom();
            MainWindow wnd = new MainWindow();
            this.Close();
            wnd.Show();
            this._t.Abort();
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }
    }
}
