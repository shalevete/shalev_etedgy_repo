﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Threading;

namespace TriviaClient
{
    enum Score { BestFirst = 1, BestSecond, BestThird, BestFourth };

    public partial class ScoresWindow : Window
    {
        public ScoresWindow(List<Dictionary<string, object>> scores)
        {
            InitializeComponent();
            var sorted = scores.OrderByDescending(x => x["rightAns"]).ToList();

            Score i = Score.BestFirst;
            foreach (var score in sorted)
            {
                switch (i)
                {
                    case Score.BestFirst:
                        first.Text = score["username"].ToString() + ": " + score["rightAns"].ToString();
                        break;
                    case Score.BestSecond:
                        second.Text = score["username"].ToString() + ": " + score["rightAns"].ToString();
                        break;
                    case Score.BestThird:
                        third.Text = score["username"].ToString() + ": " + score["rightAns"].ToString();
                        break;
                    case Score.BestFourth:
                        fourth.Text = score["username"].ToString() + ": " + score["rightAns"].ToString();
                        break;
                    default:
                        break;
                }
                i++;
            }
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow wnd = new MainWindow();
            this.Close();
            wnd.Show();
        }
        
        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            if (Global.clientSocket.resetHighScores())
            {
                List<Dictionary<string, object>> highscores = Global.clientSocket.getHighscores();
                //get highscores from server
                ScoresWindow wnd = new ScoresWindow(highscores);
                this.Close();
                wnd.Show();
            }

        }
    }
}

