﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Threading;

namespace TriviaClient
{
    public partial class AdminRoom : Window
    {
        private string _roomName;
        private int _maxUsers;
        private int _ansTime;
        private int _qCount;
        private Thread _t;

        private Dictionary<string, object> roomState;

        public AdminRoom(int ansTime, int maxUsers, int qCount, string roomName)
        {
            InitializeComponent();
            this._roomName = roomName;
            this._maxUsers = maxUsers;
            this._qCount = qCount;
            this._ansTime = ansTime;
            RoomNameText.Text = roomName;
            TimeToAnsText.Text = TimeToAnsText.Text.ToString() + ansTime.ToString();
            MaxUsersText.Text = MaxUsersText.Text.ToString() + maxUsers.ToString();
            QuestionCountText.Text = QuestionCountText.Text.ToString() + qCount.ToString();
            this._t = new Thread(playersList);
            this._t.Start();
        }
        public delegate void UpdatePlayerCallback(List<string> playersNames);
        private void UpdatePlayer(List<string> playersNames)
        {
            playerList.Items.Clear();
            foreach (string player in playersNames)
            {
                playerList.Items.Add(player);
            }

        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }
        private void playersList()
        {
            bool checkPlayers = true;
            while(checkPlayers)
            {
                roomState = Global.clientSocket.getRoomState();
                playerList.Dispatcher.Invoke(new UpdatePlayerCallback(this.UpdatePlayer), new object[] { JsonConvert.DeserializeObject<List<string>>(roomState["Players"].ToString()) });
                System.Threading.Thread.Sleep(3000);
            }
        }


        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            this._t.Abort();
            Global.clientSocket.startGameRequest();
            Dictionary<string, object> question = Global.clientSocket.getQuestion();
            List<string> answers =  JsonConvert.DeserializeObject<List<string>>(question["answers"].ToString());
            Question wnd = new Question(this._roomName,question["question"].ToString(),answers, int.Parse(question["rightIndex"].ToString()) + 1,this._qCount,this._ansTime);
            this.Close();
            wnd.Show();
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            BtnDelete_room_Click(sender, e);
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }

        private void BtnDelete_room_Click(object sender, RoutedEventArgs e)
        {
            this._t.Abort();
            Global.clientSocket.deleteRoomRequest();
            MainWindow wnd = new MainWindow();
            this.Close();
            wnd.Show();
        }
    }
}
