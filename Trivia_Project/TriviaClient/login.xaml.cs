﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for login.xaml
    /// </summary>
    public static class Global
    {

        private static Client C = new Client();
    internal static Client clientSocket { get => C; set => C = value; }
    }

    public partial class login : Window
    {

        public login()
        {
            InitializeComponent();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e) //Click to login 
        {
            string username = UsernameTextBox.Text;
            string password = PasswordTextBox.Text;
            bool logged = false;
            try
            {
                logged = Global.clientSocket.login_request(username, password);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Server not connected, exit!");
                BtnExit_Click(sender, e);
            }
                if (logged) //Login good , open menu screen
                {
                    MainWindow wnd = new MainWindow();
                    this.Close();
                    wnd.Show();

                }
            else
            {
                MessageBox.Show("Log in failed");
            }
        }

        private void BtnSignup_Click(object sender, RoutedEventArgs e)
        {
            if (SignUpBox.Text == "E-mail (SignUp)")
            {
                MessageBox.Show("Please enter vaild mail");
            }
            else if (SignUpBox.Text == "")
            {
                SignUpBox.Visibility = Visibility.Visible;
                SignUpBox.Text = "      Enter Email";
            }
            else
            {
                string username = UsernameTextBox.Text;
                string password = PasswordTextBox.Text;
                string mail = SignUpBox.Text;
                bool sign = false;
                if (!IsValidEmail(mail))
                {
                    MessageBox.Show("Please enter vaild mail");
                }
                else
                {


                    try
                    {
                        sign = Global.clientSocket.signUpRequest(username, password, mail);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Server not connected, exit");
                        BtnExit_Click(sender, e);
                    }

                    if (sign) //Sign up process
                    {
                        MainWindow wnd = new MainWindow();
                        this.Close();
                        wnd.ShowDialog();


                    }
                    else
                    {
                        MessageBox.Show("Sign up failed | username exsits");
                    }
                }
            }
            }
      


        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();
        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        private void PasswordTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
