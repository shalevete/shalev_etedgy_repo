﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : Window
    {
        private List<Room> rooms;
        private Thread _t;
        public JoinRoom()
        {
            InitializeComponent();
            setRoomsInList();
            this._t = new Thread(this.updateRooms);
            this._t.Start();
        }
        public delegate void UpdateRoomCallback();
        private void setRoomsInList()
        {
            this.rooms = Global.clientSocket.getRoomsRequest();
            roomList.Items.Clear();
            foreach (Room room in this.rooms)
            {
                roomList.Items.Add(room.name);
            }
        }

        private void updateRooms()
        {
            bool checkRooms = true;
            while (checkRooms)
            {
                this.Dispatcher.Invoke(new UpdateRoomCallback(this.setRoomsInList));
                System.Threading.Thread.Sleep(3000);
            }
        }
        private void BtnJoin_Click(object sender, RoutedEventArgs e)
        {
            
            if(roomList.SelectedIndex == -1)
            {
                MessageBox.Show("Please select room.");
            }
            else if (Global.clientSocket.joinRoomRequest(findRoomIdByName(roomList.SelectedItem.ToString())))
            {
                this._t.Abort();
                RoomWindow wnd = new RoomWindow(roomList.SelectedItem.ToString());
                this.Close();
                wnd.Show();
            }
            else
            {
                MessageBox.Show("Full room");
            }
        }
        private int findRoomIdByName(string roomName)
        {
            int roomId = -1;
            foreach(Room room in this.rooms)
            {
                if(room.name == roomName)
                {
                    roomId = room.Id;
                }
            }
            return roomId;
        }
       

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow wnd = new MainWindow();
            this.Close();
            this._t.Abort();
            wnd.Show();
        }
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            // Begin dragging the window
            this.DragMove();
        }

        private void RoomList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }

}
