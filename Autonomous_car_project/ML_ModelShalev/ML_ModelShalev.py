import torch
import torchvision
from torchvision import transforms, datasets
from tqdm import tqdm
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.nn.functional as F
import os
import cv2
import numpy as np
import torch.optim as optim
import time

#true when data needs to be built
REBUILD_DATA = True

#image size
IMG_SIZE = 50

PATH = r"model.pth"

class GetData():
	training_data = []
	rightCount = 0
	leftCount = 0
	stopCount = 0

	def	loadData(self, isTrain):
		if isTrain:
			RIGHT_SIGN = r"C:\Users\magshimim\Desktop\FinalProject\signcar---\DataSetSignCar\Train\Right"
			LEFT_SIGN = r"C:\Users\magshimim\Desktop\FinalProject\signcar---\DataSetSignCar\Train\Left"
			STOP_SIGN = r"C:\Users\magshimim\Desktop\FinalProject\signcar---\DataSetSignCar\Train\Stop"
		else:
			RIGHT_SIGN = r"C:\Users\magshimim\Desktop\FinalProject\signcar---\DataSetSignCar\Test\Right\0033"
			LEFT_SIGN = r"C:\Users\magshimim\Desktop\FinalProject\signcar---\DataSetSignCar\Test\Left\0034"
			STOP_SIGN = r"C:\Users\magshimim\Desktop\FinalProject\signcar---\DataSetSignCar\Test\Stop\00014"
		
		LABELS = {RIGHT_SIGN: 0, LEFT_SIGN: 1, STOP_SIGN: 2}

		for label in LABELS:
			for f in tqdm(os.listdir(label)):
				if "png" in f:
					try:
						path = os.path.join(label, f)
						img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
						img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
						self.training_data.append([np.array(img), np.eye(3)[LABELS[label]]])

						if label == RIGHT_SIGN:
							self.rightCount += 1
						elif label == LEFT_SIGN:
							self.leftCount += 1
						elif label == STOP_SIGN:
							self.stopCount += 1

					except Exception as e:
						pass

		#shuffle and save the data
		if isTrain:
			np.random.shuffle(self.training_data)
			np.save("training_data.npy", self.training_data)
		else:
			np.random.shuffle(self.training_data)
			np.save("testing_data.npy", self.training_data)

class Net(nn.Module):
    def __init__(self):
        super().__init__() # just run the init of parent class (nn.Module)
        self.conv1 = nn.Conv2d(1, 32, 5) # input is 1 image, 32 output channels, 5x5 kernel / window
        self.conv2 = nn.Conv2d(32, 64, 5) # input is 32
        self.conv3 = nn.Conv2d(64, 128, 5)

        x = torch.randn(50,50).view(-1,1,50,50)
        self._to_linear = None
        self.convs(x)

        self.fc1 = nn.Linear(self._to_linear, 512) #flattening.
        self.fc2 = nn.Linear(512, 3) # 512 in, 3 out

    def convs(self, x):
        # max pooling over 2x2
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))

        if self._to_linear is None:
            self._to_linear = x[0].shape[0]*x[0].shape[1]*x[0].shape[2]
        return x

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self._to_linear)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=1)

if torch.cuda.is_available():
    device = torch.device("cuda:0")  # you can continue going on here, like cuda:1 cuda:2....etc. 
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")

def main():
	net = Net().to(device)

	BATCH_SIZE = 100
	EPOCHS = 4

	if REBUILD_DATA:
		training_data_loader = GetData()
		training_data_loader.loadData(True)
		training_data_loader = GetData()
		training_data_loader.loadData(False)

	training_data = np.load("training_data.npy", allow_pickle=True)
	testing_data = np.load("testing_data.npy", allow_pickle=True)


	optimizer = optim.Adam(net.parameters(), lr=0.001)
	loss_function = nn.MSELoss()

	train_X = torch.Tensor([i[0] for i in training_data]).view(-1,50,50)
	train_X = train_X/255.0
	train_y  = torch.Tensor([i[1] for i in training_data])

	test_X = torch.Tensor([i[0] for i in testing_data]).view(-1,50,50)
	test_X = test_X/255.0
	test_Y = torch.Tensor([i[1] for i in testing_data])


	for epoch in range(EPOCHS):
		for i in tqdm(range(0, len(train_X), BATCH_SIZE)):
			batch_X = train_X[i:i+BATCH_SIZE].view(-1, 1, 50, 50)
			batch_y = train_y[i:i+BATCH_SIZE]

			batch_X, batch_y = batch_X.to(device), batch_y.to(device)

			net.zero_grad()

			outputs = net(batch_X)
			loss = loss_function(outputs, batch_y)
			loss.backward()
			optimizer.step()    # Does the update

		print(f"Epoch: {epoch}. Loss: {loss}")

	print(loss)

	correct = 0
	total = 0
	with torch.no_grad():
		for i in tqdm(range(len(test_X))):
			real_class = torch.argmax(test_Y[i]).to(device)
			net_out = net(test_X[i].view(-1, 1, 50, 50)).to(device)[0]  # returns a list, 
			predicted_class = torch.argmax(net_out)

			if predicted_class == real_class:
				correct += 1
			total += 1
	print("Accuracy: ", round(correct/total, 3))


	torch.save(net, PATH)


if __name__== "__main__":
  main()
