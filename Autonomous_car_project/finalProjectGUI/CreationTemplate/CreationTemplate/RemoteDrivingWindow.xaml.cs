﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreationTemplate
{
    /// <summary>
    /// Interaction logic for RemoteDrivingWindow.xaml
    /// </summary>
    public partial class RemoteDrivingWindow : Window
    {
        public RemoteDrivingWindow(bool isLiveStreamingEnabled)
        {
            InitializeComponent();

            if(isLiveStreamingEnabled)
            {
                IsLiveStream.Content = "Yes";
            }
            else
            {
                IsLiveStream.Content = "No";
            }
        }
        
        public void DriveForward_click(object sender, RoutedEventArgs e)
        {

        }

        public void DriveBackward_click(object sender, RoutedEventArgs e)
        {

        }

        public void DriveLeft_click(object sender, RoutedEventArgs e)
        {

        }

        public void DriveRight_click(object sender, RoutedEventArgs e)
        {

        }

        public void StopDriving_Click(object sender, RoutedEventArgs e)
        {
            bool isEnded = Global.clientSocket.stopRequest();
            if (isEnded)
            {
                MessageBox.Show("car stopped");
                Exit_program(sender, e);
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }

        private void Exit_program(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }
    }
}
