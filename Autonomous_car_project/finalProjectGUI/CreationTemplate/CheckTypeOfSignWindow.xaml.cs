﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CreationTemplate
{
    /// <summary>
    /// Interaction logic for CheckTypeOfSignWindow.xaml
    /// </summary>
    public partial class CheckTypeOfSignWindow : Window
    {
        public CheckTypeOfSignWindow()
        {
            InitializeComponent();
            
        }

        private void Capture_click(object sender, RoutedEventArgs e)
        {
            string fileName = "C:/Users/magshimim/Desktop/FinalProject/signcar---/finalProjectGUI/CreationTemplate/SingleImages/singleImage.jpg";

            Capture_Btn.IsEnabled = false;
            bool isCaptured = Global.clientSocket.Capture_request();
            if (isCaptured)
            {
                Status.Content = "waiting for picture\nand results";
                string signType = Global.clientSocket.GetSign();
                Status.Content = signType;
                System.Threading.Thread.Sleep(1000);
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(fileName);
                bitmap.EndInit();
                CapturedImage.Source = bitmap; 
                CapturedImage.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }

        private void Exit_program(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }

    }
}
