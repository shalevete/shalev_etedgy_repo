﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using Newtonsoft.Json;

enum MessageType : int
{
    REQUEST = 1,
    ERROR = 0
}

enum InitialProperties : int
{
    AUTONOMOUS_DRIVING = 10,
    REMOTE_DRIVING = 30, 
    CHECK_SINGLE_IMAGE = 50
}

enum SignType : int
{
    ENDOFROUTE = 3,
    LEFT = 2,
    RIGHT = 1,
    STOP = 0,
    NONE = -1
}

namespace CreationTemplate
{

    partial class Client
    {
        private Socket clientSocket;
        public Client()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = System.Net.IPAddress.Parse("127.0.0.1");
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 4561);
            this.clientSocket = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            try
            {
                clientSocket.Connect(remoteEP);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        /*
         Requests creation
         */
        public bool InitialProperties_request(bool isAutonomousDriving)
        {
            string reqData = ((int)InitialProperties.REMOTE_DRIVING).ToString();
            if (isAutonomousDriving)
            {
                reqData = ((int)InitialProperties.AUTONOMOUS_DRIVING).ToString();
            }
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize((int)MessageType.REQUEST, reqData)));
            byte[] bytes = new byte[1024]; //Buffer 
            int bytesRec = clientSocket.Receive(bytes); // Recive
            string answerReceived = System.Text.Encoding.UTF8.GetString(bytes);
            answerReceived = answerReceived.Substring(0, bytesRec);
            return answerReceived.Equals("!YES");
        }


        public bool CheckTypeOfSign_request()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize((int)MessageType.REQUEST, ((int)InitialProperties.CHECK_SINGLE_IMAGE).ToString())));
            byte[] bytes = new byte[1024]; //Buffer 
            int bytesRec = clientSocket.Receive(bytes); // Recive
            string answerReceived = System.Text.Encoding.UTF8.GetString(bytes);
            answerReceived = answerReceived.Substring(0, bytesRec);
            return answerReceived.Equals("!YES");
        }

        public bool sendDirection(int direction)
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize((int)MessageType.REQUEST, direction.ToString())));
            byte[] bytes = new byte[1024]; //Buffer 
            int bytesRec = clientSocket.Receive(bytes); // Recive
            string answerReceived = System.Text.Encoding.UTF8.GetString(bytes);
            answerReceived = answerReceived.Substring(0, bytesRec);
            return answerReceived.Equals("!YES");
        }


        public string GetSign()
        {
            byte[] bytes = new byte[1024]; //Buffer 
            int bytesRec = clientSocket.Receive(bytes); // Recive
            string answerReceived = System.Text.Encoding.UTF8.GetString(bytes);
            answerReceived = answerReceived.Substring(1, bytesRec);
            int numOfSign = -2;
            Int32.TryParse(answerReceived, out numOfSign);
            if(numOfSign == (int)SignType.RIGHT)
            {
                return ("RIGHT");
            }
            else if(numOfSign == (int)SignType.LEFT)
            {
                return ("LEFT");
            }
            else if (numOfSign == (int)SignType.ENDOFROUTE)
            {
                return ("END OF ROUTE");
            }
            else if(numOfSign == (int)SignType.STOP)
            {
                return ("STOP");
            }
            else if (numOfSign == (int)SignType.NONE)
            {
                return ("NONE");
            }
            else
            {
                return ("ERROR");
            }
        }


        public bool stopRequest()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize((int)MessageType.REQUEST, "60")));
            byte[] bytes = new byte[1024]; //Buffer 
            int bytesRec = clientSocket.Receive(bytes); // Recive
            string answerReceived = System.Text.Encoding.UTF8.GetString(bytes);
            answerReceived = answerReceived.Substring(0, bytesRec);
            return answerReceived.Equals("!YES");
        }


        public bool Capture_request()
        {
            clientSocket.Send(Encoding.ASCII.GetBytes(finalSerialize((int)MessageType.REQUEST, "70")));
            byte[] bytes = new byte[1024]; //Buffer 
            int bytesRec = clientSocket.Receive(bytes); // Recive
            string answerReceived = System.Text.Encoding.UTF8.GetString(bytes);
            answerReceived = answerReceived.Substring(0, bytesRec);
            return answerReceived.Equals("!YES");
        }


        /*
         final Serialize of length and request type
         */
        private string finalSerialize(int requestType, string data)
        {
            string request = "$" + requestType.ToString("00") + "%" + data + "&" + data.Length.ToString("0000");
            return request;
        }
    }
}

