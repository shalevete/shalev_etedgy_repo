﻿#pragma checksum "..\..\..\RemoteDrivingWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C8DE73F134AC6FE1287B6C47A69A257FB42B4CC9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using CreationTemplate.UserControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace CreationTemplate {
    
    
    /// <summary>
    /// RemoteDrivingWindow
    /// </summary>
    public partial class RemoteDrivingWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 5 "..\..\..\RemoteDrivingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid LayoutRoot;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\RemoteDrivingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DriveForward_Btn;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\RemoteDrivingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Pause_Btn;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\RemoteDrivingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DriveBackward_Btn;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\RemoteDrivingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DriveLeft_Btn;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\RemoteDrivingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DriveRight_Btn;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\RemoteDrivingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label DrivingLog;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\RemoteDrivingWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Stop_Btn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/CreationTemplate;component/remotedrivingwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\RemoteDrivingWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.LayoutRoot = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.DriveForward_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 28 "..\..\..\RemoteDrivingWindow.xaml"
            this.DriveForward_Btn.Click += new System.Windows.RoutedEventHandler(this.DriveForward_click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Pause_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 29 "..\..\..\RemoteDrivingWindow.xaml"
            this.Pause_Btn.Click += new System.Windows.RoutedEventHandler(this.Pause_click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.DriveBackward_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 30 "..\..\..\RemoteDrivingWindow.xaml"
            this.DriveBackward_Btn.Click += new System.Windows.RoutedEventHandler(this.DriveBackward_click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.DriveLeft_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\..\RemoteDrivingWindow.xaml"
            this.DriveLeft_Btn.Click += new System.Windows.RoutedEventHandler(this.DriveRight_click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.DriveRight_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\..\RemoteDrivingWindow.xaml"
            this.DriveRight_Btn.Click += new System.Windows.RoutedEventHandler(this.DriveLeft_click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.DrivingLog = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.Stop_Btn = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\RemoteDrivingWindow.xaml"
            this.Stop_Btn.Click += new System.Windows.RoutedEventHandler(this.StopDriving_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

