﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreationTemplate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public static class Global
    {
        private static Client C = new Client();
        internal static Client clientSocket { get => C; set => C = value; }
    }


    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AutonomousDriving_Click(object sender, RoutedEventArgs e)
        {
            bool logged = false;
            try
            {
                logged = Global.clientSocket.InitialProperties_request(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Server not connected, exit!" + ex.ToString());
                Exit_program(sender, e);
            }
            if (logged)
            {
                AutonomousDrivingWindow wnd = new AutonomousDrivingWindow();
                this.Close();
                wnd.Show();

            }
            else
            {
                MessageBox.Show("connection failed!");
                Exit_program(sender, e);
            }
        }
    

        private void RemoteDriving_Click(object sender, RoutedEventArgs e)
        {
            bool logged = false;
            try
            {
                logged = Global.clientSocket.InitialProperties_request(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Server not connected, exit!" + ex.ToString());
                Exit_program(sender, e);
            }
            if (logged)
            {
                RemoteDrivingWindow wnd = new RemoteDrivingWindow();
                this.Close();
                wnd.Show();

            }
            else
            {
                MessageBox.Show("connection failed!");
                Exit_program(sender, e);
            }

        }

        private void CheckTypeOfSign_Click(object sender, RoutedEventArgs e)
        {
            bool logged = false;
            try
            {
                logged = Global.clientSocket.CheckTypeOfSign_request();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Server not connected, exit!" + ex.ToString());
                Exit_program(sender, e);
            }
            if (logged)
            {
                CheckTypeOfSignWindow wnd = new CheckTypeOfSignWindow();
                this.Close();
                wnd.Show();

            }
            else
            {
                MessageBox.Show("connection failed!");
                Exit_program(sender, e);
            }
        }

        private void Exit_program(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }
    }
}
