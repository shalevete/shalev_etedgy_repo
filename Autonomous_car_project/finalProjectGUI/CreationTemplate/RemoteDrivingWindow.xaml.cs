﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreationTemplate
{
    /// <summary>
    /// Interaction logic for RemoteDrivingWindow.xaml
    /// </summary>

    enum DrivingDirectionToSend : ushort
    {
        FORWARD = 100,
        RIGHT = 200,
        LEFT = 300,
        STOP = 400,
        REVERSE = 500
    }

    public partial class RemoteDrivingWindow : Window
    {
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public RemoteDrivingWindow()
        {
            InitializeComponent();
        }

        public void DriveForward_click(object sender, RoutedEventArgs e)
        {
            bool isDriving = Global.clientSocket.sendDirection((int)DrivingDirectionToSend.FORWARD);
            if (isDriving)
            {
                DrivingLog.Content = "Driving Forward\n";
                Disable_Every_Btn();
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }

        public void DriveBackward_click(object sender, RoutedEventArgs e)
        {
            bool isDriving = Global.clientSocket.sendDirection((int)DrivingDirectionToSend.REVERSE);
            if (isDriving)
            {
                DrivingLog.Content = "Driving Backward\n";
                Disable_Every_Btn();
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }

        public void DriveLeft_click(object sender, RoutedEventArgs e)
        {
            bool isDriving = Global.clientSocket.sendDirection((int)DrivingDirectionToSend.LEFT);
            if (isDriving)
            {
                DrivingLog.Content = "Driving Left\n";
                Disable_Every_Btn();
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }

        public void DriveRight_click(object sender, RoutedEventArgs e)
        {
            bool isDriving = Global.clientSocket.sendDirection((int)DrivingDirectionToSend.RIGHT);
            if (isDriving)
            {
                DrivingLog.Content = "Driving Right\n";
                Disable_Every_Btn();
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }

        public void StopDriving_Click(object sender, RoutedEventArgs e)
        {
            bool isEnded = Global.clientSocket.stopRequest();
            if (isEnded)
            {
                MessageBox.Show("car stopped");
                Exit_program(sender, e);
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }

        public void Pause_click(object sender, RoutedEventArgs e)
        {
            bool isPaused = Global.clientSocket.sendDirection((int)DrivingDirectionToSend.STOP);
            if (isPaused)
            {

                DrivingLog.Content = "Driving paused\n";
                Disable_Every_Btn();
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }
        private void Exit_program(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();

        }

        private void Disable_Every_Btn()
        {
            timer.Interval = 1500; // here time in milliseconds
            timer.Tick += timer_Tick;
            timer.Start();
            DriveForward_Btn.IsEnabled = false;
            Pause_Btn.IsEnabled = false;
            DriveBackward_Btn.IsEnabled = false;
            DriveLeft_Btn.IsEnabled = false;
            DriveRight_Btn.IsEnabled = false;
        }

        void timer_Tick(object sender, System.EventArgs e)
        {
            DriveForward_Btn.IsEnabled = true;
            DriveBackward_Btn.IsEnabled = true;
            DriveLeft_Btn.IsEnabled = true;
            DriveRight_Btn.IsEnabled = true;
            Pause_Btn.IsEnabled = true;
            timer.Stop();
        }

    }
}
