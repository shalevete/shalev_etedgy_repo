﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CreationTemplate
{
    /// <summary>
    /// Interaction logic for AutonomousDrivingWindow.xaml
    /// </summary>
    public partial class AutonomousDrivingWindow : Window
    {
        public AutonomousDrivingWindow()
        {
            InitializeComponent();
        }

        public void StopDriving_Click(object sender, RoutedEventArgs e)
        {
            bool isEnded = Global.clientSocket.stopRequest();
            if(isEnded)
            {
                MessageBox.Show("car stopped");
                Exit_program(sender, e);
            }
            else
            {
                MessageBox.Show("error in connection");
                Exit_program(sender, e);
            }
        }

        private void Exit_program(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();

            System.Windows.Application.Current.Shutdown();
        }
    }
}
