#import necessery packets
import torch
import torchvision
from torchvision import transforms, datasets
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import torch.optim as optim
import os
import cv2
import numpy as np
from tqdm import tqdm

#Run on the CPU or on the GPU
if torch.cuda.is_available():
    device = torch.device("cuda:0")  
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")


#set to true when you want to build your data(or you want to change something)
REBUILD_DATA = True
#set to True of you want to save your model
SAVE_MODEL = True
#the size of the image is IMG_SIZE*IMG_SIZE
IMG_SIZE = 60
#The image type - png(can be jpg)
IMG_FILE = "png"

BATCH_SIZE = 10
EPOCHS = 300

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1,32,3)
        self.conv2 = nn.Conv2d(32,64,3)
        self.conv3 = nn.Conv2d(64,128,3)
        self.conv4 = nn.Conv2d(128,256,3)

        x = torch.randn(IMG_SIZE,IMG_SIZE).view(-1,1,IMG_SIZE,IMG_SIZE)
        self._to_linear = None
        self.convs(x)

        self.fc1 = nn.Linear(self._to_linear, 512) #flattening
        self.fc2 = nn.Linear(512, 4) # 512 in, 4 out.

    def convs(self, x):
        # max pooling over 2x2
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv4(x)), (2, 2))

        if self._to_linear is None:
            self._to_linear = x[0].shape[0]*x[0].shape[1]*x[0].shape[2]
        return x

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self._to_linear)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=1)


def getFirstTensor(data):
    train = torch.Tensor([i[0] for i in data]).view(-1,IMG_SIZE,IMG_SIZE)
    return (train/255.0)


def getSecondTensor(data):
    train = torch.Tensor([i[1] for i in data])
    return train


def Count(stop,right,left,EndOfRoute):
     print("Stop:" + str(stop))
     print("Right" + str(right))
     print("Left:" + str(left))
     print("EndOfRoute:" + str(EndOfRoute))


#class of the three signs - stop, right and left
#this class help to load the data and to build the model
class TrainingData():
    #the path of the files with the dataset
    STOP_SIGN_PATH = "C:\\Users\\Eyal\\Desktop\\DanielProjects\\DataSetSignCar\\Train\\Stop"
    RIGHT_SIGN_PATH = "C:\\Users\\Eyal\\Desktop\\DanielProjects\\DataSetSignCar\\Train\\Right"
    LEFT_SIGN_PATH = "C:\\Users\\Eyal\\Desktop\\DanielProjects\\DataSetSignCar\\Train\\Left"
    END_ROUTE_SIGN_PATH = "C:\\Users\\Eyal\\Desktop\\DanielProjects\\DataSetSignCar\\Train\\EndOfRoute"

    LABELS = {STOP_SIGN_PATH: 0,RIGHT_SIGN_PATH: 1,LEFT_SIGN_PATH: 2,END_ROUTE_SIGN_PATH: 3}
    training_data = []
    
    #4 variables to know how many signs we have
    stopCount = 0
    rightCount = 0
    leftCount = 0
    endrouteCount = 0

    #this function loads the training data.
    def loadTrainingData(self):
        for label in self.LABELS:
            for file in tqdm(os.listdir(label)):
                #on the files in the directory
                if IMG_FILE in file:
                    try:
                        path = os.path.join(label, file)
                        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
                        #resize the image
                        img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
                        #make training data
                        self.training_data.append([np.array(img), np.eye(4)[self.LABELS[label]]]) 

                        #count of the pictures
                        if label == self.STOP_SIGN_PATH:
                            self.stopCount += 1

                        if label == self.RIGHT_SIGN_PATH:
                            self.rightCount += 1

                        if label == self.LEFT_SIGN_PATH:
                            self.leftCount += 1

                        if label == self.END_ROUTE_SIGN_PATH:
                            self.endrouteCount += 1

                    except Exception as e:
                        #for the csv files
                        pass
        #shuffle the data
        np.random.shuffle(self.training_data)
        #save the data
        np.save("training_data.npy", self.training_data)
        Count(self.stopCount,self.rightCount,self.leftCount,self.endrouteCount)
        print("Total trainnig pictures:" + str(self.stopCount + self.rightCount + self.leftCount + self.endrouteCount))


class TestingData():
    #the path of the files with the dataset
    STOP_SIGN_PATH = "C:\\Users\\Eyal\\Desktop\\DanielProjects\\DataSetSignCar\\Test\\Stop"
    RIGHT_SIGN_PATH = "C:\\Users\\Eyal\\Desktop\\DanielProjects\\DataSetSignCar\\Test\\Right"
    LEFT_SIGN_PATH = "C:\\Users\\Eyal\\Desktop\\DanielProjects\\DataSetSignCar\\Test\\Left"
    END_ROUTE_SIGN_PATH = "C:\\Users\\Eyal\\Desktop\\DanielProjects\\DataSetSignCar\\Test\\EndOfRoute"

    LABELS = {STOP_SIGN_PATH: 0,RIGHT_SIGN_PATH: 1,LEFT_SIGN_PATH: 2,END_ROUTE_SIGN_PATH: 3}

    testing_data = []
    
    #3 variables to know how many signs we have
    stopCount = 0
    rightCount = 0
    leftCount = 0
    endrouteCount = 0

    def loadTestingData(self):
        for label in self.LABELS:
            for file in tqdm(os.listdir(label)):
                #on the files in the directory
                if IMG_FILE in file:
                    try:
                        path = os.path.join(label, file)
                        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
                        #resize the image
                        img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
                        #make training data
                        self.testing_data.append([np.array(img), np.eye(4)[self.LABELS[label]]]) 

                        #count of the pictures
                        if label == self.STOP_SIGN_PATH:
                            self.stopCount += 1

                        if label == self.RIGHT_SIGN_PATH:
                            self.rightCount += 1

                        if label == self.LEFT_SIGN_PATH:
                            self.leftCount += 1

                        if label == self.END_ROUTE_SIGN_PATH:
                            self.endrouteCount += 1

                    except Exception as e:
                        #for the csv files
                        pass
        #shuffle the data
        np.random.shuffle(self.testing_data)
        #save the data
        np.save("testing_data.npy", self.testing_data)
        Count(self.stopCount,self.rightCount,self.leftCount,self.endrouteCount)
        print("Total testing pictures:" + str(self.stopCount + self.rightCount + self.leftCount + self.endrouteCount))

def main():
    net = Net().to(device)

    if REBUILD_DATA:
       LoadTrainingData = TrainingData()
       LoadTrainingData.loadTrainingData()
       LoadTestingData = TestingData()
       LoadTestingData.loadTestingData()

    training_data = np.load("training_data.npy", allow_pickle=True)

    testing_data = np.load("testing_data.npy", allow_pickle=True)
    
    optimizer = optim.Adam(net.parameters(), lr=0.001)
    loss_function = nn.MSELoss()

    train_X = getFirstTensor(training_data)
    train_y = getSecondTensor(training_data)

    test_X = getFirstTensor(testing_data)
    test_y = getSecondTensor(testing_data)
    
    #Training Part
    for epoch in range(EPOCHS):
        for i in tqdm(range(0, len(train_X), BATCH_SIZE)):
            batch_X = train_X[i:i+BATCH_SIZE].view(-1, 1, IMG_SIZE, IMG_SIZE)
            batch_y = train_y[i:i+BATCH_SIZE]

            batch_X, batch_y = batch_X.to(device), batch_y.to(device)

            net.zero_grad()

            outputs = net(batch_X)
            loss = loss_function(outputs, batch_y)
            loss.backward()
            optimizer.step()    # Does the update

        print(f"Epoch: {epoch}. Loss: {loss}")

    #Testing Part
    test_X.to(device)
    test_y.to(device)

    correct = 0
    total = 0
    with torch.no_grad():
        for i in tqdm(range(len(test_X))):
            real_class = torch.argmax(test_y[i]).to(device)
            net_out = net(test_X[i].view(-1, 1, IMG_SIZE, IMG_SIZE).to(device))[0]  # returns a list, 
            predicted_class = torch.argmax(net_out)

            if predicted_class == real_class:
                correct += 1
            total += 1

    print("Accuracy: ", round(correct/total, 3))

    #Save the model update
    if SAVE_MODEL:
        torch.save(net, "model.pth")



if __name__ == "__main__":
    main()
