#import necessery packets
import torch
import torchvision
from torchvision import transforms, datasets
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import torch.optim as optim
import os
import cv2
import numpy as np
from tqdm import tqdm

#the size of the image is IMG_SIZE*IMG_SIZE
IMG_SIZE = 60
#This is the percentage that will be the condition to identify the sign
PERCENTAGE = 30.0
#Run on the CPU or on the GPU
if torch.cuda.is_available():
    device = torch.device("cuda:0")  
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")


#The class of the model
class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1,32,3)
        self.conv2 = nn.Conv2d(32,64,3)
        self.conv3 = nn.Conv2d(64,128,3)
        self.conv4 = nn.Conv2d(128,256,3)

        x = torch.randn(IMG_SIZE,IMG_SIZE).view(-1,1,IMG_SIZE,IMG_SIZE)
        self._to_linear = None
        self.convs(x)

        self.fc1 = nn.Linear(self._to_linear, 512) #flattening
        self.fc2 = nn.Linear(512, 4) # 512 in, 4 out.

    def convs(self, x):
        # max pooling over 2x2
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv4(x)), (2, 2))

        if self._to_linear is None:
            self._to_linear = x[0].shape[0]*x[0].shape[1]*x[0].shape[2]
        return x

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self._to_linear)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=1)



def main():
    #load model
    model = torch.load("model.pth", map_location=torch.device('cpu'))
    model.eval()
    print("Model loaded.")

    ImagePath = "E:\image1.jpg"

    ImageData = loadPicture(ImagePath)
    ImageData.to(device)
    print("Image loaded.")

    ModelOutput = model(ImageData[0].view(-1, 1, IMG_SIZE, IMG_SIZE).to(device))[0]
    if CheckSign(ModelOutput):
        predicted_class = torch.argmax(ModelOutput)
        SignPrint(predicted_class)
    else:
        print("The sign cannot be identified.")
 
def loadPicture(path):
    #Data List
    Image_data = []
    #Read the image in grayscale
    Image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    #resize the image
    img = cv2.resize(Image, (IMG_SIZE, IMG_SIZE))
    #add
    Image_data.append([np.array(img), np.eye(4)]) 
    return getTensor(Image_data)

#This function get a picture and return 
#which sign in it.  
#In case there is no sign - she will return -1
def IdentifyImage(Image):
    #load the model
    model = torch.load("C:\\Users\\Eyal\\Desktop\\DanielProjects\\signcar---\\Models\\Build_ML_Model\\model.pth")
    model.eval()
    predicted_class = 1
    Image_data = []
    #resize the image
    img = cv2.resize(Image, (IMG_SIZE, IMG_SIZE))
    #add
    Image_data.append([np.array(img), np.eye(3)])
    #tensor of the data
    data = getTensor(Image_data)
    data.to(device)
    
    ModelOutput = model(data[0].view(-1, 1, IMG_SIZE, IMG_SIZE).to(device))[0]
    #chech that the sign is from the data set
    if CheckSign(ModelOutput):
        predicted_class = torch.argmax(ModelOutput)
        return predicted_class
    else:
        return -1


#this fucntion prints which sign is that
def SignPrint(label):
    if label == 0:
        print("The sign is Stop Sign.")
    if label == 1:
        print("The Sign is Right Sign.")
    if label == 2:
        print("The sign is Left Sign.")
    if label == 3:
        print("The sign is EndOfRoute Sign.")

def CheckSign(ModelOutput):
    signfound = False
    sm = torch.nn.Softmax(dim=-1)
    x = sm(ModelOutput)
    x = x*100
    for i in x:
        if i > PERCENTAGE:
            signfound = True
    return signfound

def getTensor(data):
    train = torch.Tensor([i[0] for i in data]).view(-1,IMG_SIZE,IMG_SIZE)
    return (train/255.0)


if __name__ == "__main__":
    main()
