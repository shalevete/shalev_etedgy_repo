#import necessery packets
import cv2
import time
import numpy as np
import random
import socket, select
from time import gmtime, strftime
from random import randint
from io import BytesIO
from PIL import Image
import boto3
from botocore.exceptions import ClientError
import json
from enum import Enum
import picamera
import time

#Class: Message type - types of the messages in the protocol.
class MessageType(Enum):
    ERROR = 0
    SYSTEM_COMMAND = 1
    REQUEST = 2
    ANSWER = 3

#Class: System Command Types -  types of the commands in the protocol.
class SystemCommandType(Enum):
    START_ROUTE = 10
    END_ROUTE = 20
    START_REMOTE_DRIVING = 30
    END_REMOTE_DRIVING = 40
    CHECK_SIGN_TYPE = 50

#Class: Sign Type - types of signs.
class SignType(Enum):
    ENDOFROUTE = 3
    LEFT = 2
    RIGHT = 1
    STOP = 0
    NONE = -1

#Class: Driving Direction - types of directions.
class DrivingDirection(Enum):
    FORWARD = 3
    RIGHT = 4
    LEFT = 5
    STOP = 6
    REVERSE = 7

#Type of classification to the AWS server
BUCKET = "sign-detect"
#Address
HOST = '192.168.1.3'
#Port
PORT = 1861

def main():
    #Open socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (HOST, PORT)
    sock.connect(server_address)

    ans = (sock.recv(1024)).decode('utf-8')

    typeOfCommand = getCommandTypeFromAnswer(ans)
    if SystemCommandType(typeOfCommand) ==  SystemCommandType.START_ROUTE:
        StartDriving(sock)
    elif SystemCommandType(typeOfCommand) ==  SystemCommandType.START_REMOTE_DRIVING:
        RemoteDriving(sock)
    elif SystemCommandType(typeOfCommand) ==  SystemCommandType.CHECK_SIGN_TYPE:
        CheckTypeOfSign(sock)

    sock.close()
    

def StartDriving(sockfd):
    EndRoute = False
    #Start driving forward
    camera = picamera.PiCamera()
    while not EndRoute:
        camera.capture('images/picture.jpg')
        print("captured")
        #Sending the image to the AWS server
        isUploaded = upload_file('images/picture.jpg', BUCKET,'picture.jpg')
        print('uploaded')
        if (isUploaded):
            data = createRequest('picture.jpg')
        else:
            data = createError("Unable to open file.")

        #Send update to the server on the PC
        print("sending: "+ data)
        sockfd.sendall(data.encode('utf-8'))
        print("sent.")

        #Receive answer from the server on the PC
        ans = (sockfd.recv(1024)).decode('utf-8')
        print('recieved: ' +ans)
        print(ans)
        typeOfAnswer = getCommandTypeFromAnswer(ans)
        if typeOfAnswer ==  DrivingDirection.FORWARD.value:
            print("Keep Forward.")
            #DriveForward()
        elif typeOfAnswer ==  DrivingDirection.LEFT.value:
            print("Turning Left...")
            #TurnLeft()
            #DriveForward()
        elif typeOfAnswer ==  DrivingDirection.RIGHT.value:
            print("Turning Right...")
            #TurnRight()
            #DriveForward()
        elif typeOfAnswer ==  DrivingDirection.STOP.value:
            print("Stopping...")
            #Stop()
            #DriveForward()
        elif typeOfAnswer ==  SystemCommandType.END_ROUTE.value:
            print("Stopping...")
            #Stop()
            EndRoute = True


def RemoteDriving(sockfd):
    StopDriving = False
   
    while not StopDriving:
        ans = (sockfd.recv(1024)).decode('utf-8')
        print(ans)
        typeOfAnswer = getCommandTypeFromAnswer(ans)
        if typeOfAnswer ==  DrivingDirection.FORWARD.value:
            print("Keep Forward for 250 miliseconds.")
            #DriveForwardRemote()
        elif typeOfAnswer ==  DrivingDirection.RIGHT.value:
            print("Turning Right...")
            #TurnRight()
        elif typeOfAnswer ==  DrivingDirection.LEFT.value:
            print("Turning Left...")
            #TurnLeft()
        elif typeOfAnswer ==  DrivingDirection.REVERSE.value:
            print("Driving Reverse for 250 miliseconds.")
            #DriveReverseRemote()
        elif typeOfAnswer ==  DrivingDirection.STOP.value:
            print("Stopping(for 1 second)...")
            #Stop()
        elif typeOfAnswer ==  SystemCommandType.END_REMOTE_DRIVING.value:
            print("Stopping...")
            #Stop()
            StopDriving = True


def CheckTypeOfSign(sockfd):
    camera = picamera.PiCamera()
    #wait for capture request
    ans = (sockfd.recv(1024)).decode('utf-8')
    print("Received request from the server.")
    data = str(ans[ans.find('#')+1:ans.find('!')])
    print(data)
    if(data == 'Capture'):
        camera.capture('images/single_picture.jpg')
        print("captured")
        #Sending the image to the AWS server
        isUploaded = upload_file('images/single_picture.jpg', BUCKET,'single_picture.jpg')
        if (isUploaded):
            data = createRequest('single_picture.jpg')
        else:
            data = createError("Unable to open file.")
        #Send update to the server on the PC
        sockfd.sendall(data.encode('utf-8'))
        print("Update sent.")
        #Receive answer from the server on the PC
        ans = (sockfd.recv(1024)).decode('utf-8')
        print("Received answer from the server.")
        typeOfAnswer = getSignTypeFromAnswer(ans)
        print("The type of the sign is:" + typeOfAnswer)
    else:
        data = createError("ERROR RECIEVED.")
        sockfd.sendall(data.encode('utf-8'))
        print("ERROR Update sent.")
        
    

"""Upload a file to an S3 bucket

:param file_name: File to upload
:param bucket: Bucket to upload to
:param object_name: S3 object name. If not specified then file_name is used
:return: True if file was uploaded, else False
"""

def upload_file(file_name, bucket, object_name):
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name
    # Upload the file
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True

#This functions creats a string request to the server on the PC
def createRequest(file_name):
    return ("@" + str(MessageType.REQUEST.value) + "#" + file_name + "!" + str(len(file_name)))

#This functions creats a string error to the server on the PC
#(In case that the file is not opened)
def createError(errorDetails):
    return ("@" + str(MessageType.ERROR.value) + "#" + errorDetails + "!" + str(len(errorDetails)))

#This function returns the type of the sign
#(Translate the answer from the server on the PC) 
def getSignTypeFromAnswer(answer):
   signNumber = int((answer[answer.find('#')+1:answer.find('!')]))
   return (SignType(signNumber).name)

def getCommandTypeFromAnswer(answer):
   sysNumber = int((answer[answer.find('#')+1:answer.find('!')]))
   return (sysNumber)

if __name__ == "__main__":
    main()
