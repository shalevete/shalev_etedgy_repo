#import necessery packets
import sys
import os
import torch
import torchvision
from torchvision import transforms, datasets
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import torch.optim as optim
import cv2
import numpy as np
import socket, select
from time import gmtime, strftime
from random import randint
from PIL import Image
import io
from PIL import ImageFile
import imp
import logging
import boto3
from botocore.exceptions import ClientError
import json
from enum import Enum
import threading
import shutil

#Last version of the model that saved.
MODEL_PATH = r"C:\Users\magshimim\Desktop\FinalProject\signcar---\Models\Build_ML_Model\model.pth"

#Address
HOST_OfRP = '192.168.1.2'
HOST_OfUI = '127.0.0.1'

#Port
PORT_ToRP = 1861
PORT_ToUI = 4561

#Type of classification to the AWS server
BUCKET = "sign-detect"

#Size of the buffer
BUFFER = 1024

#the size of the image is IMG_SIZE*IMG_SIZE
IMG_SIZE = 60

#This is the percentage that will be the condition to identify the sign.
PERCENTAGE = 30.0

#global boolean - if true: driving ends
_isStopPressed = False
stopFlag_lock = threading.Lock()

#Run on the CPU or on the GPU
if torch.cuda.is_available():
    device = torch.device("cuda:0")  
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")

#Class: Message type - types of the messages in the protocol.
class MessageType(Enum):
    ERROR = 0
    SYSTEM_COMMAND = 1
    REQUEST = 2
    ANSWER = 3

#Class: System Command Types -  types of the commands in the protocol.
class SystemCommandType(Enum):
    START_ROUTE = 10
    END_ROUTE = 20
    START_REMOTE_DRIVING = 30
    END_REMOTE_DRIVING = 40
    CHECK_SIGN_TYPE = 50

#Class: Sign Type - types of signs.
class SignType(Enum):
    ENDOFROUTE = 3
    LEFT = 2
    RIGHT = 1
    STOP = 0
    NONE = -1

#Class: Driving Direction - types of directions.
class DrivingDirection(Enum):
    FORWARD = 3
    RIGHT = 4
    LEFT = 5
    STOP = 6
    REVERSE = 7

#Class: UI Choice - types of the actions that the UI can request.
class UIChoice(Enum):
    SELF_DRIVING = 1
    REMOTE_CONTROL = 2
    CHECK_TYPE_OF_SIGN = 12

#The class of the model.
class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1,32,3)
        self.conv2 = nn.Conv2d(32,64,3)
        self.conv3 = nn.Conv2d(64,128,3)
        self.conv4 = nn.Conv2d(128,256,3)

        x = torch.randn(IMG_SIZE,IMG_SIZE).view(-1,1,IMG_SIZE,IMG_SIZE)
        self._to_linear = None
        self.convs(x)

        self.fc1 = nn.Linear(self._to_linear, 512) #flattening
        self.fc2 = nn.Linear(512, 4) # 512 in, 4 out.

    def convs(self, x):
        # max pooling over 2x2
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv4(x)), (2, 2))

        if self._to_linear is None:
            self._to_linear = x[0].shape[0]*x[0].shape[1]*x[0].shape[2]
        return x

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self._to_linear)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=1)


#Loading the model to identify the sign
print("Loading Model...")
model = torch.load(MODEL_PATH, map_location=torch.device('cpu'))
model.eval()
print("Model loaded.")

def main():
    #Open TCP socket - With the raspberry pi
    socketToRP = OpenTCPSocket('',PORT_ToRP,"RP")
    sockfdRP, client_addressRP = socketToRP.accept()
    print("accepted...(RP)\n")

    #Open TCP socket - With the UI
    socketToUI = OpenTCPSocket(HOST_OfUI, PORT_ToUI, "UI")
    sockfdUI, client_addressUI = socketToUI.accept()
    print("accepted...(UI)\n")

    choice = getChoiceFromUI(sockfdUI)

    #One of the main function activated - SelfDriving, Remote Control or check type of sign. 
    clientChoice(choice,sockfdUI,sockfdRP)

    #Closing the sockets
    closeSockets(sockfdRP,socketToRP,sockfdUI,socketToUI)


#*********************************************************** Main Functions ***********************************************************************

def StartRoute(sockfdRP, sockfdUI):
    global _isStopPressed
    global stopFlag_lock

    SignIndex = SignType.NONE
    currentSign = SignType.NONE
    EndRoute = False
    seqIdentify = 0
    Answers = 0

    try:
        CheckThread = threading.Thread(target=WaitForStop, args=(sockfdUI, ))
        CheckThread.daemon = True  # set thread to daemon ('ok' won't be printed in this case)
        CheckThread.start()
    except:
       print ("Error: unable to start thread.")

    #While the model didn't recognize the EndOfRoute Sign.  
    while not EndRoute:
        #Recieved a new name of image
        message = (sockfdRP.recv(BUFFER)).decode('utf-8')
        signImage = getSignImage(message)

        #Get the type of the sign
        SignIndex = getTypeOfSign(signImage)

        stopFlag_lock.acquire()
        if ((seqIdentify > 0) and (currentSign != SignIndex) and (currentSign == SignType.ENDOFROUTE)) or (_isStopPressed):
            sendCommand(sockfdRP,SystemCommandType.END_ROUTE)
            EndRoute = True
        elif Answers == 0:
            #The first sign in the route
            currentSign = SignIndex
            seqIdentify += 1
            sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))
        elif seqIdentify == 0:
            currentSign = SignIndex
            seqIdentify +=1
            sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))
        elif (seqIdentify > 0) and (currentSign == SignIndex):
            seqIdentify += 1
            sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))
        elif (seqIdentify > 0) and (currentSign != SignIndex):
            sockfdRP.sendall(createCommand(FindCurrentSign(currentSign)).encode('utf-8'))
            seqIdentify = 0
        stopFlag_lock.release()
        Answers += 1
    CheckThread.join()
        

def CheckTypeOfSign(sockfdRP, sockfdUI):
    msgFromUI = sockfdUI.recv(BUFFER)
    reqType = getRequestType(msgFromUI)

    if(reqType == 23):
        ans = "Capture"
        SendConfirmToUI(sockfdUI)
        sockfdRP.sendall(str('@' + str(reqType) + '#' + str(ans) + '!' + str(len(str(ans)))).encode('utf-8'))
        message = (sockfdRP.recv(BUFFER)).decode('utf-8')
        signImage = getSignImage(message)
        SignIndex = getTypeOfSign(signImage)
        print("Recieved update.")

        data = getDatafromMessage(message)
        msgType = getMessageType(message)

        signImage = checkTypeOfMsg(msgType,data)


        if signImage is not None:
            signImage.save(r"C:\Users\magshimim\Desktop\FinalProject\signcar---\finalProjectGUI\CreationTemplate\SingleImages\singleImage.jpg")
            ImageData = editImageToModel(signImage)
            ModelOutput = model(ImageData[0].view(-1, 1, IMG_SIZE, IMG_SIZE).to(device))[0]
            os.remove("images/image.jpg")
            SignIndex = getSignType(ModelOutput)
        else:
            shutil.move(r"C:\Users\magshimim\source\repos\serverTry\serverTry_updated\images\image.jpg", r"C:\Users\magshimim\Desktop\FinalProject\signcar---\finalProjectGUI\CreationTemplate\SingleImages\singleImage.jpg")
            result = None
            SignIndex = -1

        sockfdRP.sendall(createAnswer(SignIndex).encode('utf-8'))
        sockfdUI.sendall((str(SignIndex)).encode('utf-8'))
    else:    
        SendErrorToUI(sockfdUI)
        message = (sockfdRP.recv(BUFFER)).decode('utf-8')
        print("ERROR update Recieved.")
        print(message)
    
          

def RemoteDrive(sockfdRP, sockfdUI):
    StopDriving = False
    currentDirection = DrivingDirection.FORWARD
    sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))

    while not StopDriving:
        msgFromUI = sockfdUI.recv(BUFFER)
        reqType = getRequestType(msgFromUI)
        if(reqType == 2):
            sendCommand(sockfdRP,SystemCommandType.END_REMOTE_DRIVING)
            SendConfirmToUI(sockfdUI)
            StopDriving = True
        elif(reqType == DrivingDirection.FORWARD.value):
            sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))
            SendConfirmToUI(sockfdUI)
        elif(reqType == DrivingDirection.RIGHT.value):
            sockfdRP.sendall(createCommand(DrivingDirection.RIGHT.value).encode('utf-8'))
            SendConfirmToUI(sockfdUI)
        elif(reqType == DrivingDirection.LEFT.value):
            sockfdRP.sendall(createCommand(DrivingDirection.LEFT.value).encode('utf-8'))
            SendConfirmToUI(sockfdUI)
        elif(reqType == DrivingDirection.STOP.value):
            sockfdRP.sendall(createCommand(DrivingDirection.STOP.value).encode('utf-8'))
            SendConfirmToUI(sockfdUI)
        elif(reqType == DrivingDirection.REVERSE.value):
            sockfdRP.sendall(createCommand(DrivingDirection.REVERSE.value).encode('utf-8'))
            SendConfirmToUI(sockfdUI)
        else:    
            sendCommand(sockfdRP,SystemCommandType.END_REMOTE_DRIVING)
            SendConfirmToUI(sockfdUI)
            StopDriving = True

#*********************************************************** Image Functions ***********************************************************************


#Input - list of the labels that in the image
#bucket = Bucket to upload to
#key = File to download from bucket
#max_labels = number of maximum labels to detect
#min_confidence - number of minimum confidence that the labels will have
#Output: list of labels that in the image
#The function identify hte labels in the image
def detect_labels(bucket, key, max_labels=20, min_confidence=50, region="us-east-2"):
	rekognition = boto3.client("rekognition", region)
	response = rekognition.detect_labels(Image={"S3Object": 
       {"Bucket": bucket,"Name": key,}},
		MaxLabels=max_labels,
		MinConfidence=min_confidence,)
	s3 = boto3.resource('s3')
	s3.Object(bucket, key).delete()
	return response['Labels']

#Input:Image path, the indexes of the image
#Output:cut image
#Then function get a image and returns the cut image
#(according to the indexes)
def cutImage(imagePath, widthToCut, heightToCut, leftToCut, topToCut):  
    # Size of the image in pixels (size of orginal image) 
    # (This is not mandatory) 
    im = Image.open(imagePath) 
    width, height = im.size 
    # Setting the points for cropped image 
    left = width * leftToCut
    top = height * topToCut
    right = left + (width* widthToCut)
    bottom = top + (height* heightToCut)
    # Cropped image of above dimension 
    # (It will not change orginal image) 
    im1 = im.crop((left, top, right, bottom))
    return im1

#Input:image path
#Output:image(with the sign only)
#This function get an image and 
#return a new picture with the sign only in it.
def getSignImage(imagePath):
    s3 = boto3.client('s3')
    #Downloading the image
    s3.download_file(BUCKET, imagePath, "images/image.jpg")
    for label in detect_labels(BUCKET, imagePath):
        if (label["Name"] == "Road Sign"):
            cutDetails = label["Instances"][0]["BoundingBox"]
            return cutImage("images/image.jpg", cutDetails["Width"], cutDetails["Height"], cutDetails["Left"], cutDetails["Top"])
    return None

#Input:image(with the sign only)
#Output:tensor
#The function convert the omage to a tensor that will
#be relavent to the model input.
def editImageToModel(signImage):
    Image_data = []
    img = cv2.imread("images/image.jpg", cv2.IMREAD_GRAYSCALE)
    #resize the image
    img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
    #add
    Image_data.append([np.array(img), np.eye(4)])
    #tensor of the data
    data = getTensor(Image_data)
    data.to(device)
    return data

#Input:string
#Output:image(with the sign only)
#The function get the image with the sign only
#(from the AWS server).
def getSignImage(message):
    print("Recieved update.")
    data = getDatafromMessage(message)
    msgType = getMessageType(message)
    #Get the sign image from another function.
    signImage = checkTypeOfMsg(msgType,data)
    return signImage

#*********************************************************** Protocol Functions ***********************************************************************

#Input:HOST(Address),PORT(int),Type(string)
#Output:new socket
#The function creates a new TCP socket.
def OpenTCPSocket(HOST,PORT,Type):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, PORT))
    sock.listen(100)
    print("listening...(" + Type + ")\n")
    return sock

#Input:socket,type(SystemCommandType - Enum)
#Output:None
#The function send a command.
def sendCommand(sockfd,Type):
    sockfd.sendall(createCommand(Type.value).encode('utf-8'))

#Input:socket
#Output:None
#The function send a confirm message. 
def SendConfirmToUI(sockfdUI):
    sockfdUI.sendall("!YES".encode('utf-8'))

#Input:socket
#Output:None
#The function send an error message. 
def SendErrorToUI(sockfdUI):
    sockfdUI.sendall("!ERR".encode('utf-8'))

#*****Need to edit function*******
#Input:type of sign(int)
#Output:string
#The function get a type of sign and returns the message
#that need to be send.
def createAnswer(typeOFSign):
    return ("@" + str(MessageType.ANSWER.value) + "#" + str(typeOFSign) + "!" + str(len(str(typeOFSign))))

#*****Need to edit function*******
#Input:type of command(int)
#Output:string
#The function get a type of command and returns the message
#that need to be send.
def createCommand(typeOfCommand):
    return ("@" + str(typeOfCommand) + "#" + str(typeOfCommand) + "!" + str(len(str(typeOfCommand))))

#Input:message
#Output:string
#The function get a message and returns the data 
#from the message.
def getDatafromMessage(msg):
   data = str((msg[msg.find('#')+1:msg.find('!')]))
   return (data)

#Input:message
#Output:Message type(Enum)
#The function get a message and returns the type 
#of the meesage.
def getMessageType(msg):
    type = MessageType(int(msg[msg.find('@')+1:msg.find('#')]))
    return type

#Input:MessageType(Enum),string
#Output:image(with the sign only)
#This function checks what is the type of the 
#message. If it's an error she return None
def checkTypeOfMsg(msgType,data):
    if (msgType == MessageType.ERROR):
        print ("ERR:" + data)
        return None
    elif (msgType == MessageType.REQUEST):
        return getSignImage(data)
    else:
        print ("ERR: Wrong Message Type")
        return None

#Input:socket
#Output:int
#The function returns the action that the UI sent.
def getChoiceFromUI(sockfdUI):
    msgFromUI = sockfdUI.recv(BUFFER)
    print("Recieved choice from UI.\n")
    msgFromUI = msgFromUI.decode('utf-8')
    #Another function that converts the choise(to int)
    choice = getChoice(msgFromUI)
    return choice

#*****Need to edit function*******
#Input:socket
#Output:None
#The function stop the driving (with the thread 
#opened int the past - global).
def WaitForStop(sockfdUI):
    #Global - because of the thread
    global _isStopPressed
    global stopFlag_lock

    msgFromUI = sockfdUI.recv(BUFFER)
    print("recieved stop(UI)\n")
    msgFromUI = msgFromUI.decode('utf-8')
    reqType = int(msgFromUI[0:2])
    #Stop the thread
    if(reqType == 2):
        stopFlag_lock.acquire()
        _isStopPressed = True
        stopFlag_lock.release()
        SendConfirmToUI(sockfdUI)
    else:
        SendErrorToUI(sockfdUI)

#Input:msg from socket
#Output:type of request(int)
#The function convert the request type from string to int 
def getRequestType(msgFromUI):
    print("Recieved command from UI(UI)\n")
    msgFromUI = msgFromUI.decode('utf-8')
    print(msgFromUI +" (UI)\n")
    return int(msgFromUI[0:2])

#*****Need to edit function*******
def getChoice(request):
    reqType = int(request[0:2])
    if(reqType == 1):
        reqDict = json.loads(request[2:-4])
        if(reqDict["isAutonomousDriving"]):
            return 1
        else:
            return 2
    elif (reqType == 12):
        return 12
    else:
        return 0

#*********************************************************** Model Functions ***********************************************************************

#Input:image(with the sign only)
#Output:type of sign in image
#The function checks what is the type of the sign in the image
def getTypeOfSign(signImage):
    if signImage is not None:
        signImage.save(r"C:\Users\magshimim\Desktop\FinalProject\signcar---\finalProjectGUI\CreationTemplate\SingleImages\singleImage.jpg")
        #Get the tensor of the data of the image
        ImageData = editImageToModel(signImage)
        #Get the type of the sign
        ModelOutput = model(ImageData[0].view(-1, 1, IMG_SIZE, IMG_SIZE).to(device))[0]
        os.remove("images/image.jpg")
        #from tensor to int
        SignIndex = getSignType(ModelOutput)
    else:
        shutil.move(r"C:\Users\magshimim\source\repos\serverTry\serverTry_updated\images\image.jpg", r"C:\Users\magshimim\Desktop\FinalProject\signcar---\finalProjectGUI\SingleImages\singleImage.jpg")
        result = None
        SignIndex = -1
    return SignIndex

#Input:ModelOutput(tensor)
#Output:int
#The function convert the Model output from 
#tensor to int
def getSignType(ModelOutput):
    #Check that the identify is ok
    if CheckSign(ModelOutput):
        #Convert to int
        predicted_class = torch.argmax(ModelOutput)
        return int(predicted_class)
    else:
        return -1

#Input:ModelOutput(tensor)
#Output:Bool
#This function checks that the
#image identification is OK
def CheckSign(ModelOutput):
    signfound = False
    sm = torch.nn.Softmax(dim=-1)
    x = sm(ModelOutput)
    x = x*100
   #Converted to percentage
    for i in x:
        if i > PERCENTAGE:
            signfound = True
    return signfound

#Input:list
#Output:tensor of the data(to the model)
#The function converts from the list of the data of the image
#to a tensor(to the model).
def getTensor(data):
    train = torch.Tensor([i[0] for i in data]).view(-1,IMG_SIZE,IMG_SIZE)
    return (train/255.0)

#*********************************************************** Print Functions ***********************************************************************        

#Input:int
#Output:None
#The function prints to the screen what is the type of the sign.
def PrintSign(SignIndex):
    if SignIndex == -1:
        print("There is no sign.")
    if SignIndex == 0:
        print("The sign is stop sign.")
    if SignIndex == 1:
        print("The sign is right sign.")
    if SignIndex == 2:
        print("The sign is left sign.")
    if SignIndex == 3:
        print("The sign is EndOfRoute sign.")


#*********************************************************** Other Functions ***********************************************************************

#Input:
#choice - int
#sockfdUI - TCP socket(Connection with UI)
#sockfdRP - TCP socket(Connection with Raspberry Pi)
def clientChoice(choice,sockfdUI,sockfdRP):
    if choice == UIChoice.SELF_DRIVING.value:
        SendConfirmToUI(sockfdUI)
        sendCommand(sockfdRP,SystemCommandType.START_ROUTE)
        StartRoute(sockfdRP, sockfdUI)

    elif choice == UIChoice.REMOTE_CONTROL.value:
        SendConfirmToUI(sockfdUI)
        sendCommand(sockfdRP,SystemCommandType.START_REMOTE_DRIVING)
        RemoteDrive(sockfdRP, sockfdUI)

    elif choice == UIChoice.CHECK_TYPE_OF_SIGN.value:
        SendConfirmToUI(sockfdUI)
        sendCommand(sockfdRP,SystemCommandType.CHECK_SIGN_TYPE)
        CheckTypeOfSign(sockfdRP, sockfdUI)

    else:
        SendErrorToUI(sockfdUI)
        print("Error in request from UI.")

#Input: 4 TCP sockets
#Output:None
#The function close the sockets.
def closeSockets(sockfdRP,socketToRP,sockfdUI,socketToUI):
    sockfdRP.close()
    socketToRP.close()
    sockfdUI.close()
    socketToUI.close()


#*****Need to edit function*******
#Input:int
#Output:Dri
#The function send a command.
def FindCurrentSign(currentSign):
    if currentSign == 0:
        return DrivingDirection.STOP.value
    if currentSign == 1:
        return DrivingDirection.RIGHT.value
    if currentSign == 2:
        return DrivingDirection.LEFT.value
    if currentSign == -1:
        return DrivingDirection.FORWARD.value

if __name__ == "__main__":
    main()
