#import necessery packets
import sys
import os
import torch
import torchvision
from torchvision import transforms, datasets
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import torch.optim as optim
import cv2
import numpy as np
import socket, select
from time import gmtime, strftime
from random import randint
from PIL import Image
import io
from PIL import ImageFile
import imp
import logging
import boto3
from botocore.exceptions import ClientError
import json
from enum import Enum

#Last version of the model that saved.
MODEL_PATH = r"C:\Users\Eyal\Desktop\DanielProjects\signcar---\Models\Build_ML_Model\model.pth"

#Address
HOST_OfRP = '127.0.0.1'
HOST_OfUI = '127.0.0.1'

#Port
PORT_ToRP = 4321
PORT_ToUI = 4561

#Type of classification to the AWS server.
BUCKET = "sign-detect"

#Size of the buffer.
BUFFER = 1024

#The size of the image is IMG_SIZE*IMG_SIZE
IMG_SIZE = 60

#This is the percentage that will be the condition to identify the sign.
PERCENTAGE = 30.0

#global boolean - if true: driving ends
_isStopPressed = False
stopFlag_lock = threading.Lock()

#Run on the CPU or on the GPU
if torch.cuda.is_available():
    device = torch.device("cuda:0")  
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")

#Class: Message type - types of the messages in the protocol.
class MessageType(Enum):
    ERROR = 0
    SYSTEM_COMMAND = 1
    REQUEST = 2
    ANSWER = 3

#Class: System Command Types -  types of the commands in the protocol.
class SystemCommandType(Enum):
    START_ROUTE = 10
    END_ROUTE = 20
    START_REMOTE_DRIVING = 30
    END_REMOTE_DRIVING = 40
    CHECK_SIGN_TYPE = 50

#Class: Sign Type - types of signs.
class SignType(Enum):
    ENDOFROUTE = 3
    LEFT = 2
    RIGHT = 1
    STOP = 0
    NONE = -1

#Class: Driving Direction - types of directions.
class DrivingDirection(Enum):
    FORWARD = 11
    RIGHT = 22
    LEFT = 33
    REVERSE = 44
    STOP = 55

#Class: UI Choice - types of the actions that the UI can request.
class UIChoice(Enum):
    SELF_DRIVING = 1
    REMOTE_CONTROL = 2
    CHECK_TYPE_OF_SIGN = 3

#The class of the model.
class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1,32,3)
        self.conv2 = nn.Conv2d(32,64,3)
        self.conv3 = nn.Conv2d(64,128,3)
        self.conv4 = nn.Conv2d(128,256,3)

        x = torch.randn(IMG_SIZE,IMG_SIZE).view(-1,1,IMG_SIZE,IMG_SIZE)
        self._to_linear = None
        self.convs(x)

        self.fc1 = nn.Linear(self._to_linear, 512) #flattening
        self.fc2 = nn.Linear(512, 4) # 512 in, 4 out.

    def convs(self, x):
        # max pooling over 2x2
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv4(x)), (2, 2))

        if self._to_linear is None:
            self._to_linear = x[0].shape[0]*x[0].shape[1]*x[0].shape[2]
        return x

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self._to_linear)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=1)

def main():
    #Open TCP socket - With the UI
    sockfdUI = OpenTCPSocket(HOST_OfUI, PORT_ToUI, "UI")
    
    #Open TCP socket - With the raspberry pi
    sockfdRP = OpenTCPSocket(HOST_OfRP,PORT_ToRP,"RP")

    choice = getChoiceFromUI(sockfdUI)

    if choice == UIChoice.SELF_DRIVING.value:
        SendConfirmToUI(sockfdUI)
        sendCommand(sockfdRP,SystemCommandType.START_ROUTE)
        StartRoute(sockfdRP, sockfdUI)
    elif choice == UIChoice.REMOTE_CONTROL.value:
        SendConfirmToUI(sockfdUI)
        sendCommand(sockfdRP,SystemCommandType.START_REMOTE_DRIVING)
        RemoteDrive(sockfdRP)
    elif choice == UIChoice.CHECK_TYPE_OF_SIGN.value:
        #SendConfirmToUI(sockfdUI)
        sendCommand(sockfdRP,SystemCommandType.CHECK_SIGN_TYPE)
        CheckTypeOfSign(sockfdRP)
    else:
        sockfdUI.sendall("!ERR".encode('utf-8'))
        print("Error in request from UI.")

    sockfdRP.close()
    socketToRP.close()
    sockfdUI.close()
    socketToUI.close()


def StartRoute(sockfdRP, sockfdUI):
    global _isStopPressed
    global stopFlag_lock

    SignIndex = SignType.NONE
    currentSign = SignType.NONE
    EndRoute = False
    seqIdentify = 0
    Answers = 0

    #Load Model before the Route Start
    device = torch.device("cpu")
    print("Loading Model...")
    model = torch.load(MODEL_PATH, map_location=torch.device('cpu'))
    model.eval()
    print("Model loaded.")

    try:
        CheckThread = threading.Thread(target=WaitForStop, args=(sockfdUI, ))
        CheckThread.daemon = True  # set thread to daemon ('ok' won't be printed in this case)
        CheckThread.start()
    except:
       print ("Error: unable to start thread.")

    #While the model didn't recognize the EndOfRoute Sign.  
    while not EndRoute:
        #Recieved a new name of image
        message = (sockfdRP.recv(BUFFER)).decode('utf-8')
        print("Recieved update.")
        data = getDatafromMessage(message)
        msgType = getMessageType(message)
        
        signImage = checkTypeOfMsg(msgType,data)
        os.remove("images/image.jpg")
        signImage.save("images/image.jpg")
        
        #Get the type of the sign
        if signImage is not None:
            ImageData = editImageToModel(signImage)
            ModelOutput = model(ImageData[0].view(-1, 1, IMG_SIZE, IMG_SIZE).to(device))[0]
            os.remove("images/image.jpg")
            SignIndex = getSignType(ModelOutput)
        else:
            result = None
            SignIndex = SignType.NONE

        stopFlag_lock.acquire()
        if ((seqIdentify > 0) and (currentSign != SignIndex) and (currentSign == SignType.ENDOFROUTE)) or (_isStopPressed):
            sockfdRP.sendall(createCommand(SystemCommandType.END_ROUTE.value).encode('utf-8'))
            EndRoute = True
        elif Answers == 0:
            #The first sign in the route
            currentSign = SignIndex
            seqIdentify += 1
            sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))
        elif seqIdentify == 0:
            currentSign = SignIndex
            seqIdentify +=1
            sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))
        elif (seqIdentify > 0) and (currentSign == SignIndex):
            seqIdentify += 1
            sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))
        elif (seqIdentify > 0) and (currentSign != SignIndex):
            sockfdRP.sendall(createCommand(FindCurrentSign(currentSign)).encode('utf-8'))
            seqIdentify = 0
        stopFlag_lock.release()
        Answers += 1
    CheckThread.join()
        

def CheckTypeOfSign(sockfdRP):
    #Loading the model to identify the sign
    print("Loading Model...")
    model = torch.load(MODEL_PATH)
    model.eval()
    print("Model loaded.")

    message = (sockfdRP.recv(BUFFER)).decode('utf-8')
    print("Recieved update.")

    data = getDatafromMessage(message)
    msgType = getMessageType(message)

    signImage = checkTypeOfMsg(msgType,data)
    os.remove("images/image.jpg")
    signImage.save("images/image.jpg")

    if signImage is not None:
        ImageData = editImageToModel(signImage)
        ModelOutput = model(ImageData[0].view(-1, 1, IMG_SIZE, IMG_SIZE).to(device))[0]
        os.remove("images/image.jpg")
        SignIndex = getSignType(ModelOutput)
    else:
        result = None
        SignIndex = -1

    sockfdRP.sendall(createAnswer(SignIndex).encode('utf-8'))
          

def RemoteDrive(sockfdRP):
    StopDriving = False
    #Loading in the gui for 
    #3 seconds until start driving
    currentDirection = DrivingDirection.FORWARD
    sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))

    while not StopDriving:
        #While not recived from gui server
        #ans = sock rcv from gui
        ans = "Driving Direction"
        if ans == "Forward":
            sockfdRP.sendall(createCommand(DrivingDirection.FORWARD.value).encode('utf-8'))
        elif ans == "Right":
            sockfdRP.sendall(createCommand(DrivingDirection.RIGHT.value).encode('utf-8'))
        elif ans == "Left":
            sockfdRP.sendall(createCommand(DrivingDirection.LEFT.value).encode('utf-8'))
        elif ans == "Reverse":
            sockfdRP.sendall(createCommand(DrivingDirection.REVERSE.value).encode('utf-8'))
        elif ans == "Stop":
            sockfdRP.sendall(createCommand(DrivingDirection.STOP.value).encode('utf-8'))
        elif ans == "EndRemoteDriving":
            sockfdRP.sendall(createCommand(SystemCommandType.END_REMOTE_DRIVING.value).encode('utf-8'))
            StopDriving = True
        
"""get list of the labels that in the image

:param bucket: Bucket to upload to
:param key: File to download from bucket
:param max_labels: number of maximum labels to detect
:param min_confidence: number of minimum confidence that the labels will have
:return: list of labels that in the image
"""
def detect_labels(bucket, key, max_labels=20, min_confidence=50, region="us-east-2"):
	rekognition = boto3.client("rekognition", region)
	response = rekognition.detect_labels(
		Image={
			"S3Object": {
	    	    "Bucket": bucket,
			    "Name": key,
			}
		},
		MaxLabels=max_labels,
		MinConfidence=min_confidence,
    
        )

	s3 = boto3.resource('s3')
	s3.Object(bucket, key).delete()
	return response['Labels']

def cutImage(imagePath, widthToCut, heightToCut, leftToCut, topToCut):  
    # Size of the image in pixels (size of orginal image) 
    # (This is not mandatory) 
    im = Image.open(imagePath) 

    width, height = im.size 
  
    # Setting the points for cropped image 
    left = width * leftToCut
    top = height * topToCut
    right = left + (width* widthToCut)
    bottom = top + (height* heightToCut)

    # Cropped image of above dimension 
    # (It will not change orginal image) 
    im1 = im.crop((left, top, right, bottom))
    return im1

#This function will get a picture and 
#return a new picture with the sign in it 
def getSignImage(photoPath):
    s3 = boto3.client('s3')
    s3.download_file(BUCKET, photoPath, "images/image.jpg")
    for label in detect_labels(BUCKET, photoPath):
        if (label["Name"] == "Road Sign"):
            cutDetails = label["Instances"][0]["BoundingBox"]
            return cutImage("images/image.jpg", cutDetails["Width"], cutDetails["Height"], cutDetails["Left"], cutDetails["Top"])
    return None

def createAnswer(typeOFSign):
    return ("@" + str(MessageType.ANSWER.value) + "#" + str(typeOFSign) + "!" + str(len(str(typeOFSign))))

def createCommand(typeOfCommand):
    return ("@" + str(typeOfCommand) + "#" + str(typeOfCommand) + "!" + str(len(str(typeOfCommand))))

def getDatafromMessage(msg):
   data = str((msg[msg.find('#')+1:msg.find('!')]))
   return (data)

def getMessageType(msg):
    type = MessageType(int(msg[msg.find('@')+1:msg.find('#')]))
    return type


def editImageToModel(signImage):
    Image_data = []
    #numpy_sign_image = np.array(signImage,dtype=np.uint8)
    img = cv2.imread("images/image.jpg", cv2.IMREAD_GRAYSCALE)
    #resize the image
    img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
    #add
    Image_data.append([np.array(img), np.eye(4)])
    #tensor of the data
    data = getTensor(Image_data)
    data.to(device)
    return data
        

def PrintSign(SignIndex):
    if SignIndex == -1:
        print("There is no sign.")
    if SignIndex == 0:
        print("The sign is stop sign.")
    if SignIndex == 1:
        print("The sign is right sign.")
    if SignIndex == 2:
        print("The sign is left sign.")
    if SignIndex == 3:
        print("The sign is EndOfRoute sign.")

#returns the class type
def getSignType(ModelOutput):
    if CheckSign(ModelOutput):
        predicted_class = torch.argmax(ModelOutput)
        return int(predicted_class)
    else:
        return -1

#This function checks that the
#image identification is OK
def CheckSign(ModelOutput):
    signfound = False
    sm = torch.nn.Softmax(dim=-1)
    x = sm(ModelOutput)
    x = x*100
    for i in x:
        if i > PERCENTAGE:
            signfound = True
    return signfound


def PrintOpen():
    print("What is the type of action:")
    print("1.Self-Driving")
    print("2.Remote driving.")
    print("3.Check type of sign.")
    ch = input("Your choice:")
    return int(ch)

#returns a tensor of the data(to the model)
def getTensor(data):
    train = torch.Tensor([i[0] for i in data]).view(-1,IMG_SIZE,IMG_SIZE)
    return (train/255.0)


#This function checks what is the type of the 
#message. If it's an error she return None
def checkTypeOfMsg(msgType,data):
    if (msgType == MessageType.ERROR):
        print ("ERR:" + data)
        return None
    elif (msgType == MessageType.REQUEST):
        return getSignImage(data)
    else:
        print ("ERR: Wrong Message Type")
        return None


def FindCurrentSign(currentSign):
    if currentSign == 0:
        return DrivingDirection.STOP.value
    if currentSign == 1:
        return DrivingDirection.RIGHT.value
    if currentSign == 2:
        return DrivingDirection.LEFT.value
    if currentSign == -1:
        return DrivingDirection.FORWARD.value

def sendCommand(sockfd,Type):
    sockfd.sendall(createCommand(Type.value).encode('utf-8'))

def OpenTCPSocket(HOST,PORT,Type):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, PORT))
    sock.listen(100)
    print("listening...(" + Type + ")\n")
    sock, client_address = socketToUI.accept()
    print("accepted...(" + Type + ")\n")
    return sock

def getChoiceFromUI(sockfdUI):
    msgFromUI = sockfdUI.recv(BUFFER)
    print("Recieved choice from UI.\n")
    msgFromUI = msgFromUI.decode('utf-8')
    choice = getChoice(msgFromUI)
    return choice

def WaitForStop(sockfdUI):
    global _isStopPressed
    global stopFlag_lock

    msgFromUI = sockfdUI.recv(BUFFER)
    print("recieved stop(UI)\n")
    msgFromUI = msgFromUI.decode('utf-8')
    reqType = int(msgFromUI[0:2])
    if(reqType == 2):
        stopFlag_lock.acquire()
        _isStopPressed = True
        stopFlag_lock.release()
        sockfdUI.sendall("!YES".encode('utf-8'))
    else:
        sockfdUI.sendall("!ERR".encode('utf-8'))

def SendConfirmToUI(sockfdUI):
    sockfdUI.sendall("!YES".encode('utf-8'))


def getChoice(request):
    reqType = int(request[0:2])
    if(reqType == 1):
        reqDict = json.loads(request[2:-4])
        if(reqDict["isAutonomousDriving"]):
            return 1
        else:
            return 2
    else:
        return 0

if __name__ == "__main__":
    main()
