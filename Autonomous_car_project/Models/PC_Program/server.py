#import necessery packets
import sys
import os
import torch
import torchvision
from torchvision import transforms, datasets
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import torch.optim as optim
import cv2
import numpy as np
import socket, select
from time import gmtime, strftime
from random import randint
from PIL import Image
import io
from PIL import ImageFile
import imp
import logging
import boto3
from botocore.exceptions import ClientError
import json
from enum import Enum
import threading
import shutil

#Last version of the model that saved.
MODEL_PATH = r"C:\Users\magshimim\Desktop\FinalProject\signcar---\Models\Build_ML_Model\model.pth"

#Address
HOST_OfRP = '192.168.1.11'
HOST_OfUI = '127.0.0.1'

#Port
PORT_ToRP = 2428
PORT_ToUI = 4561

#Type of classification to the AWS server
BUCKET = "sign-detect"

#Size of the buffer
BUFFER = 1024

#the size of the image is IMG_SIZE*IMG_SIZE
IMG_SIZE = 60

#This is the percentage that will be the condition to identify the sign.
PERCENTAGE = 30.0

#global boolean - if true: driving ends
_isStopPressed = False
stopFlag_lock = threading.Lock()



#Run on the CPU or on the GPU
if torch.cuda.is_available():
    device = torch.device("cuda:0")  
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")

class AnswersToUI():
    SUCCESS_UI = "YES"
    ERROR_UI = "ERR"

#Class: Message type - types of the messages in the protocol.
class MessageType(Enum):
    ERROR = 0
    REQUEST = 1
    ANSWER = 2

#Class: System Command Types -  types of the commands in the protocol.
class SystemCommandType(Enum):
    START_ROUTE = 10
    END_ROUTE = 20
    START_REMOTE_DRIVING = 30
    END_REMOTE_DRIVING = 40
    CHECK_SIGN_TYPE = 50
    FINISH_DRIVING = 60
    CAPTURE_IMAGE = 70

#Class: Driving Direction - types of directions.
class DrivingDirection(Enum):
    FORWARD = 100
    RIGHT = 200
    LEFT = 300
    STOP = 400
    REVERSE = 500

#Class: Sign Type - types of signs.
class SignType(Enum):
    ENDOFROUTE = 3
    LEFT = 2
    RIGHT = 1
    STOP = 0
    NONE = -1



#The class of the model.
class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(1,32,3)
        self.conv2 = nn.Conv2d(32,64,3)
        self.conv3 = nn.Conv2d(64,128,3)
        self.conv4 = nn.Conv2d(128,256,3)

        x = torch.randn(IMG_SIZE,IMG_SIZE).view(-1,1,IMG_SIZE,IMG_SIZE)
        self._to_linear = None
        self.convs(x)

        self.fc1 = nn.Linear(self._to_linear, 512) #flattening
        self.fc2 = nn.Linear(512, 4) # 512 in, 4 out.

    def convs(self, x):
        # max pooling over 2x2
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv4(x)), (2, 2))

        if self._to_linear is None:
            self._to_linear = x[0].shape[0]*x[0].shape[1]*x[0].shape[2]
        return x

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self._to_linear)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.softmax(x, dim=1)



#Loading the model to identify the sign
print("Loading Model...")
model = torch.load(MODEL_PATH, map_location=torch.device('cpu'))
model.eval()
print("Model loaded.")


def main():
    #Open TCP socket - With the raspberry pi
    socketToRP = OpenTCPSocket('',PORT_ToRP,"RP")
    sockfdRP, client_addressRP = socketToRP.accept()
    print("accepted...(RP)\n")

    #Open TCP socket - With the UI
    socketToUI = OpenTCPSocket(HOST_OfUI, PORT_ToUI, "UI")
    sockfdUI, client_addressUI = socketToUI.accept()
    print("accepted...(UI)\n")

    msgTypeUI, msgDataUI = receiveRequestFromUI(sockfdUI)
    startProgram(msgTypeUI, msgDataUI ,sockfdUI,sockfdRP)

    #Closing the sockets
    closeSockets(sockfdRP,socketToRP,sockfdUI,socketToUI)


#Input:
#choice - int
#sockfdUI - TCP socket(Connection with UI)
#sockfdRP - TCP socket(Connection with Raspberry Pi)
def startProgram(msgTypeUI, msgDataUI ,sockfdUI,sockfdRP):
    if(isError(msgTypeUI)):
        sendAnswerToUI(sockfdUI, AnswersToUI.ERROR_UI)
        return 0

    systemCommandFromUI = SystemCommandType(int(msgDataUI))
    if systemCommandFromUI == SystemCommandType.START_ROUTE or systemCommandFromUI == SystemCommandType.START_REMOTE_DRIVING or systemCommandFromUI == SystemCommandType.CHECK_SIGN_TYPE:
        sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)
        sendMsgToClient(sockfdRP, MessageType.REQUEST, systemCommandFromUI.value)
    else:
        sendAnswerToUI(sockfdUI, AnswersToUI.ERROR_UI)
        print("Error in request from UI.")
        return 0

    if systemCommandFromUI == SystemCommandType.START_ROUTE:
        startAutonomousDriving(sockfdRP, sockfdUI)

    elif systemCommandFromUI == SystemCommandType.START_REMOTE_DRIVING:
        RemoteDrive(sockfdRP, sockfdUI)

    elif systemCommandFromUI == SystemCommandType.CHECK_SIGN_TYPE:
        CheckTypeOfSign(sockfdRP, sockfdUI)

def isError(msgType):
    if (msgType == MessageType.REQUEST or msgType == MessageType.ANSWER):
        return False
    elif (msgType == MessageType.ERROR):
        print("received error from UI - " + msgDataUI)
        return True
    else:
        print("error in request")
        return True

#*********************************************************** Start Route OPTION:1 ***********************************************************************

def startAutonomousDriving(sockfdRP, sockfdUI):
    global _isStopPressed
    global stopFlag_lock

    signIndex = SignType.NONE
    currentSign = SignType.NONE
    EndRoute = False
    seqIdentify = 0
    Answers = 0

    try:
        CheckThread = threading.Thread(target=WaitForStop, args=(sockfdUI, ))
        CheckThread.daemon = True  # set thread to daemon ('ok' won't be printed in this case)
        CheckThread.start()
    except:
       print ("Error: unable to start thread.")

    while not EndRoute:
        msgTypeRP, msgDataRP = receiveMsgFromClient(sockfdRP)

        if (isError(msgTypeRP)):           
            CheckThread._stop()
            return 0

        signImage = getSignImage(msgDataRP)
        #Get the type of the sign
        signIndex = getTypeOfSign(signImage)

        stopFlag_lock.acquire()
        if ((seqIdentify > 0) and (currentSign != signIndex) and (currentSign == SignType.ENDOFROUTE)) or (_isStopPressed):
            sendMsgToClient(sockfdRP, MessageType.REQUEST, SystemCommandType.END_ROUTE.value)
            EndRoute = True
        elif Answers == 0:
            #The first sign in the route
            currentSign = signIndex
            seqIdentify += 1
            sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.FORWARD.value)
        elif seqIdentify == 0:
            currentSign = signIndex
            seqIdentify +=1
            sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.FORWARD.value)
        elif (seqIdentify > 0) and (currentSign == signIndex):
            seqIdentify += 1
            sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.FORWARD.value)
        elif (seqIdentify > 0) and (currentSign != signIndex):
            sendMsgToClient(sockfdRP, MessageType.REQUEST, FindCurrentSign(currentSign))
            seqIdentify = 0
        stopFlag_lock.release()
        Answers += 1
    CheckThread.join()

#*****Need to edit function*******
#Input:socket
#Output:None
#The function stop the driving (with the thread 
#opened int the past - global).
def WaitForStop(sockfdUI):
    #Global - because of the thread
    global _isStopPressed
    global stopFlag_lock

    msgTypeUI, msgDataUI = receiveRequestFromUI(sockfdUI)
    print("recieved stop(UI)\n")
    #Stop the thread
    if(int(msgDataUI) == SystemCommandType.FINISH_DRIVING.value):
        stopFlag_lock.acquire()
        _isStopPressed = True
        stopFlag_lock.release()
        sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)
    else:
        sendAnswerToUI(sockfdUI, AnswersToUI.ERROR_UI)

#*********************************************************** Remote Drive OPTION:2 ***********************************************************************

def RemoteDrive(sockfdRP, sockfdUI):
    StopDriving = False
    currentDirection = DrivingDirection.FORWARD
    sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.FORWARD.value)

    while not StopDriving:
        msgTypeUI, msgDataUI = receiveRequestFromUI(sockfdUI)

        if (isError(msgTypeUI)):
            sendAnswerToUI(sockfdUI, AnswersToUI.ERROR_UI)
            return 0
        
        directionCommand = int(msgDataUI)

        if(directionCommand == SystemCommandType.FINISH_DRIVING.value):
            sendMsgToClient(sockfdRP, MessageType.REQUEST, SystemCommandType.END_REMOTE_DRIVING.value)
            sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)
            StopDriving = True

        elif(directionCommand == DrivingDirection.FORWARD.value):
            sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.FORWARD.value)
            sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)

        elif(directionCommand == DrivingDirection.RIGHT.value):
            sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.RIGHT.value)
            sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)

        elif(directionCommand == DrivingDirection.LEFT.value):
            sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.LEFT.value)
            sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)

        elif(directionCommand == DrivingDirection.STOP.value):
            sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.STOP.value)
            sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)

        elif(directionCommand == DrivingDirection.REVERSE.value):
            sendMsgToClient(sockfdRP, MessageType.REQUEST, DrivingDirection.REVERSE.value)
            sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)

        else:    
            sendMsgToClient(sockfdRP, MessageType.REQUEST, SystemCommandType.END_REMOTE_DRIVING.value)
            sendAnswerToUI(sockfdUI, AnswersToUI.ERROR_UI)
            StopDriving = True


#*********************************************************** Check Type Of Sign OPTION:3 ***********************************************************************

def CheckTypeOfSign(sockfdRP, sockfdUI):
    msgTypeUI, msgDataUI = receiveRequestFromUI(sockfdUI)

    if (isError(msgTypeUI)):
        sendAnswerToUI(sockfdUI, AnswersToUI.ERROR_UI)
        return 0

    dataUI = int(msgDataUI)
    if(dataUI == SystemCommandType.CAPTURE_IMAGE.value):
        sendAnswerToUI(sockfdUI, AnswersToUI.SUCCESS_UI)
        sendMsgToClient(sockfdRP, MessageType.REQUEST, SystemCommandType.CAPTURE_IMAGE.value)
        msgTypeRP, msgDataRP = receiveMsgFromClient(sockfdRP)

        if (isError(msgTypeRP)):
            sendMsgToClient(sockfdRP, MessageType.ERROR, "Error in message")
            return 0

        signImage = getSignImage(msgDataRP)
        if signImage is not None:
            signImage.save(r"C:/Users/magshimim/Desktop/FinalProject/signcar---/finalProjectGUI/CreationTemplate/SingleImages/singleImage.jpg")
        else:
            shutil.move(r"C:/Users/magshimim/source/repos/serverTry/serverTry_FullyUpdated/images/image.jpg", r"C:\Users\magshimim\Desktop\FinalProject\signcar---\finalProjectGUI\CreationTemplate\SingleImages\singleImage.jpg")
        signIndex = getTypeOfSign(signImage)

        sendMsgToClient(sockfdRP, MessageType.REQUEST, signIndex)
        sendAnswerToUI(sockfdUI, str(signIndex))
    else:    
        sendAnswerToUI(sockfdUI, AnswersToUI.ERROR_UI)
        msgTypeRP, msgDataRP = receiveMsgFromClient(sockfdRP)
    

#*********************************************************** Image Functions ***********************************************************************


#Input - list of the labels that in the image
#bucket = Bucket to upload to
#key = File to download from bucket
#max_labels = number of maximum labels to detect
#min_confidence - number of minimum confidence that the labels will have
#Output: list of labels that in the image
#The function identify hte labels in the image
def detect_labels(bucket, key, max_labels=20, min_confidence=50, region="us-east-2"):
	rekognition = boto3.client("rekognition", region)
	response = rekognition.detect_labels(Image={"S3Object": 
       {"Bucket": bucket,"Name": key,}},
		MaxLabels=max_labels,
		MinConfidence=min_confidence,)
	s3 = boto3.resource('s3')
	s3.Object(bucket, key).delete()
	return response['Labels']

#Input:Image path, the indexes of the image
#Output:cut image
#Then function get a image and returns the cut image
#(according to the indexes)
def cutImage(imagePath, widthToCut, heightToCut, leftToCut, topToCut):  
    # Size of the image in pixels (size of orginal image) 
    # (This is not mandatory) 
    im = Image.open(imagePath) 
    width, height = im.size 
    # Setting the points for cropped image 
    left = width * leftToCut
    top = height * topToCut
    right = left + (width* widthToCut)
    bottom = top + (height* heightToCut)
    # Cropped image of above dimension 
    # (It will not change orginal image) 
    im1 = im.crop((left, top, right, bottom))
    return im1

#Input:image path
#Output:image(with the sign only)
#This function get an image and 
#return a new picture with the sign only in it.
def getSignImage(imagePath):
    s3 = boto3.client('s3')
    #Downloading the image
    s3.download_file(BUCKET, imagePath, "images/image.jpg")
    for label in detect_labels(BUCKET, imagePath):
        if (label["Name"] == "Road Sign"):
            try:
                cutDetails = label["Instances"][0]["BoundingBox"]
            except:
                print("Image Error occurred")
                return None
            return cutImage("images/image.jpg", cutDetails["Width"], cutDetails["Height"], cutDetails["Left"], cutDetails["Top"])
    return None

#Input:image(with the sign only)
#Output:tensor
#The function convert the omage to a tensor that will
#be relavent to the model input.
def editImageToModel(signImage):
    Image_data = []
    img = cv2.imread("images/image.jpg", cv2.IMREAD_GRAYSCALE)
    #resize the image
    img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
    #add
    Image_data.append([np.array(img), np.eye(4)])
    #tensor of the data
    data = getTensor(Image_data)
    data.to(device)
    return data



#*********************************************************** Protocol Functions ***********************************************************************

#Input:HOST(Address),PORT(int),Type(string)
#Output:new socket
#The function creates a new TCP socket.
def OpenTCPSocket(HOST,PORT,Type):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((HOST, PORT))
    sock.listen(100)
    print("listening...(" + Type + ")\n")
    return sock

#*************************************** Protocol - Raspberry PI ***************************************
#   @MessageType#DATA!Checksum

#Input:socket,type(SystemCommandType - Enum), msgData
#Output:None
#The function creates and sends a message to the client (Raspberry Pi).
def sendMsgToClient(sockfdRP, msgType, msgData):
    msgToRP = ("@" + str(msgType.value) + "#" + str(msgData) + "!" + str(len(str(msgData))))
    sockfdRP.sendall((msgToRP).encode('utf-8'))
    print("sent message to client (RP).  " + msgToRP + "\n")

#Input:socket
#Output: Type of the message, Data from the message
#The function receives and parsers the message from client
def receiveMsgFromClient(sockfdRP):
    #receive message
    msgFromRP = sockfdRP.recv(BUFFER)
    msg = msgFromRP.decode('utf-8')
    print("Recieved message from client (RP).  " + msg + "\n")
    #parse message
    msgType = MessageType(int(msg[msg.find('@')+1:msg.find('#')]))
    msgData = str((msg[msg.find('#')+1:msg.find('!')]))
    checksum = int((msg[msg.find('!')+1:]))
    #make sure that the checksum is good
    if ( len(msgData) is not checksum):
        msgType = MessageType.ERROR
        msgData = "checksum Error"
    return msgType, msgData


#*************************************** Protocol - UI ***************************************
#   REQUEST:             $MessageType%DATA&Checksum
#   SUCCESS - ANSWER:    ?YES
#   ERROR  - ANSWER:     ?ERR

#Input:socket, answerToUI
#Output:None
#The function creates and sends an answer to UI.
def sendAnswerToUI(sockfdUI, answerToUI):
    sockfdUI.sendall(("!" + answerToUI).encode('utf-8'))
    print("sent answer to UI (UI).  " + answerToUI + "\n")

#Input:socket
#Output: Type of the message, Data from the message
#The function receives and parsers the message from UI
def receiveRequestFromUI(sockfdUI):
    #receive request
    msgFromUI = sockfdUI.recv(BUFFER)
    msg = msgFromUI.decode('utf-8')
    print("Recieved message from UI.  " + msg + "\n")
    #parse request
    msgType = MessageType(int(msg[msg.find('$')+1:msg.find('%')]))
    msgData = str((msg[msg.find('%')+1:msg.find('&')]))
    checksum = int((msg[msg.find('&')+1:]))
    #make sure that the checksum is good
    if ( len(msgData) is not checksum):
        msgType = MessageType.ERROR
        msgData = "checksum Error"
    return msgType, msgData



#*********************************************************** Model Functions ***********************************************************************

#Input:image(with the sign only)
#Output:type of sign in image
#The function checks what is the type of the sign in the image
def getTypeOfSign(signImage):
    if signImage is not None:
        #Get the tensor of the data of the image
        ImageData = editImageToModel(signImage)
        #Get the type of the sign
        ModelOutput = model(ImageData[0].view(-1, 1, IMG_SIZE, IMG_SIZE))[0]
        os.remove("images/image.jpg")
        #from tensor to int
        signIndex = getSignTypeResults(ModelOutput)
    else:
        result = None
        signIndex = -1
    return signIndex

#Input:ModelOutput(tensor)
#Output:int
#The function convert the Model output from 
#tensor to int
def getSignTypeResults(ModelOutput):
    #Check that the identify is ok
    if CheckSign(ModelOutput):
        #Convert to int
        predicted_class = torch.argmax(ModelOutput)
        return predicted_class
    else:
        return -1

#Input:ModelOutput(tensor)
#Output:Bool
#This function checks that the
#image identification is OK
def CheckSign(ModelOutput):
    signfound = False
    sm = torch.nn.Softmax(dim=-1)
    x = sm(ModelOutput)
    x = x*100
   #Converted to percentage
    for i in x:
        if i > PERCENTAGE:
            signfound = True
    return signfound

#Input:list
#Output:tensor of the data(to the model)
#The function converts from the list of the data of the image
#to a tensor(to the model).
def getTensor(data):
    train = torch.Tensor([i[0] for i in data]).view(-1,IMG_SIZE,IMG_SIZE)
    return (train/255.0)

#*********************************************************** Other Functions ***********************************************************************

#Input: 4 TCP sockets
#Output:None
#The function close the sockets.
def closeSockets(sockfdRP,socketToRP,sockfdUI,socketToUI):
    sockfdRP.close()
    socketToRP.close()
    sockfdUI.close()
    socketToUI.close()


#*****Need to edit function*******
#Input:int
#Output:Dri
#The function send a command.
def FindCurrentSign(currentSign):
    if currentSign == SignType.STOP.value:
        return DrivingDirection.STOP.value
    if currentSign == SignType.RIGHT.value:
        return DrivingDirection.RIGHT.value
    if currentSign == SignType.LEFT.value:
        return DrivingDirection.LEFT.value
    if currentSign == -1:
        return DrivingDirection.FORWARD.value

if __name__ == "__main__":
    main()
