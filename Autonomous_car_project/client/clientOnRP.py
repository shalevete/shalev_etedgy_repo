#import necessery packets
import cv2
import time
import numpy as np
import random
import socket, select
from time import gmtime, strftime
from random import randint
from io import BytesIO
from PIL import Image
import boto3
from botocore.exceptions import ClientError
import json
from enum import Enum
import picamera
import PiMotor
import RPi.GPIO as GPIO

#Name of Individual MOTORS 
m1 = PiMotor.Motor("MOTOR1",1)
m2 = PiMotor.Motor("MOTOR2",1)
m3 = PiMotor.Motor("MOTOR3",1)
m4 = PiMotor.Motor("MOTOR4",1)

#Names for Individual Arrows
ab = PiMotor.Arrow(1)
al = PiMotor.Arrow(2)
af = PiMotor.Arrow(3) 
ar = PiMotor.Arrow(4)

motorAll = PiMotor.LinkedMotors(m1,m2,m3,m4)

#Type of classification to the AWS server
BUCKET = "sign-detect"
#Address
HOST = '192.168.1.6'
#Port
PORT = 2428


#Class: Message type - types of the messages in the protocol.
class MessageType(Enum):
    ERROR = 0
    REQUEST = 1
    ANSWER = 2

#Class: System Command Types -  types of the commands in the protocol.
class SystemCommandType(Enum):
    START_ROUTE = 10
    END_ROUTE = 20
    START_REMOTE_DRIVING = 30
    END_REMOTE_DRIVING = 40
    CHECK_SIGN_TYPE = 50
    CAPTURE_IMAGE = 70

#Class: Sign Type - types of signs.
class SignType(Enum):
    ENDOFROUTE = 3
    LEFT = 2
    RIGHT = 1
    STOP = 0
    NONE = -1

#Class: Driving Direction - types of directions.
class DrivingDirection(Enum):
    FORWARD = 100
    RIGHT = 200
    LEFT = 300
    STOP = 400
    REVERSE = 500

def main():
    #Open socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (HOST, PORT)
    sock.connect(server_address)
    
    msgType, msgData = receiveMsgFromServer(sock)
        
    clientChoice(int(msgData), sock)

    sock.close()


#Input:typeOfCommand - int, sock - TCP socket
#Output:None
#The function active the right main function according to the command.
def clientChoice(typeOfCommand, sock):
    if SystemCommandType(typeOfCommand) ==  SystemCommandType.START_ROUTE:
        StartDriving(sock)
    elif SystemCommandType(typeOfCommand) ==  SystemCommandType.START_REMOTE_DRIVING:
        RemoteDriving(sock)
    elif SystemCommandType(typeOfCommand) ==  SystemCommandType.CHECK_SIGN_TYPE:
        CheckTypeOfSign(sock)

        
#********************************************************************** Main Functions *********************************************************************************************

#Input:sockfd - TCP socket
#Output:None
#The function activate the Self-Driving option.
def StartDriving(sockfd):

    EndRoute = False
    camera = picamera.PiCamera()

    while not EndRoute:
        #Taking a picture
        camera.capture('images/picture.jpg')
        print("Captured.")

        #Sending the image to the AWS server
        isUploaded = upload_file('images/picture.jpg', BUCKET,'picture.jpg')
        #Send Update to the PC server
        sendUpdate(isUploaded,sockfd,'picture.jpg')

        msgType, msgData = receiveMsgFromServer(sockfd)     
        
        if (msgType == MessageType.REQUEST):
            EndRoute = DoCommand(int(msgData))
        elif (msgType == MessageType.ERROR):
            print("received error from server - " + msgDataRP)
            return 0
        else:
            print("error in answer from server")
            return 0

        

#Input:sockfd - TCP socket
#Output:None
#The function activate the remote control option.
def RemoteDriving(sockfd):
    StopDriving = False

    while not StopDriving:
        msgType, msgData = receiveMsgFromServer(sockfd)
        if (msgType == MessageType.REQUEST):
            StopDriving = DoCommand(int(msgData))
        elif (msgType == MessageType.ERROR):
            print("received error from server - " + msgDataRP)
            return 0
        else:
            print("error in answer from server")
            return 0


#Input:sockfd
#Output:None
##The function activate the Check Type of sign option.
def CheckTypeOfSign(sockfd):
    camera = picamera.PiCamera()

    #Waiting for capture request
    msgType, msgData = receiveMsgFromServer(sockfd)
    if (msgType == MessageType.REQUEST):
        if(int(msgData) == SystemCommandType.CAPTURE_IMAGE.value):
            #Taking a picture
            camera.capture('images/single_picture.jpg')
            print("Captured.")

            #Sending the image to the AWS server
            isUploaded = upload_file("/home/pi/Desktop/image.jpg", BUCKET,'single_picture.jpg')
            #Send Update to the PC server
            sendUpdate(isUploaded,sockfd,'single_picture.jpg')

            #Receive answer from the server on the PC
            msgType, msgData = receiveMsgFromServer(sockfd)
            print("The type of the sign is:" + msgData)

        else:
            sendMsgToServer(sockfd, MessageType.Error, "ERROR in receiving rrequest")
    elif (msgType == MessageType.ERROR):
        print("received error from server - " + msgDataRP)
        return 0
    else:
        print("error in answer from server")
        return 0



#********************************************************************** Protocol Functions *********************************************************************************************

#Input:
#isUploaded - bool
#sockfd - TCP socket
#picName - string
#Output:None
#The function send update to the PC Server.
def sendUpdate(isUploaded,sockfd,picName):
    if (isUploaded):
        sendMsgToServer(sockfd, MessageType.ANSWER, picName)
    else:
        sendMsgToServer(sockfd, MessageType.ERROR, "Unable to open file.")


#*************************************** Protocol ***************************************
#   @MessageType#DATA!Checksum

#Input:socket,type(SystemCommandType - Enum), msgData
#Output:None
#The function creates and sends a message to the server.
def sendMsgToServer(sockfd, msgType, msgData):
    msgToSend = ("@" + str(msgType.value) + "#" + str(msgData) + "!" + str(len(str(msgData))))
    sockfd.sendall((msgToSend).encode('utf-8'))
    print("sent message to server.  " + msgToSend + "\n")

#Input:socket
#Output: Type of the message, Data from the message
#The function receives and parsers the message from the server
def receiveMsgFromServer(sockfd):
    #receive message
    msgFromServer = sockfd.recv(1024)
    msg = msgFromServer.decode('utf-8')
    print("Recieved message from server.  " + msg + "\n")
    #parse message
    msgType = MessageType(int(msg[msg.find('@')+1:msg.find('#')]))
    msgData = str((msg[msg.find('#')+1:msg.find('!')]))
    checksum = int((msg[msg.find('!')+1:]))
    #make sure that the checksum is good
    if ( len(msgData) is not checksum):
        msgType = MessageType.ERROR
        msgData = "checksum Error"
    return msgType, msgData

#********************************************************************** Image Functions *********************************************************************************************

#Input: 
#file_name - File to upload
#bucket - Bucket to upload to
#object_name - S3 object name. If not specified then file_name is used
#Output:Bool - True if file was uploaded, else False
#The function uploads a file to an S3 bucket.
def upload_file(file_name, bucket, object_name):
    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = file_name
    # Upload the file
    s3_client = boto3.client('s3')
    #Try and catch - in case(No connection, server down).
    try:
        response = s3_client.upload_file(file_name, bucket, object_name)
    except ClientError as e:
        logging.error(e)
        return False
    return True
        
#********************************************************************** Engines Functions *********************************************************************************************

#Input:int
#Output:Bool - Finish the action or not
#The function call to the right function 
#to activate the engines
def DoCommand(typeOfAnswer):
    if typeOfAnswer ==  DrivingDirection.FORWARD.value:
        print("Keep Forward.")
        driveForward()
        return False
    elif typeOfAnswer ==  DrivingDirection.LEFT.value:
        print("Turning Left...")
        turnLeft()
        return False
    elif typeOfAnswer ==  DrivingDirection.RIGHT.value:
        print("Turning Right...")
        turnRight()
        return False
    elif typeOfAnswer ==  DrivingDirection.REVERSE.value:
        print("Driving Reverse")
        driveReverse()
        return False
    elif typeOfAnswer ==  DrivingDirection.STOP.value:
        print("Pause...")
        motorAll.stop()
        return False
    elif typeOfAnswer ==  SystemCommandType.END_ROUTE.value:
        print("Stopping...")
        motorAll.stop()
        return True
    elif typeOfAnswer ==  SystemCommandType.END_REMOTE_DRIVING.value:
        print("Stopping...")
        motorAll.stop()
        return True

#Input:None
#Output:None
#The function active the engines
#and turning right
def turnRight():
    ar.on()
    m4.reverse(100)
    m3.forward(100)
    time.sleep(1.5)
    motorAll.stop()
    ar.off()

#Input:None
#Output:None
#The function active the engines
#and turning left
def turnLeft():
    al.on()
    m2.reverse(100)
    m1.forward(100)
    time.sleep(1.5)
    motorAll.stop()
    al.off()

#Input:None
#Output:None
#The function active the engines
#and driving forward for 2.5 seconds.
def driveForward():
    af.on()
    m2.reverse(100)
    m3.forward(100)
    m1.forward(100)
    m4.reverse(100)
    time.sleep(2.5)
    motorAll.stop()
    af.off()

#Input:None
#Output:None
#The function active the engines
#and do reverse for 2.5 seconds.
def driveReverse():
    ab.on()
    m2.forward(100)
    m3.reverse(100)
    m1.reverse(100)
    m4.forward(100)
    time.sleep(2.5)
    motorAll.stop()
    ab.off()


if __name__ == "__main__":
    main()
