import socket
import requests
import json
import time

KEYS_FOR_STATISTICS = ["ip", "country", "software", "port"]
INDEXES_TO_CHANGE_IN_HTML = [114, 152, 156, 180, 184, 225, 229, 272, 276, 317, 321, 364, 368, 403]

#this port(9145) will be used to get details from the agents
LISTEN_PORT = 9145

#this port(8808) and ip(54.71.128.194) will be used to upload the details to the website
SERVER_IP = "54.71.128.194"
SERVER_PORT = 8808

def open_settings():
    """
    this function parse the settings of the admin(WORKERS, BLACKLIST)
    input: none
    return: dict of the workers({ip: name}), dict of the blacklist({ip: name})
    """
    with open(r"settings.dat", "r") as f:
        settings = f.read()

    settings = settings.split("\n") #split the settings file by \n

    #parsing workers

    workers = settings[0][10:] #get only the ip and names from the line of the workers

    #splitting the workers details
    workers = workers.split(",")
    workers_list = []
    for worker in workers:
        workers_list.append(worker.split(":"))

    workers_dict = {worker[0]:str(worker[1:]) for worker in workers_list} #creating the dict of the workers({ip: name})

    #deleting the [] from every IP in the dict
    for key, worker in workers_dict.items():
        workers_dict[key] = worker.replace("[", "")
        workers_dict[key] = workers_dict[key].replace("]", "")
        workers_dict[key] = workers_dict[key].replace("'", "")

    workers_dict = dict((y,x) for x,y in workers_dict.items()) #switch between the name and the IP in the dict (from {name: ip} to {ip: name})

    ##############################################################################################
    #parsing blacklist

    blacklist = settings[1][12:] #get only the ip and websites names from the line of the blacklist

    #splitting the workers details
    blacklist = blacklist.split(",")
    blacklist_list = []
    for worker in blacklist:
        blacklist_list.append(worker.split(":"))

    blacklist_dict = {worker[0]:str(worker[1:]) for worker in blacklist_list} #creating the dict of the blacklist({ip: name})

    #deleting the [] from every IP in the dict
    for key, value in blacklist_dict.items():
        blacklist_dict[key] = value.replace("[", "")
        blacklist_dict[key] = blacklist_dict[key].replace("]", "")
        blacklist_dict[key] = blacklist_dict[key].replace("'", "")

    return(workers_dict, blacklist_dict)

def receive_data_from_agent():
    """
    this function recive the data from the agent
    input: none
    return: client_msg: the msg from the agent, client_addr: the details about the agent who sent the msg(tuple) 
    """
    # Create a non-specific UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Binding to local UDP port 3986
    server_address = ('', LISTEN_PORT)

    sock.bind(server_address)

    # Receiving data from the the socket, could be from anyone!

    client_msg, client_addr = sock.recvfrom(40000)
    print("msg_recived")
    sock.close()

    return (client_msg, client_addr)

def all_details_to_list(list_of_details, data):
    """
    this function order the details in data structures
    input: list_of_details - list that contains the past sniffs details, data - the data from the current agent(data to add to the list_of_details)
    return: list_of_details - updated
    """
    for dict_of_packet in data:
        i = 0
        for key in KEYS_FOR_STATISTICS:
            list_of_details[i].append((dict_of_packet[key], dict_of_packet["packet_size"]))
            i = i + 1
    return list_of_details

def sum_size(list_of_detail_and_size):
    """
    this function sums the size of each detail in the list of tuples
    input: list_of_detail_and_size - list of tuples that contains details and their sizes
    return: list of tuples with the summed bytes
    """
    list_of_details = []
    list_of_summed_bytes = []

    #put the details in a different list
    for detail_and_size in list_of_detail_and_size:
        list_of_details.append(detail_and_size[0])

    #remove the duplicates from the list by converting to set and to list again
    duplicates_removed = set(list_of_details)
    list_of_details = list(duplicates_removed)

    #sum the bytes of each detail and add it to a new list of tuples
    sum = 0
    for detail in list_of_details:
        sum = 0
        for detail_and_size in list_of_detail_and_size:
            if detail_and_size[0] == detail:
                sum = sum + int(detail_and_size[1])
        list_of_summed_bytes.append((detail,sum))
    return list_of_summed_bytes

def traffic_per_agent_and_alerts(worker_name, data, list_of_details, blacklist):
    """
    this function analyze the traffic per agent and alerts from the data that came from the agent
    input: worker_name - the worker(agent) name, list_of_details - list that contains the past sniffs details, data - the data from the current agent(data to add to the list_of_details), blacklist
    return: list_of_dicts(traffic_per_agent), alerts
    """
    traffic_per_agent = []
    list_of_dicts = list_of_details[4]
    alerts = list_of_details[5]

    for dict_of_packet in data:
        traffic_per_agent.append((dict_of_packet["packet_incoming"], dict_of_packet["packet_size"]))
        if dict_of_packet["ip"] in blacklist:	
            alerts.append((worker_name, dict_of_packet["ip"]))

    traffic_per_agent = sum_size(traffic_per_agent)

    for incoming_tuple in traffic_per_agent:
        if incoming_tuple[0] == True:
            list_of_dicts[0][worker_name] = incoming_tuple[1]
        else:
            list_of_dicts[1][worker_name] = incoming_tuple[1]

    #remove the duplicates from the list by converting to set and to list again
    alerts = set(alerts)
    alerts = list(alerts)

    return (list_of_dicts, alerts)

def analyze_data_from_agent(list_of_details, data, worker_name, blacklist):
    """
    this function analyze the data that came from the agent
    input: list_of_details - list that contains the past sniffs details, data - the data from the current agent(data to add to the list_of_details), worker_name - the worker(agent) name, blacklist 
    return: list_of_details - updated
    """
    #add all of the details and their size into the list
    list_of_details = all_details_to_list(list_of_details, data)

    for i in range(4):
        list_of_details[i] = sum_size(list_of_details[i])

    #analyze traffic per agent and alerts
    list_of_details[4], list_of_details[5] = traffic_per_agent_and_alerts(worker_name, data, list_of_details, blacklist)

    return list_of_details

def get_time():
    return str(time.strftime("%d.%m.%Y") + ', ' + time.strftime("%H:%M"))#get the time and date

def update_line(is_data, str_instead):
    """
    this function return the new string that will be in a line
    input: is_data - boolean, str_instead - data that we want to put in the website
    return: new string that will be in a line
    """
    if is_data:
        return '                                data: '+str_instead+'\n'
    else:
        return '                            labels: '+str_instead+',\n'

def change_type_for_agent_details(list_of_keys_or_values):
    """
    this function change the type of a list of keys to list and then to str
    input: list_of_keys_or_values - list to convert
    return: list of keys to list and then to str
    """
    return str(list(list_of_keys_or_values))

def change_type_for_statistics(label_or_data, list_to_change):
    """
    this function change the type of the list to str
    input: label_or_data(0 or 1), list_to_change
    return: list converted to str
    """
    return str([item[label_or_data] for item in list_to_change])

def update_website(list_of_details):
    """
    this function update the html code of the website
    input: list_of_details - list that contains the past sniffs details
    return: html code of the website
    """
    with open("template/html/template.html", 'r') as file:
        # read a list of lines into all_lines
        all_lines = file.readlines()

    # update time
    all_lines[INDEXES_TO_CHANGE_IN_HTML[0]] = '                        <p>Last update: '+ get_time() +'</p>\n'

    # update Traffic Per Agent
    agents = change_type_for_agent_details(list_of_details[4][0].keys()) #get from the incoming dict the agent names and convert them into a string(Example: "[Dani, Roy, Shay, Shalev]")
    agents_in_values = change_type_for_agent_details(list_of_details[4][0].values())
    agents_out_values = change_type_for_agent_details(list_of_details[4][1].values())

    all_lines[INDEXES_TO_CHANGE_IN_HTML[1]] = update_line(False, agents)
    all_lines[INDEXES_TO_CHANGE_IN_HTML[2]] = update_line(True, agents_in_values)
    all_lines[INDEXES_TO_CHANGE_IN_HTML[3]] = update_line(False, agents)
    all_lines[INDEXES_TO_CHANGE_IN_HTML[4]] = update_line(True, agents_out_values)

    # update Traffic Per Country
    all_lines[INDEXES_TO_CHANGE_IN_HTML[5]] = update_line(False, change_type_for_statistics(0, list_of_details[1])) #Label
    all_lines[INDEXES_TO_CHANGE_IN_HTML[6]] = update_line(True, change_type_for_statistics(1, list_of_details[1])) #Data

    # update Traffic Per IP
    all_lines[INDEXES_TO_CHANGE_IN_HTML[7]] = update_line(False, change_type_for_statistics(0, list_of_details[0])) #Label
    all_lines[INDEXES_TO_CHANGE_IN_HTML[8]] = update_line(True, change_type_for_statistics(1, list_of_details[0])) #Data

    # update Traffic Per App
    all_lines[INDEXES_TO_CHANGE_IN_HTML[9]] = update_line(False, change_type_for_statistics(0, list_of_details[2])) #Data
    all_lines[INDEXES_TO_CHANGE_IN_HTML[10]] = update_line(True, change_type_for_statistics(1, list_of_details[2])) #Data


    # update Traffic Per Port
    all_lines[INDEXES_TO_CHANGE_IN_HTML[11]] = update_line(False, change_type_for_statistics(0, list_of_details[3])) #Data
    all_lines[INDEXES_TO_CHANGE_IN_HTML[12]] = update_line(True, change_type_for_statistics(1, list_of_details[3])) #Data

    # update alerts
    alerts = str(list_of_details[5])
    all_lines[INDEXES_TO_CHANGE_IN_HTML[13]] = '                '+alerts+'\n'


    # and write everything back
    with open("template/html/template.html", 'w') as file:
        file.writelines( all_lines )

    return ''.join(all_lines)

def upload_website(html):
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connecting to remote computer 80
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)

    # Sending data to server
    msg = "400#USER=shalev.etedgy"
    sock.sendall(msg.encode())

    # Receiving data from the server
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()

    # Sending data to server
    msg = "700#SIZE="+str(len(html))+",HTML=" + html
    sock.sendall(msg.encode())

    # Receiving data from the server
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()

    # Sending data to server
    msg = "900#BYE"
    sock.sendall(msg.encode())

    # Receiving data from the server
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()

    # Closing the socket
    sock.close()

def main():
    workers, blacklist = open_settings()
    print(workers, blacklist)

    #list which contains all the details of the packets
    list_of_details = [[], [], [], [], [{}, {}], []] #[list of IPs, list of countries, list of softwares, list of ports, traffic per agent{incoming}{outgoing}, alerts(list)]

    while True:
        #receive data from agent
        data, client_addr = receive_data_from_agent()
        data=json.loads(data)

        #get the worker name
        if client_addr[0] in workers:
            worker_name = workers[client_addr[0]]

        #analyze the data
        list_of_details = analyze_data_from_agent(list_of_details, data, worker_name, blacklist)

        print('\nlist of IPs: ',list_of_details[0], '\nlist of countries: ', list_of_details[1], '\nlist of softwares: ',list_of_details[2], '\nlist of ports: ', list_of_details[3],'\ntraffic per agent(incoming): ', list_of_details[4][0],'\ntraffic per agent(outgoing): ', list_of_details[4][1], '\nalerts: ', list_of_details[5])

        #edit and upload the website
        html = update_website(list_of_details)
        upload_website(html)

if __name__ == "__main__": 
    main()