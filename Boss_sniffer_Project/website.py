import socket

SERVER_IP = "54.71.128.194"
SERVER_PORT = 8808

def upload_site():
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connecting to remote computer 80
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)

    # Sending data to server
    msg = "400#USER=shalev.etedgy"
    sock.sendall(msg.encode())

    # Receiving data from the server
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()

    print(server_msg)

    # Sending data to server
    msg = "700#SIZE=0,HTML="
    sock.sendall(msg.encode())
    print('data sent')

    # Receiving data from the server
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    print(server_msg)

    # Sending data to server
    msg = "900#BYE"
    sock.sendall(msg.encode())

    # Receiving data from the server
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    print(server_msg)

    # Closing the socket
    sock.close()


def main():
    upload_site()

if __name__ == "__main__": 
    main()