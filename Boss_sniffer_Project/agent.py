import requests
import json
from scapy3k.all import *
import socket
import os
import re

SERVER_ADDR = '127.0.0.1'
SERVER_PORT = 9145

country_dicts_global = {}
software_dict_global = {}

def what_my_ip():
    """
    this function check what is this computer's IP
    input: none
    return: this computer's IP
    """
    p = IP(dst="www.google.com", ttl = 50)/ICMP() #set up a package in order to scapy will automatically complete the IP of this computer
    return p.src

def is_good(packet):
    """checking if the packet has IP under TCP or UDP"""
    return IP in packet and (UDP in packet or TCP in packet)

def check_software(packet):
    global software_dict_global

    my_ip = what_my_ip()
    netstat = os.popen('netstat -nb').read()

    if TCP in packet:
        if packet[IP].dst == my_ip:
            app_port = packet[TCP].dport
        else:
            app_port = packet[TCP].sport
    else:
        if packet[IP].dst == my_ip:
            app_port = packet[UDP].dport
        else:
            app_port = packet[UDP].sport

    if str(app_port) in netstat:
        pattern = "(:"+str(app_port)+")(.+[0-9].+\n [[])([A-z]+.(exe|EXE))"
        matches = re.search(pattern, netstat)
        if matches:
            print(matches.group(3), app_port, packet[IP].dst)
            software_dict_global[app_port] = matches.group(3)
        else:
            print(matches, app_port, packet[IP].dst)

def sniff_packets():
    """
    this function sniff for 250 packets
    input: none
    return: the packets that were sniffed from the computer
    """
    print("sniff started\n")
    packets = sniff(lfilter=is_good, prn=check_software, count=250)
    print("sniff finished\n")
    return packets

def is_packet_incoming(my_ip, packet):
    """
    this function check if the packet incoming
    input: the packet, ip of the user's computer(str)
    return: if the packet is incoming(boolean)
    """
    return packet[IP].dst == my_ip

def get_ip(incoming, packet):
    """
    this function check what is the IP that we conversed with
    input: incoming: if the packet is incoming(boolean),  packet
    return: the IP that we conversed with(str)
    """
    if incoming:
        return packet[IP].src
    else:
        return packet[IP].dst

def get_country(ip):
    """
    this function check what is the country of the IP that we conversed with
    input: ip: the IP that we conversed with(str)
    return: the country of the IP that we conversed with(str)
    """
    global country_dicts_global

    if ip in country_dicts_global: #if the ip already in the global dict of the countries
        return country_dicts_global[ip] #take the country name from the global dict of the countries
    else:
        #send get request to ip-api.com in order to get the country of the ip
        url_country_of_ip = 'http://ip-api.com/json/' + ip
        response = requests.get(url_country_of_ip)
        response = response.text
        country_of_ip = json.loads(response)

        #add the country of the ip to the global dict of the countries
        if 'country' in country_of_ip.keys():
	        country_ip = country_of_ip['country']
        else: #check if the country unknown
	        country_ip = 'not found'
        country_dicts_global[ip] = country_ip

        return country_ip

def get_port(incoming, packet):
    """
    this function return the port of the IP that we conversed with
    input: incoming: if the packet is incoming(boolean),  packet
    return: the port of the IP that we conversed with(int)
    """
    if TCP in packet:
        if incoming:
            return(packet[TCP].sport, packet[TCP].dport)
        else:
            return(packet[TCP].dport, packet[TCP].sport)
    else:
        if incoming:
            return(packet[UDP].sport, packet[UDP].dport)
        else:
            return(packet[UDP].dport, packet[UDP].sport)

def get_software(my_port):
    """
    this function check what software is related to the port of the packet
    input: my_port: my port(int)
    return: the name of the software or 'Unknown'(str)
    """
    global software_dict_global

    found_flag = False
    my_port = my_port

    for port, software in software_dict_global.items():
        if my_port == port: 
            found_flag = True
            return software

    #if the software of the port wasn't found so return 'Unknown'
    if found_flag is False:
        return 'Unknown'

def analyze_packet(packet, my_ip):
    """
    this function get the details about the packet and put him into a dictionary
    input: the packet, ip of the user's computer(str)
    return: dictionary of the details about the packet
    """
    incoming = is_packet_incoming(my_ip, packet)
    ip = get_ip(incoming, packet)
    ip_country = get_country(ip)
    port_ip, my_port = get_port(incoming, packet)
    packet_size = len(packet)
    software_used = get_software(my_port)
    dict = {"ip":ip, "country": ip_country, "port":port_ip, "packet_incoming": incoming, "packet_size": packet_size, "software": software_used}
    print(dict, '\n')
    return dict

def send_data(list_of_packets):
    """
    this function sends the data to the admin
    input: list_of_packets - list to send_data
    return: none
    """

    # Create a non-specific UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Sending a message to server
    msg = json.dumps(list_of_packets)
    server_address = (SERVER_ADDR, SERVER_PORT)
    print(len(msg.encode()))
    sock.sendto(msg.encode(),server_address)

    print("Data sent\n")

    # Closing the socket
    sock.close()


def main():
    global software_dict_global
    my_ip = what_my_ip()
    while True:
        packets = sniff_packets()
        print(software_dict_global, '\n')
        packets_list = []
        for packet in packets:
            packet_dict = analyze_packet(packet, my_ip)
            packets_list.append(packet_dict)
        send_data(packets_list)

if __name__ == "__main__": 
    main()