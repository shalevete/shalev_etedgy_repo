This is the final project of my first year in Magshimim as part of networks and python course.
This project includes Python network programming tools such as sockets, scapy and HTTP requests.
In this project, I developed a tool that allows managers to see if their employees are using their computer not for work.
The "Boss Sniffer" tracking tool has two parts:
1) The agent - a tool that located on employees' computers and sniffing quietly, and collects data.
Once in a while, he will send a report with the data that he collected to the manager.
2) The manager - a tool that receives reports from the various agents and collects all the information and summarize it, for the company manager.
The tool generates reports (HTML files) that summarize the information that came from the agents, and indicate which employees have violated rules defined by the manager.